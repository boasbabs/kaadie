# from __future__ import with_statement
from fabric.api import local, abort, settings, run, env, cd, sudo
from fabric.contrib.console import confirm

env.key_filename = ['/home/stikks/Documents/projects/kaadie/kaadie.pem']
env.roledefs = {
    'local': ['localhost'],
    'test': {
        'hosts': ['ubuntu@75.101.244.147']
    },
    'prod': ['kaadie@kaadie.com']
}

env.roledefs['all'] = [h for r in env.roledefs.values() for h in r]


def commit(message='updating...'):
    """
    commit changes to the staging area
    :param message:
    :return:
    """
    local("git add --all")
    with settings(warn_only=True):
        result = local("git commit -m '%s'" % message, capture=True)
        if result.failed and not confirm("Tests failed. Continue anyway?"):
            abort("Aborting at your behest")


def script(variable='shell'):
    """
    python shell
    :param variable:
    :return:
    """
    local("python kaadie/manage.py %s" % variable)


def pull():
    """
    update environment
    :return:
    """
    local("git pull")


def update_environs(message='updating...'):
    """
    update local working environment
    :param message
    :return:
    """
    commit(message)
    local("git pull")


def update_prod(message='updating...'):
    """
    update local working environment
    :return:
    """
    with settings(warn_only=True, password='kaadie.com'):
        with cd('/opt/kaadie'):
            run("git add --all")
            result = run("git commit -m '%s'" % message, warn_only=True)
            if result.failed and not confirm("Tests failed. Continue anyway?"):
                abort("Aborting at your behest")
            run("git pull")


def push(message='updating...', branch='master', should_commit=True):
    """
    push changes
    :param message
    :return:
    """
    if should_commit is True:
    	commit(message)
    local("git push --set-upstream origin %s" % branch)


def update_static(python_path='/usr/bin/python', manage_path='kaadie/manage.py'):
    """
    update static
    :param python_path:
    :param manage_path:
    :return:
    """
    run('%s %s collectstatic' % (python_path, manage_path))


def migrate_database(python_path='/usr/bin/python', manage_path='kaadie/manage.py'):
    """
    migrate database
    :return:
    """
    run('%s %s makemigrations --merge' % (python_path, manage_path))
    run('%s %s migrate' % (python_path, manage_path))


def start_service(service_path):
    """
    restart a system service
    :param service_path:
    :return:
    """
    sudo('%s start' % service_path)


def stop_service(service_path):
    """
    restart a system service
    :param service_path:
    :return:
    """
    sudo('%s stop' % service_path)


def restart_service(service_path):
    """
    restart a system service
    :param service_path:
    :return:
    """
    sudo('%s restart' % service_path)


def restart_services(service_paths=list()):
    """
    restart list of services
    """
    for _path in service_paths:
        sudo('/usr/sbin/service %s restart' % _path)


def deploy():
    """
    update production environment
    :return:
    """
    with cd('/opt/kaadie'):
        sudo('git pull')
        run('. venv/bin/activate')
        restart_services(['uwsgi', 'nginx'])


def static_deploy():
    """
    update production environment
    :return:
    """
    with cd('/opt/kaadie'):
        sudo('git pull')
        sudo('. venv/bin/activate')
        update_static(python_path='/opt/kaadie/venv/bin/python')
        restart_services(['uwsgi', 'nginx'])


def full_deploy():
    """
    deploy major changes to production, including model changes
    :return:
    """
    with cd('/opt/kaadie'):
        sudo('git pull')
        run('. venv/bin/activate')
        sudo('pip install -r requirements.txt')
        update_static(python_path='/opt/kaadie/venv/bin/python')
        migrate_database(python_path='/opt/kaadie/venv/bin/python')
        restart_services(['uwsgi', 'nginx', 'celeryd'])
