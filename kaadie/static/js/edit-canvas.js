//$(document).ready(function() {
//
//  var ct = 0;
//
//  $('.next1').one('click', function(e) {
//
//    e.preventDefault();
//
//    $('#cards').animate('slow', function() {
//
//      if (ct > 0) {
//        $('#cards').removeClass('transition visible');
//        $('#cards').addClass('transition hidden');
//
//      }
//      $('#cards').css('display', 'none');
//
//      $('#card-tile').addClass('disabled');
//      $('#inner-tile').removeClass('disabled');
//      $("#internal").transition('fly right');
//      $('body').css('background-color', '#06000a');
//      $('#internal button').removeClass('inverted violet');
//      $('#internal button').addClass('inverted blue');
//      ct++;
//
//    });
//
//  });
//
//  $('.prev1').one('click', function(e) {
//
//    e.preventDefault();
//    $('#card-tile').removeClass('disabled');
//    $('#inner-tile').addClass('disabled');
//
//    $('#internal').animate('slow', function() {
//
//      $('body').css('background-color', '#300032');
//      $('#internal').transition('hide');
//      $("#cards").transition('fly right');
//
//    });
//
//  });
//
//  $('.next2').one('click', function(m) {
//
//    m.preventDefault();
//
//    $('#inner-tile').addClass('disabled');
//    $('#details-tile').removeClass('disabled');
//
//    $('#internal').animate('slow', function() {
//
//      $('#details button').removeClass("inverted violet");
//      $('#details button').addClass("inverted orange");
//      $('body').css('background-color', '#251605');
//      $('#internal').transition('hide');
//
//      $('#details').transition('fly right');
//    });
//
//  });
//
//  $('.prev2').one('click', function(m) {
//
//    m.preventDefault();
//    $('#details-tile').addClass('disabled');
//    $('#inner-tile').removeClass('disabled');
//
//    $('#details').animate('slow', function() {
//
//      $('body').css('background-color', '#06000a');
//      $('#details').transition('hide');
//
//      $('#internal').transition('fly right');
//    });
//
//  });
//
//  $('.submit').one('click', function(p) {
//
//    p.preventDefault();
//
//    $('#details').stop();
//  });
//
//});

$(document).ready(function() {

  var ct = 0;

  $('.next').one('click', function(e) {

    e.preventDefault();

    $('#cards').animate('slow', function() {

      if (ct > 0) {
        $('#cards').removeClass('transition visible');
        $('#cards').addClass('transition hidden');

      }
      $('#cards').css('display', 'none');

      $('#card-tile').addClass('disabled');
      $('#details-tile').removeClass('disabled');
      $("#details").transition('fly right');
      $('body').css('background-color', '#06000a');
      $('#details button').removeClass('inverted violet');
      $('#details button').addClass('inverted blue');
      ct++;

    });

  });

  $('.submit').one('click', function(p) {

    p.preventDefault();

    $('#details').stop();
  });

});