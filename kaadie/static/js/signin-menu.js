$(document).ready(function() {
  $(".toggle-close").click(function () {
    $("#side-tab").addClass("hide-menu");
    $(".toggle-menu").addClass("opacity-one");
    $("#designpage").addClass("remove-shift");
  });

  $(".toggle-menu").click(function () {
    $("#side-tab").removeClass("hide-menu");
    $(".toggle-menu").removeClass("opacity-one");
    $("#designpage").removeClass("remove-shift");
  });
});
