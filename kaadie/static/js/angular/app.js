/**
 * Created by stikks-workstation on 2/10/17.
 */
var app = angular.module('kaadie', ['kaadie.controllers', 'kaadie.models','angularUtils.directives.dirPagination']);

// app.factory('httpRequestInterceptor', function () {
//   return {
//     request: function (config) {
//
//       config.headers['HTTP_X_CSRFTOKEN'] = 'Basic d2VudHdvcnRobWFuOkNoYW5nZV9tZQ==';
//       config.headers['Accept'] = 'application/json;odata=verbose';
//
//       return config;
//     }
//   };
// });

app.config(function ($httpProvider, $interpolateProvider) {
    // $httpProvider.interceptors.push('httpRequestInterceptor');
    // $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    // $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');

    // $httpProvider.defaults.headers.common["X-CSRFToken"] = window.csrf_token;
});
