/**
 * Created by stikks-workstation on 2/10/17.
 */

Date.prototype.yyyymmdd = function () {
    var mm = this.getMonth() + 1; // getMonth() is zero-based
    var dd = this.getDate();

    return [this.getFullYear(),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd
    ].join('-');
};

var MD5 = function(s){function L(k,d){return(k<<d)|(k>>>(32-d))}function K(G,k){var I,d,F,H,x;F=(G&2147483648);H=(k&2147483648);I=(G&1073741824);d=(k&1073741824);x=(G&1073741823)+(k&1073741823);if(I&d){return(x^2147483648^F^H)}if(I|d){if(x&1073741824){return(x^3221225472^F^H)}else{return(x^1073741824^F^H)}}else{return(x^F^H)}}function r(d,F,k){return(d&F)|((~d)&k)}function q(d,F,k){return(d&k)|(F&(~k))}function p(d,F,k){return(d^F^k)}function n(d,F,k){return(F^(d|(~k)))}function u(G,F,aa,Z,k,H,I){G=K(G,K(K(r(F,aa,Z),k),I));return K(L(G,H),F)}function f(G,F,aa,Z,k,H,I){G=K(G,K(K(q(F,aa,Z),k),I));return K(L(G,H),F)}function D(G,F,aa,Z,k,H,I){G=K(G,K(K(p(F,aa,Z),k),I));return K(L(G,H),F)}function t(G,F,aa,Z,k,H,I){G=K(G,K(K(n(F,aa,Z),k),I));return K(L(G,H),F)}function e(G){var Z;var F=G.length;var x=F+8;var k=(x-(x%64))/64;var I=(k+1)*16;var aa=Array(I-1);var d=0;var H=0;while(H<F){Z=(H-(H%4))/4;d=(H%4)*8;aa[Z]=(aa[Z]| (G.charCodeAt(H)<<d));H++}Z=(H-(H%4))/4;d=(H%4)*8;aa[Z]=aa[Z]|(128<<d);aa[I-2]=F<<3;aa[I-1]=F>>>29;return aa}function B(x){var k="",F="",G,d;for(d=0;d<=3;d++){G=(x>>>(d*8))&255;F="0"+G.toString(16);k=k+F.substr(F.length-2,2)}return k}function J(k){k=k.replace(/rn/g,"n");var d="";for(var F=0;F<k.length;F++){var x=k.charCodeAt(F);if(x<128){d+=String.fromCharCode(x)}else{if((x>127)&&(x<2048)){d+=String.fromCharCode((x>>6)|192);d+=String.fromCharCode((x&63)|128)}else{d+=String.fromCharCode((x>>12)|224);d+=String.fromCharCode(((x>>6)&63)|128);d+=String.fromCharCode((x&63)|128)}}}return d}var C=Array();var P,h,E,v,g,Y,X,W,V;var S=7,Q=12,N=17,M=22;var A=5,z=9,y=14,w=20;var o=4,m=11,l=16,j=23;var U=6,T=10,R=15,O=21;s=J(s);C=e(s);Y=1732584193;X=4023233417;W=2562383102;V=271733878;for(P=0;P<C.length;P+=16){h=Y;E=X;v=W;g=V;Y=u(Y,X,W,V,C[P+0],S,3614090360);V=u(V,Y,X,W,C[P+1],Q,3905402710);W=u(W,V,Y,X,C[P+2],N,606105819);X=u(X,W,V,Y,C[P+3],M,3250441966);Y=u(Y,X,W,V,C[P+4],S,4118548399);V=u(V,Y,X,W,C[P+5],Q,1200080426);W=u(W,V,Y,X,C[P+6],N,2821735955);X=u(X,W,V,Y,C[P+7],M,4249261313);Y=u(Y,X,W,V,C[P+8],S,1770035416);V=u(V,Y,X,W,C[P+9],Q,2336552879);W=u(W,V,Y,X,C[P+10],N,4294925233);X=u(X,W,V,Y,C[P+11],M,2304563134);Y=u(Y,X,W,V,C[P+12],S,1804603682);V=u(V,Y,X,W,C[P+13],Q,4254626195);W=u(W,V,Y,X,C[P+14],N,2792965006);X=u(X,W,V,Y,C[P+15],M,1236535329);Y=f(Y,X,W,V,C[P+1],A,4129170786);V=f(V,Y,X,W,C[P+6],z,3225465664);W=f(W,V,Y,X,C[P+11],y,643717713);X=f(X,W,V,Y,C[P+0],w,3921069994);Y=f(Y,X,W,V,C[P+5],A,3593408605);V=f(V,Y,X,W,C[P+10],z,38016083);W=f(W,V,Y,X,C[P+15],y,3634488961);X=f(X,W,V,Y,C[P+4],w,3889429448);Y=f(Y,X,W,V,C[P+9],A,568446438);V=f(V,Y,X,W,C[P+14],z,3275163606);W=f(W,V,Y,X,C[P+3],y,4107603335);X=f(X,W,V,Y,C[P+8],w,1163531501);Y=f(Y,X,W,V,C[P+13],A,2850285829);V=f(V,Y,X,W,C[P+2],z,4243563512);W=f(W,V,Y,X,C[P+7],y,1735328473);X=f(X,W,V,Y,C[P+12],w,2368359562);Y=D(Y,X,W,V,C[P+5],o,4294588738);V=D(V,Y,X,W,C[P+8],m,2272392833);W=D(W,V,Y,X,C[P+11],l,1839030562);X=D(X,W,V,Y,C[P+14],j,4259657740);Y=D(Y,X,W,V,C[P+1],o,2763975236);V=D(V,Y,X,W,C[P+4],m,1272893353);W=D(W,V,Y,X,C[P+7],l,4139469664);X=D(X,W,V,Y,C[P+10],j,3200236656);Y=D(Y,X,W,V,C[P+13],o,681279174);V=D(V,Y,X,W,C[P+0],m,3936430074);W=D(W,V,Y,X,C[P+3],l,3572445317);X=D(X,W,V,Y,C[P+6],j,76029189);Y=D(Y,X,W,V,C[P+9],o,3654602809);V=D(V,Y,X,W,C[P+12],m,3873151461);W=D(W,V,Y,X,C[P+15],l,530742520);X=D(X,W,V,Y,C[P+2],j,3299628645);Y=t(Y,X,W,V,C[P+0],U,4096336452);V=t(V,Y,X,W,C[P+7],T,1126891415);W=t(W,V,Y,X,C[P+14],R,2878612391);X=t(X,W,V,Y,C[P+5],O,4237533241);Y=t(Y,X,W,V,C[P+12],U,1700485571);V=t(V,Y,X,W,C[P+3],T,2399980690);W=t(W,V,Y,X,C[P+10],R,4293915773);X=t(X,W,V,Y,C[P+1],O,2240044497);Y=t(Y,X,W,V,C[P+8],U,1873313359);V=t(V,Y,X,W,C[P+15],T,4264355552);W=t(W,V,Y,X,C[P+6],R,2734768916);X=t(X,W,V,Y,C[P+13],O,1309151649);Y=t(Y,X,W,V,C[P+4],U,4149444226);V=t(V,Y,X,W,C[P+11],T,3174756917);W=t(W,V,Y,X,C[P+2],R,718787259);X=t(X,W,V,Y,C[P+9],O,3951481745);Y=K(Y,h);X=K(X,E);W=K(W,v);V=K(V,g)}var i=B(Y)+B(X)+B(W)+B(V);return i.toLowerCase()};

var app = angular.module('kaadie.controllers', ['kaadie.models', 'infinite-scroll']);

app.controller('BaseController', function ($scope, $rootScope, $timeout, Card, SavedCard, SentCard, $q, Contact, Account) {

    $rootScope.form = {
        'sending': false,
        'receipt': null,
        'receipt_phone': null,
        'message': null,
        'card_image': null,
        'as_json': true
    };
    $rootScope.message = {error: null, success: null};
    $scope.artist_request = {portfolio: null, error: null, success: null};

    $rootScope.account_form = {
        'first_name': null,
        'last_name': null,
        "address": null,
        'city': null,
        'phone': null,
        'country': null,
        'birthday': null
    };
    $rootScope.data = {
        'saved_cards': [],
        'banners': [],
        'featured_cards': [],
        'categories': [],
        'notifications': [],
        'saved_ids': [],
        'account_id': '0',
        'account_name': '',
        'notification_count': 0
    };

     $scope.load_notifications = function () {
        $.get('/ajax/notifications', function (data, status) {
            if (status == 'success') {
                $timeout(function () {
                    $scope.$apply(function () {
                        $rootScope.data.notification_count = data.messages.length;
                    })
                });
            }
        })
    };

    function webNotify(noteObject) {
        // show web notifications

        // check if browser supports notifications
        if(!"Notification" in window) {
            console.log("This browser does not support notifications.");
        }

        // check if permission is granted
        else {

            var imageLink = noteObject.image;

            if (!imageLink) {
                imageLink = 'https://s3-us-west-2.amazonaws.com/kaadie/kaddielogo.png';
            }


            if (Notification.permission === 'granted') {
                var _notification = new Notification('Kaadie Notification', {body: noteObject.message, icon: imageLink});
                window.navigator.vibrate(500);
            }
            else if (Notification.permission !== 'granted') {
                Notification.requestPermission(function (permission) {
                    if(!('permission' in Notification)) {
                        Notification.permission = permission;
                    }

                    if (permission === 'granted') {
                        var _notification = new Notification('Kaadie Notification',
                            {
                                //body: '<a href="' + noteObject.link + '">' + noteObject.message + '</a>',
                                body: noteObject.message,
                                icon: imageLink,
                                sound: 'audio/alert.mp3',
                                vibrate: [200, 100, 200]
                            });
                        window.navigator.vibrate(500);
                    }
                })
            }
        }
    }

    var pusher = new Pusher('fd9fe2dff7bcd0031b36', {
        cluster: 'eu'
    });

    $scope.init = function (account_id, name) {
        $scope.data.account_id = account_id;
        $scope.data.account_name = name;
        $scope.form.sender_name = name || null;
        $scope.load_notifications();
         var channel = pusher.subscribe($scope.data.account_id);
        //var channel = pusher.subscribe('testing123');
        channel.bind('broadcast', function (note) {
            webNotify(note);
            $scope.load_notifications()
        });
    };

    $scope.share = function (card_id, image_url, slug, title, description) {
        $scope.selected_card = {pk: card_id, title: title, slug: slug, image_url: image_url, description: description};
        $scope.selected_card.link = location.origin + '/cards/' + $scope.selected_card.slug;
        $scope.selected_card.encoded_link = encodeURIComponent(location.origin + '/cards/' + $scope.selected_card.slug);
        $scope.selected_image_url = image_url;
        // $timeout(function () {
        //     var qx = Card.get({id: card_id});
        //     qx.$promise.then(function (result) {
        //         $scope.selected_card = result;
        //         $scope.selected_card.link = location.origin + '/cards/' + $scope.selected_card.slug;
        //         console.log($scope.selected_card.link);
        //         $scope.selected_card.encoded_link = encodeURIComponent(location.origin + '/cards/' + $scope.selected_card.slug)
        //     })
        // });
    };

    var incr_shared_count = function (platform, hash_code) {
        var x = Card.get({id: $scope.selected_card.pk});
        x.$promise.then(function (card) {
            card.$increment_shared_count({
                id: $scope.selected_card.pk, resource: 'increment_shared_count', hash_code: hash_code,
                platform: platform
            }, function (data, status) {
            })
        });
    };

    function addMeta(title, description, image_url) {
        $('head').append('<meta property="og:site_name" content="Kaadie | Virtual Cards">' +
            '<meta property="og:title" content="' + title + '">' +
            '<meta property="og:description" content="' + description + '">' +
            '<meta property="og:image" itemprop="image" content="' + image_url + '">' +
            '<meta property="og:type" content="website">')
    }

     function addFBMeta(slug, title, description, image_url) {
         var card_url = window.location.origin + '/cards/' + slug;
         $('head').append('<meta property="og:url" content="' + card_url + '">' +
             '<meta property="og:type" content="website" />' +
             '<meta property="og:title" content="' + title + '">' +
             '<meta property="og:description" content="' + description + '">' +
             '<meta property="og:image"  content="' + image_url + '">'
         )
     }


    $scope.shareFB = function () {
        addFBMeta($scope.selected_card.slug, $scope.selected_card.title, $scope.selected_card.description, $scope.selected_card.image_url);
        var timestamp = new Date().getTime();
        var code_ = (MD5($scope.data.account_id + $scope.selected_card.slug + 'facebook' + timestamp));
        var encoded_ = encodeURIComponent(location.origin + '/shared/' + code_);
        window.open('https://www.facebook.com/dialog/share?app_id=1329121063813084&href=' + encoded_ + '&display=iframe&redirect_uri=' + encodeURIComponent(location.origin) + '&picture=' + encodeURIComponent($scope.selected_card.image_url) + '&caption=Kaadie | ' + $scope.selected_card.title + '&name=' + $scope.selected_card.title + '&description=' + $scope.selected_card.description);
        incr_shared_count('facebook', code_);
        return false;
    };

    $scope.shareIn = function () {
        var timestamp = new Date().getTime();
        var code_ = (MD5($scope.data.account_id + $scope.selected_card.slug + 'linkedIn' + timestamp));
        var encoded_ = encodeURIComponent(location.origin + '/shared/' + code_);
        window.open('http://www.linkedin.com/shareArticle?mini=true&url=' + encoded_ + '&title=' + encodeURIComponent($scope.selected_card.title));
        incr_shared_count('linkedIn', code_);
        return false;
    };

    $scope.shareTweet = function () {
        var timestamp = new Date().getTime();
        var code_ = (MD5($scope.data.account_id + $scope.selected_card.slug + 'twitter' + timestamp));
        var encoded_ = encodeURIComponent(location.origin + '/shared/' + code_);
        window.open('https://twitter.com/intent/tweet?text=' + encodeURIComponent($scope.selected_card.title) + ':%20' + encoded_, 'height=200,width=150');
        incr_shared_count('twitter', code_);
        return false;
    };

    $scope.shareGPlus = function () {
        var timestamp = new Date().getTime();
        var code_ = (MD5($scope.data.account_id + $scope.selected_card.slug + 'google-plus' + timestamp));
        var encoded_ = encodeURIComponent(location.origin + '/shared/' + code_);
        window.open('https://plus.google.com/share?url=' + encoded_ + '&title=' + $scope.selected_card.title);
        incr_shared_count('google-plus', code_);
        return false;
    };

    $scope.shareMail = function () {
        var timestamp = new Date().getTime();
        var code_ = (MD5($scope.data.account_id + $scope.selected_card.slug + 'mail' + timestamp));
        var encoded_ = encodeURIComponent(location.origin + '/shared/' + code_);
        window.open('mailto:?subject=' + encodeURIComponent('Kaadie Card - ' + $scope.selected_card.title) + '&body=' + encoded_);
        incr_shared_count('mail', code_);
        return false;
    };

    $scope.sharePinterest = function () {
        var timestamp = new Date().getTime();
        var code_ = (MD5($scope.data.account_id + $scope.selected_card.slug + 'pinterest' + timestamp));
        var encoded_ = encodeURIComponent(location.origin + '/shared/' + code_);
        window.open('https://www.pinterest.com/pin/create/button/?url=' + encoded_ + '&media=' + encoded_ + '&description=' + encodeURIComponent($scope.selected_card.description) + '&hashtags=' + encodeURIComponent($scope.selected_card.title));
        incr_shared_count('pinterest', code_);
        return false
    };

    $scope.shareWhatsapp = function(is_pc) {
        addMeta($scope.selected_card.title, $scope.selected_card.description, $scope.selected_card.image_url);
        var timestamp = new Date().getTime();
        var code_ = (MD5($scope.data.account_id + $scope.selected_card.slug + 'whatsapp' + timestamp));
        var encoded_ = encodeURIComponent(location.origin + '/shared/' + code_);
        if (is_pc) {
            window.open('https://web.whatsapp.com/send?text=' + 'I just shared a special ' + $scope.selected_card.title + ' card with you.' + ' Click here to see --> ' + encoded_);
        }
        else {
            window.open('https://api.whatsapp.com/send?text=' + 'I just shared a special ' + $scope.selected_card.title + ' card with you.' + ' Click here to see --> ' + encoded_);
        }
        incr_shared_count('whatsapp', code_);
        return false
    };

    $scope.zoomCard = function (image_url) {
        $scope.zoomed_url = image_url
    };

    $scope.saveCard = function (card_id) {
        var card = new SavedCard({
            'user_id': $scope.data.account_id,
            'card_id': card_id
        });
        card.$save(function (data, status) {
            $rootScope.data.saved_ids.push(card_id);
        })
    };

    $scope.sendCard = function () {
        $scope.form.receipt = $('#email_recipients').val().join();
        $scope.form.card_id = $scope.selected_card.pk;
        $scope.form.receipt_phone = $('#phone_recipients').val();
        if (!$scope.form.receipt && !$scope.form.receipt_phone) {
            $scope.message.error = 'Reciepient(s) missing!!!'
        }
        else {
            $scope.form.sending = true;
            var _url = location.origin + '/cards/' + $scope.selected_card.slug + '/send';
            // if (location.pathname != '/') {
            //     _url = location.pathname + '/cards/' + $scope.selected_card.slug + '/send';
            // }
            $.post(_url, $scope.form, function (data, status) {
                if (data.status == 'success') {
                    $scope.$apply(function () {
                        $scope.form.sending = false;
                        $scope.message.success = data.message;
                    });
                    $timeout(function () {
                        location.href = data.redirect_url
                    }, 2000)
                }
                else {
                    $scope.$apply(function () {
                        $scope.form.sending = false;
                        $scope.message.error = data.message;
                    });
                }
            })
        }
    };

    $scope.requestPermission = function () {
        $.ajax({
            url: '/artist/request',
            type: "POST",
            data: $scope.artist_request,
            success: function (response) {
                if (response.status == 'error') {
                    $scope.$apply(function () {
                        $scope.artist_request.error = response.errors
                    })
                }
                else {
                    $scope.$apply(function () {
                        $scope.artist_request.success = "Your request has been sent."
                    });
                    $timeout(function () {
                        location.reload();
                    }, 100)
                }
            }
        });
    }

});


app.controller('IndexController', function ($scope, $rootScope) {
    // $scope.data = $rootScope.data;

    $scope.cards = [];
    $scope.offset = 0;
    $scope.isBusy = false;

    $scope.initialize = function (saved_ids) {
        $rootScope.data.saved_ids = saved_ids.split(',');
    };

    $scope.initialize_send = function (saved_ids, card) {
        $rootScope.selected_card = JSON.parse(card);
        $rootScope.selected_card.link = location.origin + '/cards/' + $rootScope.selected_card.slug;
        $rootScope.selected_card.encoded_link = encodeURIComponent(location.origin + '/cards/' + $rootScope.selected_card.slug);
        $rootScope.data.saved_ids = saved_ids.split(',');
    };

    $scope.can_save = function (_id) {
        return $rootScope.data.saved_ids.indexOf(_id) == -1;
    };
});


app.controller('DashBoardController', function ($scope, $timeout, $q, Card) {

    var load_cards = function () {
        var deferred = $q.defer();

        $timeout(function () {
            var entry = Card.query({'user_id': $scope.data.account.id});
            entry.$promise.then(function (result) {
                $scope.data.cards = result
            })
        });

        return deferred.promise;
    };

    $scope.init = function (user_id, csrf_token) {
        $scope.data = {'cards': [], 'account': {'id': user_id}, 'csrf_token': csrf_token};
        startParallel();
    };

    var startParallel = function () {

        $q.all([load_cards()]).then(
            function (successResult) {
            }, function (failureReason) {
                // renew()
            }
        );
    };

    $scope.save = function () {
        var card = new Card({
            'user': $scope.data.account.id,
            'title': 'Test Card',
            'description': 'Test description',
            'image_url': 'http://http://cdn.someecards.com/someecards/filestorage/zv13o3over-40-greeting-card-time-birthday-ecards-someecards.png'
        });
        card.$save(function (data, status) {
            console.log(data);
            console.log(status);
        })
    };
});

app.controller('HomeController', function ($scope, $rootScope, $timeout, SavedCard, ReceivedCard, SentCard, $q, Contact) {

    $scope.can_save = function (_id) {
        return $rootScope.data.saved_ids.indexOf(_id) == -1
    };

    $scope.deleteContact = function (contact_id) {
        var x = Contact.get({id: contact_id});
        x.$promise.then(function (contact) {
            contact.$delete({id: contact_id}, function (data, status) {
                location.reload()
            })
        });
    };

    $scope.selectContact = function (contact_id) {
        $timeout(function () {
            var qx = Contact.get({id: contact_id});
            qx.$promise.then(function (result) {
                $scope.selected_contact = result;
                if (result.address.first_name == null) {
                    $scope.selected_contact.first_name = result.address.full_name
                }
                else {
                    $scope.selected_contact.first_name = result.address.first_name
                }
                $scope.selected_contact.last_name = result.address.last_name;
                $scope.selected_contact.phone = parseInt(result.address.phone);
                $scope.selected_contact.date_of_birth = new Date(result.address.date_of_birth);
                $scope.selected_contact.city = result.address.city;
                $scope.selected_contact.country = result.address.country;
                $scope.selected_contact.email = result.email;
                $scope.selected_contact.address = result.address.address;
            })
        });
    };

    $scope.updateContact = function () {

        var _data = jQuery.extend(true, {}, $scope.selected_contact);
        _data.date_of_birth = $scope.selected_contact.date_of_birth.yyyymmdd();

        $scope.selected_contact.$update({id: _data.pk}, _data, function (data, status) {
        });
        location.reload()
    };

    $scope.updateAccount = function () {
        $timeout(function () {
            var qx = Account.get({id: $scope.data.account_id});
            qx.$promise.then(function (account) {
                account.$update({id: $scope.data.account_id}, $scope.account_form, function (data, status) {
                    console.log(data);
                    console.log(status)
                });
                // location.reload()
            })
        });
    };

    var load_saved_cards = function () {
        var deferred = $q.defer();

        $timeout(function () {
            var entry = SavedCard.query({'user_id': $scope.data.account.id});
            entry.$promise.then(function (result) {
                $scope.data.saved_cards = result

            })
        });

        return deferred.promise;
    };

    var load_sent_cards = function () {
        var deferred = $q.defer();

        $timeout(function () {
            var entry = SentCard.query();
            entry.$promise.then(function (result) {
                $scope.data.sent_cards = result

            })
        });

        return deferred.promise;
    };

    var load_received_cards = function () {
        var deferred = $q.defer();

        $timeout(function () {
            var entry = ReceivedCard.query();
            entry.$promise.then(function (result) {
                $scope.data.received_cards = result

            })
        });

        return deferred.promise;
    };

    $scope.init = function (saved_ids) {
        $scope.data = {
            'saved_cards': [],
            'sent_cards': [],
            'received_cards': [],
            'account': $rootScope.data.account_id
        };
        $rootScope.saved_ids = saved_ids.split(',');
        // startParallel()
    };

    var startParallel = function () {

        $q.all([load_saved_cards(), load_sent_cards(), load_received_cards()]).then(
            function (successResult) {
            }, function (failureReason) {
                // renew()
            }
        );
    };
});

app.controller('LandingController', function($scope, $rootScope) {

     var loadData = function (url) {
         $.get(url, function (data, status) {
             if (status == 'success') {
                 $scope.$apply(function () {
                     $scope.data.categories = data.categories;
                 })
             }
         })
     };

    $scope.init = function () {
        loadData('/load/home');
    };

    $scope.loadMore = function () {
        if ($scope.isBusy === true){ return;}
        $scope.isBusy = true;
        $.get('/load/cards', function (data, status) {
             if (status == 'success') {
                 var newList = data.cards;
                 for (var info=0; info < newList.length; info++){
                     $scope.$apply(function () {
                        $scope.cards.push(newList[info]);
                     })
                 }
                 $scope.$apply(function () {
                    $scope.offset+=newList.length;
                    $scope.isBusy = false;
                 })
             }
         });
    }
});