/**
 * Created by stikks-workstation on 2/10/17.
 */

var app = angular.module('kaadie.models', ['ngResource']);

// app.config(function (csrfCDProvider) {
//     csrfCDProvider.setHeaderName('X-CSRFToken');
//     csrfCDProvider.setCookieName('CSRFToken');
//     csrfCDProvider.setAllowedMethods(['GET', 'POST', 'HEAD']);
// });

app.factory('Card', function ($resource, $window) {
    return $resource('/v1/cards/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        },
        increment_shared_count: {
            url: '/v1/cards/:id',
            method: 'PUT'
        }
    });
});

app.factory('SavedCard', function ($resource) {
    return $resource('/v1/saved-cards/:id/', { id: '@_id' }, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('ReceivedCard', function ($resource) {
    return $resource('/v1/received-cards/:id/', { id: '@_id' }, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('SentCard', function ($resource) {
    return $resource('/v1/sent-cards/:id', { id: '@_id' }, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

// app.factory('Card', function ($resource) {
//     return {
//         cards: function (csr_token) {
//             return $resource('/v1/cards/:id/', { id: '@_id' }, {
//                 update: {
//                   method: 'PUT' // this method issues a PUT request
//                 },
//                 save: {
//                     method: 'POST',
//                     headers: {
//                         'HTTP_X_CSRFTOKEN': csr_token,
//                         'X-CSRFToken': csr_token
//                     }
//                 }
//             })
//         }
//     }
// });

app.factory('Account', function ($resource) {
    return $resource('/v1/accounts/:id/', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Address', function ($resource) {
    return $resource('/v1/addresses/:id/', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Category', function ($resource) {
    return $resource('/v1/categories/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Contact', function ($resource) {
    return $resource('/v1/contacts/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});

app.factory('Message', function ($resource) {
    return $resource('/v1/messages/:id', { id: '@_id'}, {
        update: {
          method: 'PUT' // this method issues a PUT request
        }
    });
});