$(document).ready(function () {

    // fix menu when passed
    $('.indexpage')
        .visibility({
            once: false,
            onBottomPassed: function () {
                $('.fixed.menu').transition('fade in');
            },
            onBottomPassedReverse: function () {
                $('.fixed.menu').transition('fade out');
            }
        });

    // create sidebar and attach to menu open
    $('.ui.sidebar').sidebar('attach events', '.toc.item');

    // initializing popup/tool tip
    $('.icon-popup')
        .popup({
            inline: true
        });

    // initializing dimmer on cards when you hover
    $('.special.cards .image').dimmer({
        on: 'hover'
    });

    // initializing share button when you click
    $(".share-icon").click(function () {
        $(".ui.share.small.modal").modal("show");
    });

    // to initialize artist modal
    $(".artist_btn").click(function () {
        $('.ui.artist.small.modal').modal('show');
    });

    $('#app_message').on('click', function () {
        $(this).closest('.message').transition('fade');
    });
    $(".owl-carousel").owlCarousel({
        autoplay: true,
        autoplayTimeout: 7000,
        autoplayHoverPause: true,
        items: 1,
        loop: true,
        margin: 1,
        responsiveRefreshRate: 200,
        slideSpeed: 2000
    });

    $('.ui.dropdown').dropdown({transition: "drop", on: "hover"});
    // initializing dimmer on cards when you hover

});
