"""kaadie URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from django.views.static import serve

from rest_framework import routers

from . import views, viewsets
from . import api

router = routers.SimpleRouter(trailing_slash=False)
router.register(r'cards', viewsets.CardViewSet)
router.register(r'saved-cards', viewsets.SavedCardViewSet)
router.register(r'sent-cards', viewsets.SentCardViewSet)
router.register(r'received-cards', viewsets.ReceivedCardViewSet)
router.register(r'sections', viewsets.SectionViewSet)
router.register(r'categories', viewsets.CategoryViewSet)
router.register(r'contacts', viewsets.ContactViewSet)

router.register(r'accounts', viewsets.AccountViewSet)
router.register(r'addresses', viewsets.AddressViewSet)
router.register(r'banners', viewsets.BannerViewSet)
# router.register(r'comments', api.CommentAPIView)

urlpatterns = [
                  url(r'^v1/', include(router.urls)),
                  url(r'^$', views.IndexView.as_view(), name='index'),
                  url(r'^load/home$', views.build_index, name='build_home'),
                  url(r'^load/cards$', views.load_cards, name='load_cards'),
                  url(r'^ajax/notifications$', views.ajax_notifications, name='ajax_notifications'),
                  url(r'^notification/(?P<pk>\d+)/mark', views.mark_as_read, name='mark_read'),
                  url(r'^artist/request$', views.artist_request, name='artist_request'),
                  url(r'^test/404$', views.custom_page_not_found, name='test_404'),

                  # api endpoints
                  url(r'^v1/register/brand$', api.BrandRegisterAPIView.as_view(), name='api_business_register'),
                  url(r'^v1/login$', api.LoginAPIView.as_view(), name='api_login'),
                  url(r'^v1/register$', api.RegisterAPIView.as_view(), name='api_register'),
                  url(r'^v1/logout$', api.LogoutAPIView.as_view(), name='api_logout'),
                  url(r'^v1/forgot$', api.ForgotPasswordAPIView.as_view(), name='api_forgot'),
                  url(r'^v1/reset$', api.ResetPasswordAPIView.as_view(), name='api_reset'),
                  url(r'^v1/profile$', api.ProfileAPIView.as_view(), name='api_profile'),
                  url(r'^v1/send/cards$', api.SendCardAPIView.as_view(), name='api_send_card'),
                  url(r'^v1/premium$', api.PremiumAPIView.as_view(), name='api_premium'),
                  url(r'^v1/logs$', api.ActivityAPIView.as_view(), name='api_logs'),
                  url(r'^v1/comments/(?P<pk>\d+)$', api.CommentAPIView.as_view(), name='api_comments'),
                  url(r'^v1/defaults/sections/add$', api.AddSectionAPIView.as_view(), name='api_defaults_add'),
                  url(r'^v1/defaults/sections/(?P<slug>[\w\d-]+$)', api.DeleteSectionAPIView.as_view(),
                      name='api_defaults_add'),

                  url(r'^v1/sms$', api.SMSAPIView.as_view()),
                  url(r'^v1/email$', api.EmailAPIView.as_view()),

                  url(r'^accounts/login/$', views.LoginView.as_view(), name='login'),
                  url(r'^(?P<sub>[\w\d-]+)/accounts/login/$', views.LoginView.as_view(), name='login'),

                  url(r'^twitter/login$', views.twitter_login, name='twitter_login'),
                  url(r'^(?P<sub>[\w\d-]+)/twitter/login/$', views.twitter_login, name='twitter_login'),

                  url(r'^twitter/authenticate$', views.twitter_authenticate, name='twitter_authenticate'),
                  url(r'^(?P<sub>[\w\d-]+)/twitter/authenticate$', views.twitter_authenticate,
                      name='twitter_authenticate'),

                  url(r'^facebook/login$', views.facebook_login, name='facebook_login'),
                  url(r'^(?P<sub>[\w\d-]+)/facebook/login$', views.facebook_login, name='facebook_login'),

                  url(r'^facebook/callback$', views.facebook_callback, name='facebook_callback'),
                  url(r'^(?P<sub>[\w\d-]+)/facebook/callback$', views.facebook_callback, name='facebook_callback'),

                  url(r'^logout$', views.logout_view, name='logout'),
                  url(r'^(?P<sub>[\w\d-]+)/logout$', views.logout_view, name='logout'),

                  url(r'^register$', views.RegisterView.as_view(), name='register'),
                  url(r'^(?P<sub>[\w\d-]+)/register$', views.RegisterView.as_view(), name='register'),

                  url(r'^register/brand$', views.RegisterBrandView.as_view(), name='register_brand'),
                  url(r'^(?P<sub>[\w\d-]+)/register/brand$', views.RegisterBrandView.as_view(), name='register_brand'),

                  url(r'^request/artist$', views.ArtistRequestView.as_view(), name='request_rights'),
                  url(r'^(?P<sub>[\w\d-]+)/request/artist$', views.ArtistRequestView.as_view(), name='request_rights'),

                  url(r'^register/artist$', views.ArtistRegisterView.as_view(), name='artist_register'),
                  url(r'^(?P<sub>[\w\d-]+)/register/artist$', views.ArtistRegisterView.as_view(), name='artist_register'),

                  url(r'^verify$', views.verify, name='verify'),
                  url(r'^(?P<sub>[\w\d-]+)/verify$', views.verify, name='verify'),

                  url(r'^cards/create$', views.CreateCardView.as_view(), name='create_card'),
                  url(r'^(?P<sub>[\w\d-]+)/cards/create$', views.CreateCardView.as_view(), name='create_card'),

                  url(r'^cards/(?P<slug>[\w\d-]+)$', views.view_card, name='card'),
                  url(r'^(?P<sub>[\w\d-]+)/cards/(?P<slug>[\w\d-]+)$', views.view_card, name='card'),

                  url(r'^cards/(?P<slug>[\w\d-]+)/send$', views.SendCardView.as_view(), name='send_card'),
                  url(r'^(?P<sub>[\w\d-]+)/cards/(?P<slug>[\w\d-]+)/send$', views.SendCardView.as_view(),
                      name='send_card'),

                  url(r'^cards/(?P<slug>[\w\d-]+)/personalize', views.PersonalizeCardView.as_view(),
                      name='personalize_card'),
                  url(r'^(?P<sub>[\w\d-]+)/cards/(?P<slug>[\w\d-]+)/personalize', views.PersonalizeCardView.as_view(),
                      name='personalize_card'),

                  url(r'^contacts/(?P<pk>\d+)/schedule$', views.schedule_reminder, name='schedule_reminder'),
                  url(r'^(?P<sub>[\w\d-]+)/contacts/(?P<pk>\d+)/schedule$', views.schedule_reminder,
                      name='schedule_reminder'),

                  url(r'^cards/(?P<slug>[\w\d-]+)/edit$', views.EditCardView.as_view(), name='edit_card'),
                  url(r'^(?P<sub>[\w\d-]+)/cards/(?P<slug>[\w\d-]+)/edit$', views.EditCardView.as_view(),
                      name='edit_card'),

                  url(r'^send/cards$', views.send_card, name='card_send'),
                  url(r'^(?P<sub>[\w\d-]+)/send/cards$', views.send_card, name='card_send'),

                  url(r'^sent/(?P<id>[\w\d-]+)$', views.sent_card, name='sent_card'),
                  url(r'^(?P<sub>[\w\d-]+)/sent/(?P<id>[\w\d-]+)$', views.sent_card, name='sent_card'),

                  url(r'^shared/(?P<id>[\w\d-]+)$', views.shared_card, name='shared_card'),
                  url(r'^(?P<sub>[\w\d-]+)/shared/(?P<id>[\w\d-]+)$', views.shared_card, name='shared_card'),

                  url(r'^designs/(?P<pk>\d+)/save$', views.save_designs, name='save_designs'),
                  url(r'^(?P<sub>[\w\d-]+)/designs/(?P<pk>\d+)/save', views.save_designs, name='save_designs'),

                  url(r'^post/(?P<id>[\w\d-]+)$', views.post_comment, name='post_comment'),
                  url(r'^(?P<sub>[\w\d-]+)/post/(?P<id>[\w\d-]+)$', views.post_comment, name='post_comment'),

                  url(r'^forgot$', views.ForgotView.as_view(), name='forgot'),
                  url(r'^(?P<sub>[\w\d-]+)/forgot$', views.ForgotView.as_view(), name='forgot'),

                  url(r'^change$', views.ChangePasswordView.as_view(), name='change'),
                  url(r'^(?P<sub>[\w\d-]+)/change$', views.ChangePasswordView.as_view(), name='change'),

                  url(r'^change/email$', views.ChangeEmailView.as_view(), name='change_email'),
                  url(r'^(?P<sub>[\w\d-]+)/change/email$', views.ChangeEmailView.as_view(), name='change_email'),

                  url(r'^home$', views.HomeView.as_view(), name='home'),
                  url(r'^(?P<sub>[\w\d-]+)/home$', views.HomeView.as_view(), name='home'),

                  url(r'^search$', views.SearchView.as_view(), name='search'),
                  url(r'^(?P<sub>[\w\d-]+)/search$', views.SearchView.as_view(), name='search'),

                  url(r'^sections$', views.SectionsView.as_view(), name='sections'),
                  url(r'^(?P<sub>[\w\d-]+)/sections$', views.SectionsView.as_view(), name='sections'),

                  url(r'^sections/(?P<slug>[\w\d-]+)$', views.SectionView.as_view(), name='section'),
                  url(r'^(?P<sub>[\w\d-]+)/sections/(?P<slug>[\w\d-]+)$', views.SectionView.as_view(), name='section'),

                  url(r'^sections/(?P<pk>\d+)(?:/(?P<slug>[\w\d-]+))?/$', views.CategoryView.as_view(),
                      name='category'),
                  url(r'^(?P<sub>[\w\d-]+)/sections/(?P<pk>\d+)(?:/(?P<slug>[\w\d-]+))?/$',
                      views.CategoryView.as_view(), name='category'),

                  url(r'^contacts/upload$', views.upload_contact, name='upload_contact'),
                  url(r'^(?P<sub>[\w\d-]+)/contacts/upload$', views.upload_contact, name='upload_contact'),

                  url(r'^contacts/create$', views.create_contact, name='create_contact'),
                  url(r'^(?P<sub>[\w\d-]+)/contacts/create$', views.create_contact, name='create_contact'),

                  url(r'^profile/update$', views.update_account, name='update_account'),
                  url(r'^(?P<sub>[\w\d-]+)/profile/update$', views.update_account, name='update_account'),

                  url(r'^organization/update$', views.OrganizationView.as_view(), name='update_organization'),
                  url(r'^(?P<sub>[\w\d-]+)/organization/update$', views.OrganizationView.as_view(),
                      name='update_organization'),

                  url(r'^how-it-works$', views.how_it_works, name='how_it_works'),
                  url(r'^(?P<sub>[\w\d-]+)/how-it-works$', views.how_it_works, name='how_it_works'),

                  url(r'^privacy$', views.privacy_policy, name='privacy_policy'),
                  url(r'^(?P<sub>[\w\d-]+)/privacy$', views.privacy_policy, name='privacy_policy'),

                  url(r'^terms$', views.terms_of_use, name='terms_of_use'),
                  url(r'^(?P<sub>[\w\d-]+)/terms$', views.terms_of_use, name='terms_of_use'),

                  url(r'^about$', views.about, name='about'),
                  url(r'^(?P<sub>[\w\d-]+)/about$', views.about, name='about'),

                  url(r'^faq$', views.faq, name='faq'),
                  url(r'^(?P<sub>[\w\d-]+)/faq$', views.faq, name='faq'),

                  url(r'^premium$', views.PremiumView.as_view(), name='premium'),
                  url(r'^(?P<sub>[\w\d-]+)/premium$', views.PremiumView.as_view(), name='premium'),

                  url(r'^report$', views.ReportView.as_view(), name='report'),
                  url(r'^(?P<sub>[\w\d-]+)/report$', views.ReportView.as_view(), name='report'),

                  url(r'^panel$', views.ControlPanel.as_view(), name='control_panel'),
                  url(r'^(?P<sub>[\w\d-]+)/panel$', views.ControlPanel.as_view(), name='control_panel'),

                  # url(r'^new_panel$', views.NewControlPanel.as_view(), name='new_control_panel'),
                  # url(r'^(?P<sub>[\w\d-]+)/new_panel$', views.NewControlPanel.as_view(), name='new_control_panel'),

                  url(r'^notifications$', views.Notifications.as_view(), name='notifications'),
                  url(r'^(?P<sub>[\w\d-]+)/notifications$', views.Notifications.as_view(), name='notifications'),

                  url(r'^check/cards$', views.check_card, name='check_card'),
                  url(r'^(?P<sub>[\w\d-]+)/check/cards$', views.check_card, name='check_card'),

                  url(r'^loaderio-b3e4ccf5651467dea36644602ebb76c4/$', views.loader_io, name='loader_io'),
                  url(r'^emails$', views.email_template, name='email_template'),
                  url(r'^welcome$', views.welcome_template, name='welcome_template'),
                  url(r'^sms$', views.sms_template, name='sms_template'),
                  url(r'^css$', views.css_template, name='css_template'),

                  url(r'^image/update$', views.update_profile_image, name='update_profile_image'),
                  url(r'^(?P<sub>[\w\d-]+)/image/update$', views.update_profile_image, name='update_profile_image'),

                  url(r'^logo/update$', views.update_logo, name='update_logo'),
                  url(r'^(?P<sub>[\w\d-]+)/logo/update$', views.update_logo, name='update_logo'),

                  url(r'^artists$', views.view_artists, name='view_artists'),
                  url(r'^(?P<sub>[\w\d-]+)/artists$', views.view_artists, name='view_artists'),

                  url(r'^artists/(?P<nickname>[\w\d-]+)$', views.view_artist, name='view_artist'),
                  url(r'^(?P<sub>[\w\d-]+)/artists/(?P<nickname>[\w\d-]+)$', views.view_artist, name='view_artist'),

                  url(r'^(?P<sub>[\w\d-]+)$', views.IndexView.as_view(), name='index'),
                  url(r'^tinymce/', include('tinymce.urls')),
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) \
              + [url(r'^images/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT, })]
