from datetime import datetime
import random

from django.dispatch import receiver
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.crypto import get_random_string
from django_user_agents.utils import get_user_agent
from django.db.models import signals, Model
from django.conf import settings

from rest_framework_expiring_authtoken.models import ExpiringToken

from .signals import save_thread, new_user_registered, record_referral
import models
from . import serializers as app_serializers
from services import helper, transport

from kaadie import celery_app


@receiver(user_logged_in, sender=models.User)
def register_login_access(sender, **kwargs):
    """
    register user agent on each login
    :param sender:
    :param kwargs:
    :return:
    """
    request = kwargs['request']
    ua = get_user_agent(request)
    user = kwargs['user']
    if not user.is_staff:
        models.UserLoginDetails.objects.create(user=user.account, is_pc=ua.is_pc, is_tablet=ua.is_tablet,
                                               is_mobile=ua.is_mobile, browser=ua.browser.family,
                                               device=ua.device.family, os=ua.os.family)

    account = app_serializers.AccountSerializer(user.account)

    try:
        helper.record_log(request, 'logged in', account.data, 'object')
    except Exception, e:
        print('==============')
        print(e)
        print('==============')

    return True


@receiver(user_logged_out, sender=models.User)
def delete_authorization_token(sender, **kwargs):
    """
    expire token after user logs out
    :param sender:
    :param kwargs:
    :return:
    """
    user = kwargs['user']
    try:
        token = ExpiringToken.objects.get(user=user)
        token.delete()
    except Exception, e:
        print '==========error==========='
        print e

    return True


def send_welcome_message(sender, **kwargs):
    """
    send welcome message to newly registered user
    :param sender:
    :param kwargs:
    :return:
    """

    account = kwargs['account']
    request = kwargs['request']

    token = models.AuthorizationToken.objects.create(code=get_random_string(40), user=account)

    domain = helper.get_custom_domain(request)

    token_url = '%s%s?token=%s' % (domain, reverse('verify'), token.code)

    try:
        cards = list(
            models.Card.objects.filter(is_active=True, is_public=True, is_featured=True).order_by('?')[:6].all())
        top_cards = cards[:3]
        down_cards = cards[3:]
    except:
        cards = down_cat = top_cat = []

    # send welcome email to new user
    welcome_message = render_to_string('emails/welcome.html', locals())

    # welcome_message = '<p>Hi %s,</p>' \
    #                   '<p> We are glad to have you signed up on kaadie.com.</p>' \
    #                   '<p>Please click ' \
    #                   '<a href="%s"> here</a> to explore our extensive features and start sending cards.</p>' \
    #                   '<p>Best Regards,<br />Kaadie</p>' \
    #                   % (account.name, domain)

    transport.MailService.send_mail('Welcome to Kaadie', 'hello@kaadie.com', 'Kaadie', account.email,
                                    welcome_message, account.name)

    try:
        helper.record_log(request, 'signed up', app_serializers.AccountSerializer(account).data, 'object')
    except Exception, e:
        print('==============')
        print(e)
        print('==============')

    return True


def received_card_handler(sender, **kwargs):
    """signal intercept for user_login"""
    user = kwargs['user']
    emails = kwargs.get('emails', [])
    phones = kwargs.get('phones', [])

    email_contacts = filter(lambda x: x is not None, [c.email for c in user.account.contacts.all()])
    phone_contacts = filter(lambda x: x is not None, [c.phone for c in user.account.contacts.all()])

    for email in emails:
        if email not in email_contacts:
            account = models.User.objects.filter(username=email).first()
            address = models.Address.objects.create(full_name=email.split('@')[0])
            is_active = True if account else False
            models.Contact.objects.create(address=address, user=user.account, is_active=is_active, email=email)

    for phone in phones:
        if phone not in phone_contacts and len(phone) != 0:
            address = models.Address.objects.create(full_name=phone, phone=phone)
            models.Contact.objects.create(address=address, user=user.account, is_active=False)

    return True


@receiver(signals.post_save)
def index_record(sender, instance, **kwargs):
    """
    index changes to a db model
    :param sender:
    :param kwargs:
    :return:
    """
    if issubclass(sender, Model):
        model_name = instance._meta.verbose_name
        if model_name in settings.INDEXED_MODELS:
            return helper.index_instance.delay(instance._meta.verbose_name, instance.pk)


@receiver(signals.post_delete)
def delete_record(sender, instance, **kwargs):
    """
    delete db model from index and cache
    :param sender:
    :param kwargs:
    :return:
    """
    if issubclass(sender, Model):
        model_name = instance._meta.verbose_name
        if model_name in settings.INDEXED_MODELS:
            title = model_name.title().replace(' ', '')
            klass = getattr(app_serializers, '%sSerializer' % title)
            serialized = klass(instance)
            # payload = Payload(serialized.data)
            return helper.delete_instance.delay(model_name, serialized.data)


@receiver(signals.post_save, sender=models.Broadcast)
def trigger_broadcast(sender, instance, **kwargs):
    """
    push broadcasts to users
    :param sender:
    :param kwargs:
    :return:
    """
    return helper.trigger_notification(instance.message, instance.link,
                                       [c.pk for c in app_serializers.AccountSerializer.Meta.model.objects.all()])


@receiver(record_referral)
def record(sender, **kwargs):
    """
    record referral
    :param sender:
    :param instance:
    :param kwargs:
    :return:
    """
    try:
        account_id = kwargs.get('account_id')
        referral_code = kwargs.get('referral_code')

        account = models.Account.objects.get(pk=account_id)
        referrer = models.Settings.objects.filter(referral_code=referral_code).first()
        referral = models.Referral.objects.create(account=referrer.account, recipient=account.email, name=account.name)

        return referral
    except Exception, e:
        print(e)
        print('=========')


def accept_request(email, nickname):
    """
    accept artist request
    """
    try:
        account = models.Account.objects.filter(user__email=email).first()
        if account and not hasattr(account, 'artist'):
            artist = models.Artist.objects.create(account_id=account.pk, nickname=nickname, is_active=True)
            if artist:
                url = '%s/cards/create' % settings.DOMAIN
                request_message = "<p>Hello %s,</p>" \
                                  "<p>Welcome to kaadie artist platform. Your request to be an artist on Kaadie has " \
                                  "been accepted. We are delighted to have you as a part of our artist " \
                                  "community.</p><p>Kaadie artist community seeks to promote creativity and to give " \
                                  "you the power to earn from your creativity. You have total control over your " \
                                  "designs and what you earn.</p>" \
                                  "<p>Please follow these easy steps to start earning</p><ul>" \
                                  "<li>1.Navigate <a href='%s'>here</a> to create cards to upload your cards </li>" \
                                  "<li>2.To start uploading your cards, fill the form with the following;<ul>" \
                                  "<li>a.Card title </li><li>b.Card description </li><li>c.Card categories " \
                                  "(cards can belong to more than one categories)</li><li>d.Earn money by setting a " \
                                  "price for your cards </li></ul></li>" \
                                  "</ul><p><b>(Please note that card prices and earnings must follow agreed " \
                                  "Terms and Condition) </b></p><p></p><p></p>" \
                                  "<p>Yours Sincerely,<br>Kaadie</p>" % (account.name, url)

                return transport.MailService.send_mail('Your request has been accepted', settings.ADMIN_ACCOUNT,
                                                       'Kaadie', account.email, request_message, account.name)

    except Exception, e:
        raise e


def deny_request(email):
    """
    deny artist request
    """
    try:
        user = models.User.objects.filter(email=email).first()
        request_message = "<p>Hello %s,</p>" \
                          "<p>Your request to be an artist on Kaadie has been denied.</p>" \
                          "<p></p><p></p>" \
                          "<p>To grant access, create an artist account for this user on the admin panel.</p>" \
                          % user.account.name

        return transport.MailService.send_mail('Your request has been denied', settings.ADMIN_ACCOUNT, 'Kaadie',
                                               user.email, request_message, user.account.name)
    except Exception, e:
        raise e


@receiver(signals.pre_save, sender=models.ArtistRequest)
def process_artist_requests(sender, instance, **kwargs):
    """
    accept artist rights request
    :param sender
    :return
    """
    try:
        obj = sender.objects.get(pk=instance.pk)
        if obj.is_accepted != instance.is_accepted:
            return accept_request(instance.email, instance.nickname) if instance.is_accepted is True else \
                deny_request(instance.email)
        return
    except sender.DoesNotExist:
        if instance.is_accepted is True:
            return accept_request(instance.email, instance.nickname)


# @receiver(signals.post_init, sender=models.Artist)
# def send_acceptance_message(sender, instance, **kwargs):
#     """
#     """
#     url = '%s/cards/create' % settings.DOMAIN
#     request_message = "<p>Hello %s,</p>" \
#                       "<p>Your request to be an artist on Kaadie has been accepted.</p><p>Click here to start " \
#                       "creating cards %s.</p>" \
#                       "<p></p><p></p>" \
#                       "<p>Yours Sincerely,<br>Kaadie</p>" % (instance.account.name, url)
#
#     return transport.MailService.send_mail('Your request has been accepted', settings.ADMIN_ACCOUNT, 'Kaadie',
#                                            instance.account.email, request_message, instance.account.name)


# @receiver(signals.post_init, sender=models.Account)
# def create_wallet(sender, instance, created, **kwargs):
#     if created:
#         models.Wallet.objects.create(account=instance)


@celery_app.task
def send_birthday_reminder(contact_id, domain):
    """
    send birthday reminder for a contact
    :param contact_id:
    :return:
    """
    contact = models.Contact.objects.get(pk=contact_id)
    account = contact.user

    birthday_category = models.Section.objects.filter(slug=settings.BIRTHDAY_SECTION).first()
    random_card = next(x for x in random.sample(birthday_category.cards, 1))

    if account.saved_cards.filter(card__is_active=True).count() > 0:
        random_card = account.saved_cards.filter(card__is_active=True, card__categories__in=list((c.id for c
                                                                                                  in
                                                                                                  birthday_category.categories.all()))).order_by(
            '?').first()

    if not random_card:
        reset_message = "<p>Hello {},</p>" \
                        "<p> There are currently no birthday cards available."

        transport.MailService.send_mail('No Birthday Cards', 'system@kaadie.com', 'Kaadie', settings.ALERT_ACCOUNT,
                                        reset_message, 'Kaadie')

    return transport.send_card(random_card.pk, account.pk, [contact.email], [], account.name,
                               account.email, domain, cover_image=random_card.image_url, personalized_message='')


save_thread.connect(received_card_handler)
new_user_registered.connect(send_welcome_message)
