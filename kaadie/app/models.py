from __future__ import unicode_literals
from datetime import datetime, time
from itertools import chain

from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.utils import timezone
from django.conf import settings
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from s3direct.fields import S3DirectField
from tinymce.models import HTMLField


# Create your models here.
class Abstract(models.Model):
    date_created = models.DateTimeField(default=timezone.now)
    date_modified = models.DateTimeField(default=timezone.now)

    class Meta:
        abstract = True

    @classmethod
    def fields(cls):
        return [f.name for f in cls._meta.local_fields]


class Currency(Abstract):
    name = models.CharField(max_length=255)
    symbol = models.TextField(blank=False, null=False)
    code = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = 'currencies'


class Address(Abstract):
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    full_name = models.TextField(blank=False, null=False)
    phone = models.CharField(max_length=50, blank=True, null=True)
    date_of_birth = models.DateField(null=True, blank=True)
    address = models.TextField(blank=True, null=True)
    city = models.TextField(blank=True, null=True)
    country = models.TextField(blank=True, null=True)

    def __str__(self):
        return '%s - %s' % (self.full_name, self.phone if self.phone is not None else '')

    def __repr__(self):
        return '<Address: %s>' % self.full_name

    @property
    def full_address(self):
        """
        full address of an address object
        :return:
        """
        return '%s %s %s' % (self.address, self.city, self.country)

    def get_full_name(self):
        return '%s %s' % (self.first_name if self.first_name else '', self.last_name if self.last_name else '')

    class Meta:
        verbose_name_plural = 'addresses'


class Account(Abstract):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    image_url = S3DirectField(dest='destination', null=True)
    is_verified = models.BooleanField(default=False)
    source = models.CharField(default='basic', max_length=50)
    email_valid = models.BooleanField(default=True)
    is_premium = models.BooleanField(default=False)
    gender = models.CharField(null=True, blank=True, max_length=10)
    language = models.CharField(default='English', max_length=255)
    referral_credit = models.IntegerField(default=0)

    @receiver(models.signals.post_save, sender=User)
    def create_account(sender, instance, created, **kwargs):
        """
        creating account
        :param instance:
        :param created:
        :param kwargs:
        :return:
        """
        from . import services

        if created:
            address = Address.objects.create(first_name=instance.first_name, last_name=instance.last_name,
                                             full_name='%s %s' % (instance.first_name, instance.last_name))
            account = Account.objects.create(user=instance, address=address)

            sett = Settings.objects.create(account=account, referral_code=services.helper.generate_referral_code())

            default_sections = Section.objects.order_by('position')[:3]
            for sec in default_sections:
                sett.default_sections.add(sec)
                sett.save()

    @receiver(models.signals.post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        if hasattr(instance, 'account'):
            instance.account.save()

    class Meta:
        verbose_name_plural = 'accounts'

    @property
    def name(self):
        return self.address.full_name

    @property
    def email(self):
        return self.user.email

    @property
    def phone(self):
        return self.address.phone

    @property
    def saved_count(self):
        return sum([c.count for c in self.saved_cards.all()])

    @property
    def sent_count(self):
        return sum([c.count for c in self.sent_cards.all()])

    @property
    def threads(self):
        return list(chain(self.shared_cards.all(), self.sent_cards.all()))

    def __str__(self):
        return self.email

    def __repr__(self):
        return '<Account: %s>' % self.email


class Organization(Abstract):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=50)
    logo = S3DirectField(dest='destination', null=True, blank=True)
    primary_color = models.CharField(max_length=50)
    primary_background_color = models.CharField(max_length=50)
    color = models.CharField(max_length=50)
    primary_color_class = models.CharField(max_length=100)
    semantic_class = models.CharField(max_length=100)
    menu_color = models.CharField(max_length=100, default='0xFFFFFF')
    account = models.ForeignKey(Account)
    sms_api_url = models.TextField(null=True, blank=True)
    email_api_url = models.TextField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'organizations'

    def __str__(self):
        return self.name

    def __repr__(self):
        return '<Organization: %s>' % self.name


class Oauth(Abstract):
    user = models.ForeignKey(Account, related_name='oauth')
    platform = models.CharField(null=False, blank=False, max_length=50)
    username = models.EmailField(null=False, blank=False)
    access_id = models.CharField(null=False, blank=False, max_length=255)


class UserLoginDetails(Abstract):
    user = models.ForeignKey(Account, related_name='details')
    browser = models.CharField(max_length=100)
    is_mobile = models.BooleanField(default=False)
    is_pc = models.BooleanField(default=True)
    is_tablet = models.BooleanField(default=False)
    device = models.CharField(max_length=255)
    os = models.CharField(max_length=255)


class Contact(Abstract):
    address = models.ForeignKey(Address, on_delete=models.CASCADE, related_name='addresses')
    email = models.CharField(max_length=200, null=True, blank=True)
    user = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='contacts')
    is_active = models.BooleanField(default=False)
    image_url = S3DirectField(dest='destination', null=True, blank=True)
    is_reminder_set = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'contacts'

    @property
    def name(self):
        return self.address.full_name

    @property
    def phone(self):
        return self.address.phone

    def __str__(self):
        return '%s(%s)' % (self.user.email, self.address.full_name)

    def __repr__(self):
        return "<Contact: %s's contact - %s>" % (self.user.email, self.address.full_name)


class Section(Abstract):
    title = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    slug = models.SlugField(editable=False)
    image_url = S3DirectField(dest='destination', null=True)
    background_class = models.CharField(null=True, max_length=255, blank=True)
    font_class = models.CharField(null=True, blank=True, max_length=50)
    permissible_organizations = models.ManyToManyField(Organization)
    position = models.IntegerField(null=True, blank=True, unique=True)

    @property
    def cards(self):
        return list(chain.from_iterable([c.cards.filter(is_active=True).all() for c in self.categories.all()]))

    def save(self):
        if not self.id:
            self.slug = slugify(self.title)

        super(Section, self).save()

    class Meta:
        verbose_name_plural = 'sections'

    def __str__(self):
        return self.title

    def __repr__(self):
        return '<Category: %s>' % self.slug


class Category(Abstract):
    title = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    section = models.ForeignKey(Section, related_name='categories')
    slug = models.SlugField(editable=False)
    image_url = S3DirectField(dest='destination', null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'categories'

    def save(self):
        if not self.id:
            self.slug = slugify(self.title)

        super(Category, self).save()

    def __str__(self):
        return '%s(%s)' % (self.section.title, self.title)


class Banner(Abstract):
    title = models.CharField(max_length=255, null=True, blank=True)
    image_url = S3DirectField(dest='destination', null=True)
    link = models.TextField(null=True, blank=True)
    slug = models.SlugField(editable=False)
    is_active = models.BooleanField(default=True)

    def save(self):
        if not self.id:
            self.slug = slugify(self.title)
        super(Banner, self).save()

    class Meta:
        verbose_name_plural = 'banners'

    def __str__(self):
        return self.title


class ArtistBanner(Abstract):
    title = models.CharField(max_length=255, null=True, blank=True)
    image_url = S3DirectField(dest='destination', null=True)
    link = models.URLField(null=True, blank=True)
    slug = models.SlugField(editable=False)
    is_active = models.BooleanField(default=True)

    def save(self):
        if not self.id:
            self.slug = slugify(self.title)
        super(Banner, self).save()

    class Meta:
        verbose_name_plural = 'banners'

    def __str__(self):
        return self.title


class Tag(Abstract):
    """
    target tags for cards
    """
    name = models.CharField(null=False, blank=False, max_length=50)
    slug = models.SlugField(editable=False)

    def save(self):
        """
        override save action to add slug
        :return:
        """
        if not self.id:
            self.slug = slugify(self.name)

        super(Tag, self).save()

    def __str__(self):
        return '%s' % self.slug


class Card(Abstract):
    title = models.TextField(null=False, blank=False, unique=True)
    description = models.TextField(null=True, blank=True)
    categories = models.ManyToManyField(Category, related_name='cards')
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    image_url = S3DirectField(dest='destination', null=True)
    display_image = S3DirectField(dest='destination', null=True, blank=True)
    is_featured = models.BooleanField(default=False)
    saved_count = models.IntegerField(default=0)
    shared_count = models.IntegerField(default=0)
    sent_count = models.IntegerField(default=0)
    slug = models.SlugField(editable=False)
    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=18)
    is_active = models.BooleanField(default=False)
    is_public = models.BooleanField(default=True)
    tags = models.ManyToManyField(Tag, related_name='tags')

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        super(Card, self).save(*args, **kwargs)

    @property
    def saved_counts(self):
        return sum([c.count for c in self.saved_cards.all()])

    @property
    def sent_counts(self):
        return sum([c.count for c in self.sent_cards.all()])

    @property
    def thumbnail_url(self):
        return self.thumbnail.image_url if hasattr(self, 'thumbnail') else self.image_url

    class Meta:
        verbose_name_plural = 'cards'

    def __str__(self):
        return self.title


class SavedCard(Abstract):
    """
    saved cards belonging to a user
    """
    user = models.ForeignKey(Account, related_name='saved_cards')
    card = models.ForeignKey(Card, related_name='saved_cards')
    count = models.IntegerField(default=1)


class SavedCardImage(Abstract):
    """
    custom image for a saved card
    """
    cover_image_url = S3DirectField(dest='destination', null=False)
    card_image_url = S3DirectField(dest='destination', null=True)
    saved_card = models.ForeignKey(SavedCard, related_name='saved_cards')


class SentCard(Abstract):
    """
    sent cards belonging to a user
    """
    user = models.ForeignKey(Account, related_name='sent_cards')
    card = models.ForeignKey(Card, related_name='sent_cards')
    count = models.IntegerField(default=1)
    notification_id = models.CharField(null=True, blank=True, max_length=255)
    image_url = S3DirectField(dest='destination', null=True)
    message = models.TextField()
    recipients = models.TextField()

    @property
    def model_name(self):
        """
        return model name
        :return:
        """
        return self._meta.model_name


class ReceivedCard(Abstract):
    """
    cards received by a user
    """
    user = models.ForeignKey(Account, related_name='received_cards')
    card = models.ForeignKey(Card, related_name='received_cards')
    sent_card = models.ForeignKey(SentCard, related_name='received_cards')
    recipient = models.CharField(max_length=255)
    notification_id = models.CharField(null=True, blank=True, max_length=255)
    message = models.TextField(blank=True, null=True)
    image_url = models.TextField(blank=True, null=True)
    cover_image_url = models.TextField(blank=True, null=True)


class SharedCard(Abstract):
    """
    saved cards belonging to a user
    """
    user = models.ForeignKey(Account, related_name='shared_cards')
    card = models.ForeignKey(Card, related_name='shared_cards')
    platform = models.CharField(max_length=50)
    notification_id = models.TextField(null=True, blank=True, max_length=255)

    @property
    def model_name(self):
        """
        return model name
        :return:
        """
        return self._meta.model_name


class Message(Abstract):
    sender = models.ForeignKey(Account)
    receiver = models.ForeignKey(Account, null=True, blank=True, related_name="%(app_label)s_%(class)s_related")
    receiver_address = models.CharField(max_length=255, blank=False, null=False)
    card = models.ForeignKey(Card, null=True, blank=True)
    body = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name_plural = 'messages'


class Notifications(Abstract):
    user = models.ForeignKey(Account, related_name='notifications')
    body = models.TextField(blank=False, null=False)
    is_read = models.BooleanField(default=False)
    link = models.URLField(null=False, blank=False)
    event = models.CharField(null=False, blank=False, max_length=50)
    # tag = models.CharField(blank=False, null=False, max_length=255)

    class Meta:
        verbose_name_plural = 'notifications'


class AuthorizationToken(Abstract):
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    code = models.TextField(null=False, blank=False)
    is_expired = models.BooleanField(default=False)
    is_verification = models.BooleanField(default=True)
    is_password_change = models.BooleanField(default=False)

    def __str__(self):
        return self.code


class Cart(Abstract):
    checked_out = models.BooleanField(default=False, verbose_name=_('checked out'))

    class Meta:
        verbose_name = _('cart')
        verbose_name_plural = _('carts')
        ordering = ('-date_created',)

    def __unicode__(self):
        return unicode(self.date_created)


# class ItemManager(models.Manager):
#     def get(self, *args, **kwargs):
#         if 'product' in kwargs:
#             kwargs['content_type'] = ContentType.objects.get_for_model(type(kwargs['product']))
#             kwargs['object_id'] = kwargs['product'].pk
#             del(kwargs['product'])
#         return super(ItemManager, self).get(*args, **kwargs)


class Item(Abstract):
    cart = models.ForeignKey(Cart, verbose_name=_('cart'), related_name='items')
    quantity = models.PositiveIntegerField(verbose_name=_('quantity'))
    unit_price = models.DecimalField(max_digits=18, decimal_places=2, verbose_name=_('unit price'))
    # product as generic relation
    content_type = models.ForeignKey(ContentType)
    card = models.ForeignKey(Card, related_name='items')

    class Meta:
        verbose_name = _('item')
        verbose_name_plural = _('items')
        ordering = ('cart',)

    def __unicode__(self):
        return u'%d units of %s' % (self.quantity, self.product.__class__.__name__)

    def total_price(self):
        return self.quantity * self.unit_price

    total_price = property(total_price)

    # product
    def get_product(self):
        return self.content_type.get_object_for_this_type(pk=self.object_id)

    def set_product(self, product):
        self.content_type = ContentType.objects.get_for_model(type(product))
        self.object_id = product.pk

    product = property(get_product, set_product)


class Comments(Abstract):
    """
    comments to received cards
    """
    received_card = models.ForeignKey(ReceivedCard, null=True, blank=True)
    sent_card = models.ForeignKey(SentCard, null=True, blank=True, related_name='comments')
    shared_card = models.ForeignKey(SharedCard, null=True, blank=True, related_name='comments')
    poster = models.CharField(blank=False, null=False, max_length=255)
    poster_address = models.CharField(blank=True, null=True, max_length=255)
    comment = HTMLField()
    # body = FroalaField(blank=True, null=True)

    class Meta:
        verbose_name_plural = 'comments'


class Artist(Abstract):
    """
    artist account for creating cards
    """
    account = models.OneToOneField(Account)
    nickname = models.TextField(blank=False, null=False)
    is_active = models.BooleanField(default=False)


class ArtistRequest(Abstract):
    """
    artist rights requests by users
    """
    __INITIAL__STATE__ = None

    email = models.EmailField(blank=False, null=False)
    nickname = models.TextField(null=False, blank=False)
    portfolio = models.URLField(null=False, blank=False)
    is_accepted = models.BooleanField(default=False)
    is_closed = models.BooleanField(default=False)

    def __init__(self, *args, **kwargs):
        super(ArtistRequest, self).__init__(*args, **kwargs)
        self.__INITIAL__STATE__ = self.is_accepted

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.__INITIAL__STATE__ != self.is_accepted:
            self.is_closed = True
        super(ArtistRequest, self).save(force_insert, force_update, using, update_fields)


class Wallet(Abstract):
    """
    artist account for creating cards
    """
    is_active = models.BooleanField(default=False)
    account = models.OneToOneField(Account)
    credit_balance = models.FloatField(default=0.0)
    debit_balance = models.FloatField(default=0.0)
    total_balance = models.FloatField(default=0.0)
    sms_credit = models.IntegerField(default=settings.DEFAULT_SMS_CREDIT)


class PaymentAccount(Abstract):
    # artist = models.ForeignKey(Artist)
    account = models.ForeignKey(Account)
    account_number = models.CharField(max_length=50)
    bank = models.CharField(max_length=100)
    is_active = models.BooleanField(default=False)


class PaymentCard(Abstract):
    # artist = models.ForeignKey(Artist)
    account = models.ForeignKey(Account)
    unique_id = models.TextField()
    wallet = models.ForeignKey(Wallet)
    is_debit_card = models.BooleanField(default=True)
    is_credit_card = models.BooleanField(default=False)


class Transaction(Abstract):
    account = models.ForeignKey(Account)
    unique_id = models.TextField(blank=False, null=False)


class TransactionItem(Abstract):
    account = models.ForeignKey(Account)
    sent_card = models.ForeignKey(SentCard)
    transaction = models.ForeignKey(Transaction)


class Settings(Abstract):
    """
    account settings
    """
    account = models.OneToOneField(Account, related_name='setting')
    web_notification = models.BooleanField(default=True)
    mobile_notification = models.BooleanField(default=True)
    sms_notification = models.BooleanField(default=True)
    email_notification = models.BooleanField(default=True)
    birthday_alert_time = models.TimeField(default=time(settings.BIRTHDAY_ALERT_HOUR, settings.BIRTHDAY_ALERT_MINUTE))
    sending_limit = models.IntegerField(default=30)
    default_sections = models.ManyToManyField(Section)
    referral_code = models.CharField(null=True, blank=True, max_length=50)


class SystemMessage(Abstract):
    """
    system messages
    """

    class Meta:
        verbose_name = 'system_message'

    html_text = models.TextField(null=False, blank=False)
    sms_text = models.TextField(null=False, blank=False)
    tag = models.CharField(default='birthday', max_length=50)


class Broadcast(Abstract):
    """
    system broadcasts
    """
    message = models.TextField(null=False, blank=False)
    link = models.URLField(null=False, blank=False)
    # tags = models.ManyToManyField(Tag)


class SMSCredit(Abstract):
    """
    sms credit to be purchased
    """
    amount = models.FloatField(null=False, blank=False, unique=True)
    sms_credit = models.IntegerField(null=False, blank=False)


class SMSTransaction(Abstract):
    """
    sms transaction
    """
    account = models.ForeignKey(Account)
    amount = models.FloatField(null=False, blank=False)
    uuid = models.TextField(null=False, blank=False)
    credit = models.ForeignKey(SMSCredit)


class SMSAccount(Abstract):
    """
    sms account
    """
    account = models.OneToOneField(Account)
    username = models


class Referral(Abstract):
    """
    referral
    """
    account = models.ForeignKey(Account, related_name='referrals')
    name = models.CharField(null=True, blank=True, max_length=255)
    recipient = models.CharField(null=False, blank=False, max_length=255)


class CardThumbnail(Abstract):
    """
    thumbnail image for cards
    """
    card = models.OneToOneField(Card, related_name='thumbnail')
    image_url = S3DirectField(dest='destination', null=True)
