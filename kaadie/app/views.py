import urlparse, os, hashlib, requests, json
from datetime import datetime
import re
from base64 import b64encode
import time, random
from itertools import groupby, chain

from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.http import Http404, HttpResponseForbidden
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.template.defaultfilters import slugify
from django.views import View
from django.views.generic import TemplateView
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout, models, mixins
from django.conf import settings
from django.utils.crypto import get_random_string
from django.urls import reverse
from django.db.models import Q, Count
from django_user_agents.utils import get_user_agent

from oauth2 import Consumer, Client, Token
from openpyxl import load_workbook

import serializers, forms, signals
from .models import Account, Card, Address, Contact, AuthorizationToken, Category, Banner, Section, Settings, \
    SavedCard, Oauth, Artist, ReceivedCard, Comments, Organization, SystemMessage, \
    Notifications as modelNotification, Wallet, SharedCard, SavedCardImage, ArtistBanner

from kaadie import es, redis_obj
from services import mixins as services_mixin, encoders as services_encoders, helper, cache, transport


def custom_page_not_found(request, *args, **kwargs):
    """
    custom 404 view
    :param request:
    :return:
    """
    sub = kwargs.get('sub')
    ua = get_user_agent(request)
    user = request.user
    organization = helper.get_organization(sub)

    try:
        sections = cache.fetch_cache_sections(organization.code)
        random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                 body={'query': {
                                     'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                         'random_score': {'seed': random.randint(1, 1000000)}
                                     }]}}, 'filter': {
                                     'bool': {'must': [{'term': {'is_public': True}}]}
                                 }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)

        random_hits = random_query['hits']

        if random_hits['total'] == 0:
            random_cards = Card.objects.filter(is_active=True, is_public=True).order_by('?')[
                           :6].all()

        else:
            _random = random_hits['hits']
            random_cards = [helper.Payload(i['_source']) for i in _random]
    except Exception, e:
        random_cards = Card.objects.filter(is_active=True, is_public=True).order_by('?')[
                       :6].all()
        sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
            'position').all()
        is_db = True

    resp = render(request, '404.html', locals())
    resp.status_code = 404
    return resp


def custom_internal_server_error(request, *args, **kwargs):
    """
    custom internal server error
    :param request:
    :return:
    """
    sub = kwargs.get('sub')
    ua = get_user_agent(request)
    user = request.user
    organization = helper.get_organization(sub)

    try:
        sections = cache.fetch_cache_sections(organization.code)
    except Exception, e:
        sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
            'position').all()
        is_db = True

    resp = render(request, '500.html', locals())
    resp.status_code = 500

    return resp


class SearchView(services_mixin.MainMixin, TemplateView):
    template_name = 'search.html'

    def get(self, request, *args, **kwargs):
        """
        loads login page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user
        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)

        search_q = request.GET.get('q')
        search_result = list()
        random_cards = list()
        if search_q:
            try:
                search_query = es.search(index=settings.ES_INDEX, doc_type='card', body={
                    'query': {
                        "query_string": {
                            'fields': ['slug', 'title', 'description'],
                            'query': '*%s*' % search_q,
                            "lowercase_expanded_terms": False
                        }
                    }
                })
                search_hits = search_query['hits']
                if search_hits['total'] > 0:
                    _hits = search_hits['hits']
                    search_result = [helper.Payload(i['_source']) for i in _hits]
            except Exception, e:
                print('search error')
                print(e)

        try:
            random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                     body={'query': {
                                         'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                             'random_score': {'seed': random.randint(1, 1000000)}
                                         }]}}, 'filter': {
                                         'bool': {'must': [{'term': {'is_public': True}}]}
                                     }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)

            random_hits = random_query['hits']

            if random_hits['total'] == 0:
                random_cards = Card.objects.filter(is_active=True, is_public=True).order_by('?')[
                               :6].all()

            else:
                _random = random_hits['hits']
                random_cards = [helper.Payload(i['_source']) for i in _random]
        except Exception, e:
            is_db = True
            if len(search_result) == 0:
                random_cards = Card.objects.filter(is_active=True, is_public=True).order_by('?')[
                               :6].all()

        return render(request, self.template_name, locals())


class IndexView(services_mixin.MainMixin, TemplateView):
    template_name = 'landing.html'

    def get(self, request, *args, **kwargs):
        """
        loads login page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user
        saved_cards = []

        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)

        if organization is None:
            raise Http404

        premium = False
        user_id = None

        if user.is_authenticated() and hasattr(user, 'account'):
            premium = user.account.is_premium
            user_id = user.account.pk

        try:
            banners, featured_cards = cache.fetch_from_cache(organization.code, user_id)
            # is_search, categories = cache.fetch_categories(organization.code)
            # popular_cards = cache.fetch_popular_cards()
        except Exception, e:
            # is_search = False
            banners = Banner.objects.filter_by(is_active=True).all()
            featured_cards = Card.objects.filter(is_public=True, is_active=True, is_featured=True).all()
            # categories = Category.objects.filter(is_active=True,
            #                                      section__permissible_organizations__in=[organization.pk]).order_by(
            #     '?')[:6].all()
            is_db = True

        popular_cards = Card.objects.filter(is_public=True, is_active=True).annotate(
            cats=Count('sent_cards')).order_by('-cats')[:8]

        if request.session.get('next'):
            next_url = request.session.get('next')
            del request.session['next']
            return redirect(next_url, sub=sub)

        return render(request, self.template_name, locals())


class LoginView(services_mixin.AnonymousMixin, services_mixin.MainMixin, View):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        """
        loads login page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        request.session['next'] = request.GET.get('next')
        username = ''
        password = ''

        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        Attempts to login user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        # retrieve user agent credentials
        sub = kwargs.get('sub')
        next_url = '/'

        if sub is not None:
            next_url = '/%s' % sub

        if request.session.get('next'):
            next_url = request.session.get('next')
            del request.session['next']
        helper.Payload = {'message': 'Successfully logged In', 'status': 'success',
                          'redirect_url': next_url}

        username = password = ''
        form = forms.LoginForm(request.POST)
        social_login = request.POST['social_login']
        platform = request.POST.get('platform')

        if form.is_valid():
            username = form.cleaned_data['email']
            password = form.cleaned_data['password']

            # social login
            if social_login == 'true':
                user = models.User.objects.filter(username=username).first()

                if user:
                    if authenticate(username=username, password=password) or \
                            Oauth.objects.filter(username=username, access_id=password).exists():
                        login(request, user)
                        return services_encoders.JSONResponse({'message': 'Successfully logged In', 'status': 'success',
                                                               'redirect_url': next_url})
                    else:
                        oauth = Oauth.objects.create(user=user.account, platform=platform, username=username,
                                                     access_id=password)
                        login(request, user)
                        return services_encoders.JSONResponse({'message': 'Successfully logged In', 'status': 'success',
                                                               'redirect_url': next_url})
                else:
                    image_url = form.cleaned_data.get('image_url')
                    first_name = form.cleaned_data.get('first_name')
                    last_name = form.cleaned_data.get('last_name')

                    user = models.User.objects.create_user(username=username, email=username, password=password,
                                                           first_name=first_name, last_name=last_name)

                    oauth = Oauth.objects.create(user=user.account, platform=platform, username=username,
                                                 access_id=password)

                    account = Account.objects.filter(user=user.id).first()
                    account.image_url = image_url
                    account.save()

                    # login new user
                    login(request, user)

                    return services_encoders.JSONResponse({'message': 'Successfully logged In', 'status': 'success',
                                                           'redirect_url': next_url})
            user = models.User.objects.filter(Q(username=username) | Q(email=username)).first()
            if user and authenticate(username=username, password=password):
                login(request, user)
                return redirect(next_url)

        messages.error(request, 'Invalid email/password combination')
        return render(request, self.template_name, locals())


@login_required
def logout_view(request, *args, **kwargs):
    """
    logout user from application
    :param request:
    :return:
    """

    logout(request)
    sub = kwargs.get('sub')

    try:
        helper.record_log(request, 'logged out', request.path, 'page')
    except Exception, e:
        print('==============')
        print(e)
        print('==============')

    if sub is not None:
        return redirect('index', sub=sub)
    else:
        return redirect('index')


def verify(request, *args, **kwargs):
    """
    verify user account
    :param request:
    :return:
    """
    token = request.GET['token']
    sub = kwargs.get('sub')
    if not token:
        if sub is not None:
            return redirect('index', sub=sub)
        else:
            return redirect('index')

    auth_token = AuthorizationToken.objects.filter(code=token).first()

    # update user verification status
    account = auth_token.user
    account.is_verified = True
    account.save()

    # update expiration date for token
    auth_token.is_expired = True
    auth_token.save()

    messages.success(request, 'Your account has been verified')

    if sub is not None:
        return redirect('login', sub=sub)
    else:
        return redirect('login')


class RegisterView(services_mixin.AnonymousMixin, View):
    template_name = 'signup.html'

    def get(self, request, *args, **kwargs):
        """
        loads signup page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        referral_code = request.GET.get('code', '')

        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        Attempts to register a user acccount
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        next_url = ''

        sub = kwargs.get('sub')

        if request.session.get('next'):
            next_url = request.session.get('next')
            del request.session['next']
        social = request.POST.get('social_signup')
        platform = request.POST.get('platform')
        form = forms.RegistrationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            name = form.cleaned_data.get('name')
            _name = str(name).split()
            first_name = next(variable for variable in _name)
            last_name = ''.join(_name[1:])
            image_url = form.cleaned_data.get('image_url')

            # referral code flow
            referral_code = form.cleaned_data.get('referral_code', None)

            if social == 'true':
                user = models.User.objects.filter(username=username).first()
                if user:
                    if authenticate(username=username, password=password) or \
                            Oauth.objects.filter(username=username, access_id=password).exists():
                        login(request, user)
                        return services_encoders.JSONResponse({'message': 'Successfully logged In', 'status': 'success',
                                                               'redirect_url': request.GET.get('next', '/')})
                    else:
                        oauth = Oauth.objects.create(user=user.account, platform=platform, username=username,
                                                     access_id=password)
                        login(request, user)
                        return services_encoders.JSONResponse({
                            'message': 'Successfully logged In', 'status': 'success',
                            'redirect_url': request.GET.get('next', '/')})
                else:
                    user = models.User.objects.create_user(username=username, email=username, password=password,
                                                           first_name=first_name, last_name=last_name)

                    oauth = Oauth.objects.create(user=user.account, platform=platform, username=username,
                                                 access_id=password)

                    account = Account.objects.filter(user=user.id).first()
                    account.image_url = image_url
                    account.save()

                    # login new user
                    login(request, user)

                    return services_encoders.JSONResponse({'message': 'Successfully logged In', 'status': 'success',
                                                           'redirect_url': request.GET.get('next', '/')})

            user = models.User.objects.filter(username=username).first()

            if user:
                messages.error(request, 'An account with this username already exists')
                storage = messages.get_messages(request)

                return render(request, self.template_name, locals())

            user = models.User.objects.create_user(username=username, email=username, password=password,
                                                   first_name=first_name, last_name=last_name)

            account = Account.objects.filter(user=user.id).first()
            account.image_url = image_url
            account.save()

            # send message to user
            signals.new_user_registered.send(sender=None, account=account, request=request)

            # record referral
            signals.record_referral.send(sender=None, account_id=account.id, referral_code=referral_code)

            # log user in
            login(request, user)

            # send redirect response to ajax request
            if social == 'true':
                return services_encoders.JSONResponse({'message': 'Successfully registered account',
                                                       'status': 'success', 'redirect_url': '/'})
            if sub is not None:
                return redirect('/%s%s' % (sub, next_url))
            else:
                return redirect('/')

        messages.error(request, form.errors)
        storage = messages.get_messages(request)

        if social == 'true':
            return services_encoders.JSONResponse(
                {'message': form.non_field_errors(), 'status': 'error', 'redirect_url': None})

        return render(request, self.template_name, locals())


class RegisterBrandView(services_mixin.AnonymousMixin, services_mixin.MainMixin, View):
    template_name = 'brand_signup.html'

    def get(self, request, *args, **kwargs):
        """
        loads signup page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        Attempts to register a user acccount
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        next_url = '/'
        if request.session.get('next'):
            next_url = request.session.get('next')
            del request.session['next']
        form = forms.BrandRegistrationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            name = form.cleaned_data.get('name')
            _name = str(name).split()
            first_name = _name[0]
            last_name = ''.join(_name[1:])
            image_url = form.cleaned_data.get('image_url')
            brand_name = form.cleaned_data.get('brand_name')
            logo = form.cleaned_data.get('logo')

            user = models.User.objects.filter(username=username).first()

            if user:
                messages.error(request, 'An account with this username already exists')
                storage = messages.get_messages(request)

                return render(request, self.template_name, locals())

            org = Organization.objects.filter(name=brand_name).first()

            if org:
                messages.error(request, 'A brand with this name already exists')
                storage = messages.get_messages(request)

                return render(request, self.template_name, locals())

            user = models.User.objects.create_user(username=username, email=username, password=password,
                                                   first_name=first_name, last_name=last_name)

            account = Account.objects.get(user=user)

            Organization.objects.create(name=brand_name, logo=logo, account=account)

            # send message to user
            signals.new_user_registered.send(sender=None, account=account, request=request)

            # log user in
            login(request, user)

            account = request.user.account

            account.image_url = image_url
            account.is_premium = True
            account.save()

            sub = kwargs.get('sub')
            if sub is not None:
                return redirect(next_url, sub=sub)
            else:
                return redirect(next_url)

        messages.error(request, form.errors)
        storage = messages.get_messages(request)

        return render(request, self.template_name, locals())


class ArtistRegisterView(services_mixin.AnonymousMixin, services_mixin.MainMixin, View):
    template_name = 'forms/artist.html'

    def get(self, request, *args, **kwargs):
        """
        loads signup page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        referral_code = request.GET.get('code', '')

        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        Attempts to register a user acccount
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        next_url = ''

        sub = kwargs.get('sub')

        if request.session.get('next'):
            next_url = request.session.get('next')
            del request.session['next']

        form = forms.ArtistRegistrationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            name = form.cleaned_data.get('name')
            _name = str(name).split()
            first_name = next(variable for variable in _name)
            last_name = ''.join(_name[1:])
            portfolio = form.cleaned_data.get('portfolio', '')
            nickname = form.cleaned_data.get('nickname')

            # referral code flow
            referral_code = form.cleaned_data.get('referral_code', None)

            if models.User.objects.filter(username=username).exists():
                messages.error(request, 'An account with this username already exists')
                storage = messages.get_messages(request)
                return render(request, self.template_name, locals())

            if Artist.objects.filter(nickname=nickname).exists():
                messages.error(request, 'Artist with nickname - {} already exists'.format(nickname))
                storage = messages.get_messages(request)
                return render(request, self.template_name, locals())

            if not helper.validate_url(portfolio):
                messages.error(request, 'Invalid URL added as online portfolio')
                storage = messages.get_messages(request)
                return render(request, self.template_name, locals())

            user = models.User.objects.create_user(username=username, email=username, password=password,
                                                   first_name=first_name, last_name=last_name)

            account = Account.objects.filter(user=user.id).first()
            transport.request_artist_permission.delay(user.email, portfolio, account.name, nickname)

            # send message to user
            signals.new_user_registered.send(sender=None, account=account, request=request)

            # record referral
            signals.record_referral.send(sender=None, account_id=account.id, referral_code=referral_code)

            # log user in
            login(request, user)

            if sub is not None:
                return redirect('/%s%s' % (sub, next_url))
            else:
                return redirect('/')

        messages.error(request, form.errors)
        storage = messages.get_messages(request)

        return render(request, self.template_name, locals())


class ArtistRequestView(mixins.LoginRequiredMixin, services_mixin.NonArtistMixin, services_mixin.MainMixin, View):
    template_name = 'forms/request.html'

    def get(self, request, *args, **kwargs):
        """
        loads signup page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        Attempts to register a user acccount
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        sub = kwargs.get('sub')
        form = forms.ArtistRequestForm(request.POST)

        if form.is_valid():
            portfolio = form.cleaned_data.get('portfolio')
            nickname = form.cleaned_data.get('nickname')

            if not helper.validate_url(portfolio):
                messages.error(request, 'Invalid URL added as online portfolio')
                storage = messages.get_messages(request)
                return render(request, self.template_name, locals())

            if Artist.objects.filter(nickname=nickname).exists():
                messages.error(request, 'Artist with nickname - {} already exists'.format(nickname))
                storage = messages.get_messages(request)
                return render(request, self.template_name, locals())

            user = request.user
            transport.request_artist_permission(user.email, portfolio, user.account.name, nickname)

            messages.success(request, "Your request has been successful sent and is being reviewed")

            if sub is not None:
                return redirect('/%s/home' % sub)
            else:
                return redirect('/home')

        messages.error(request, form.errors)
        storage = messages.get_messages(request)

        return render(request, self.template_name, locals())


class ForgotView(services_mixin.AnonymousMixin, services_mixin.MainMixin, View):
    template_name = 'forgot.html'

    def get(self, request, *args, **kwargs):
        """
        forgot password
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):

        email = request.POST['email']
        try:
            user = models.User.objects.get(email=email)

            token = AuthorizationToken.objects.filter(user=user.account, is_verification=False,
                                                      is_password_change=True, is_expired=False).first()

            if not token:
                token = AuthorizationToken.objects.create(code=get_random_string(40), user=user.account,
                                                          is_verification=False, is_password_change=True)

            ua = get_user_agent(request)
            sub = kwargs.get('sub')
            domain = helper.get_domain(sub)

            token_url = '%s%s?token=%s' % (domain, reverse('change'), token.code)

            # send welcome email to new user
            reset_message = "<p>Hello {},</p>" \
                            "<p> You have requested a password reset to access your account. " \
                            "If you didn't request a password change, " \
                            "ignore this message. </p><p>To reset your password, click on this link {}</p>" \
                            "<p>Best Regards,<br />Kaadie</p>".format(user.account.name, token_url)

            message = transport.MailService.send_mail('Password Reset', 'system@kaadie.com', 'Kaadie',
                                                      user.account.email,
                                                      reset_message, user.account.name)

            if not message:
                messages.error(request, 'No Account with this email exists')
                storage = messages.get_messages(request)
                return render(request, self.template_name, locals())

            messages.success(request, 'Reset instructions has been sent to your email.')

            if sub is not None:
                return redirect('forgot', sub=sub)
            else:
                return redirect('forgot')
        except:
            messages.error(request, 'No Account with this email exists')
            storage = messages.get_messages(request)
            return render(request, self.template_name, locals())


class ChangePasswordView(services_mixin.MainMixin, services_mixin.AnonymousMixin, View):
    template_name = 'change.html'

    def get(self, request, *args, **kwargs):
        """
        change password page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        token = request.GET.get('token', None)
        sub = kwargs.get('sub')
        if not token or not AuthorizationToken.objects.filter(is_expired=False, code=token).exists():
            messages.error(request, 'Token is Invalid')
            if sub is not None:
                return redirect('login', sub=sub)
            else:
                return redirect('login')

        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        reset password
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        token = request.POST['token']
        auth = AuthorizationToken.objects.filter(is_expired=False, code=token).first()
        account = auth.user
        user = account.user
        user.set_password(request.POST['new_password'])
        user.save()

        auth.is_expired = True
        auth.save()

        sub = kwargs.get('sub')
        messages.success(request, 'Password changed successfully')

        if sub is not None:
            return redirect('login', sub=sub)
        else:
            return redirect('login')


@services_mixin.anonymous_user
@services_mixin.main_mixin
def facebook_login(request, *args, **kwargs):
    """
    Sign In with Facebook
    :param request:
    :param args:
    :param kwargs:
    :return:
    """

    state = hashlib.md5().hexdigest()
    sub = kwargs.get('sub')
    domain = helper.get_domain(sub)

    try:
        helper.record_log(request, 'visited', request.path, 'page')
    except Exception, e:
        print('==============')
        print(e)
        print('==============')

    url = "%s?response_type=code&client_id=%s&redirect_uri=%s&state=%s&scope=%s" % (
        settings.FACEBOOK_REQUEST_TOKEN_URI, settings.FACEBOOK_APP_ID,
        '%s/%s' % (domain, settings.FACEBOOK_CALLBACK_URI),
        state, settings.FACEBOOK_SCOPE)

    return HttpResponseRedirect(url)


@services_mixin.anonymous_user
@services_mixin.main_mixin
def facebook_callback(request, *args, **kwargs):
    """
    authenticate facebook account
    :param request:
    :return: homepage
    """

    code = request.GET.get('code', None)
    if not code:
        messages.error(request, 'Invalid Response from Facebook')
        return redirect('login')

    sub = kwargs.get('sub')
    domain = helper.get_domain(sub)

    url = "%s?client_secret=%s&client_id=%s&redirect_uri=%s&code=%s" % (
        settings.FACEBOOK_AUTHENTICATE_URL, settings.FACEBOOK_APP_SECRET, settings.FACEBOOK_APP_ID,
        '%s/%s' % (domain, settings.FACEBOOK_CALLBACK_URI), code)

    resp = requests.get(url)

    if resp.status_code == 200:
        request.session['token'] = json.loads(resp.content)
        access_token = request.session['token']['access_token']
        profile_url = '%s/me?access_token=%s&fields=id, email,first_name, last_name, picture' % (
            settings.FACEBOOK_BASE_URL, access_token)
        resps = requests.get(profile_url)
        if resps.status_code == 200:
            # graph = GraphAPI(access_token=access_token, version='2.2')

            # dat = graph.get_object('me', fields='permissions')
            # friends = graph.get_connections(id='me', connection_name='friends')
            data = json.loads(resps.content)
            try:
                redirect_url = None
                password = data['id']
                username = data.get('email')
                if not username:
                    username = '%s@facebook.com' % password
                    redirect_url = settings.VALIDATE_EMAIL_URL
                user = models.User.objects.filter(Q(username=username.lower()) | Q(email=username.lower())).first()
                if user:
                    if authenticate(username=username, password=password) or \
                            Oauth.objects.filter(username=username, access_id=password).exists():
                        login(request, user)
                        return redirect('index')
                    else:
                        Oauth.objects.create(user=user.account, platform='facebook', username=username,
                                             access_id=password)
                        login(request, user)
                        return redirect('index')
                else:
                    image_url = data.get('picture', None)
                    first_name = data.get('first_name')
                    last_name = data.get('last_name')

                    user = models.User.objects.create_user(username=username, email=username, password=password,
                                                           first_name=first_name, last_name=last_name)

                    Oauth.objects.create(user=user.account, platform='facebook', username=username,
                                         access_id=password)

                    if image_url:
                        account = user.account
                        account.image_url = image_url['data']['url']
                        account.save()

                    # post on user's facebook wall
                    # attachment = {
                    #     'name': 'Join Me on Kaadie',
                    #     'link': domain,
                    #     'caption': 'Join Me on Kaadie',
                    #     'description': 'I just signed up on Kaadie. With Kaadie, sending cards is as easy as ABC! '
                    #                    'Send e-cards and get feedbacks from your loved ones for FREE.',
                    #     'picture': 'http://kaadie.com/images/k_logo-white.png'
                    # }

                    # graph.put_wall_post(message='Check this out...', attachment=attachment)

                    # login new user
                    login(request, user)

                    if redirect_url:
                        account = user.account
                        account.email_valid = False
                        account.save()
                        return redirect(redirect_url)
                    next_url = '/'
                    if request.session.get('next'):
                        next_url = request.session.get('next')
                        del request.session['next']
                    return redirect(next_url)
            except Exception:
                messages.error(request, 'Invalid request')
                return redirect('login')

    messages.error(request, 'Invalid request')
    return redirect('login')


@services_mixin.anonymous_user
@services_mixin.main_mixin
def twitter_login(request):
    """
    Sign In with twitter
    :param request:
    :param args:
    :param kwargs:
    :return:
    """
    consumer = Consumer(settings.TWITTER_API_KEY, settings.TWITTER_API_SECRET)
    client = Client(consumer)
    resp, content = client.request(settings.TWITTER_REQUEST_TOKEN_URL, "GET")
    if resp['status'] != '200':
        raise Exception("Invalid response from Twitter.")

    request.session['request_token'] = dict(urlparse.parse_qsl(content))

    url = "%s?oauth_token=%s&redirect_uri=%s" % (settings.TWITTER_AUTHENTICATE_URL,
                                                 request.session['request_token']['oauth_token'],
                                                 settings.TWITTER_REDIRECT_URI)

    return HttpResponseRedirect(url)


@services_mixin.anonymous_user
@services_mixin.main_mixin
def twitter_authenticate(request):
    """
    authenticate twitter account
    :param request:
    :return: homepage
    """
    # Step 1. Use the request token in the session to build a new client.
    token = Token(request.session['request_token']['oauth_token'],
                  request.session['request_token']['oauth_token_secret'])
    token.set_verifier(request.GET['oauth_verifier'])
    consumer = Consumer(settings.TWITTER_API_KEY, settings.TWITTER_API_SECRET)
    client = Client(consumer, token)

    # Step 2. Request the authorized access token from Twitter.
    resp, content = client.request(settings.TWITTER_ACCESS_TOKEN_URL, "GET")
    if resp['status'] != '200':
        raise Exception("Invalid response from Twitter.")

    access_token = dict(urlparse.parse_qsl(content))

    try:
        user = models.User.objects.get(username='%s@twitter.com' % access_token['screen_name'])
    except models.User.DoesNotExist:
        # When creating the user I just use their screen_name@twitter.com
        # for their email and the oauth_token_secret for their password.
        # These two things will likely never be used. Alternatively, you
        # can prompt them for their email here. Either way, the password
        # should never be used.
        _name = access_token['screen_name'].split(' ')
        first_name = _name[0]

        if len(_name) > 1:
            last_name = _name[1]
        else:
            last_name = ''

        user = models.User.objects.create_user(username='%s@twitter.com' % access_token['screen_name'],
                                               email='%s@twitter.com' % access_token['screen_name'],
                                               password='%s_%s' % (access_token['user_id'],
                                                                   access_token['oauth_token_secret']),
                                               first_name=first_name, last_name=last_name)

        # Save our permanent token and secret for later.
    if user:
        account = Account.objects.filter(user=user.id).first()
        account.image_url = ''
        account.save()
        user = authenticate(username=user.username, password='%s_%s' % (access_token['user_id'],
                                                                        access_token['oauth_token_secret']))

        login(request, user)

        return redirect('home')

    messages.error(request, 'Invalid request')

    return redirect('login')


class ChangeEmailView(services_mixin.MainMixin, mixins.LoginRequiredMixin, View):
    template_name = 'change_email.html'

    def get(self, request, *args, **kwargs):
        """
        change email page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        sub = kwargs.get('sub')
        if request.user.account.email_valid:
            if sub is not None:
                return redirect('index', sub=sub)
            else:
                return redirect('index')

        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        reset password
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        sub = kwargs.get('sub')
        email = request.POST['email']
        if models.User.objects.filter(Q(username=email.lower()) | Q(email=email.lower())).exists():
            messages.error(request, 'An account with this email already exists.')
            if sub is not None:
                return redirect('change_email', sub=sub)
            else:
                return redirect('change_email')

        user = models.User.objects.get(pk=request.user.pk)
        user.email = email
        user.save()

        account = user.account
        account.email_valid = True
        account.save()

        messages.success(request, 'Email changed successfully')

        next_url = 'home'
        if request.session.get('next'):
            next_url = request.session.get('next')
            del request.session['next']

        try:
            helper.record_log(request, 'changed email', serializers.AccountSerializer(account).data, 'object')
        except Exception, e:
            print('==============')
            print(e)
            print('==============')

        return redirect(next_url)


class HomeView(services_mixin.MainMixin, mixins.LoginRequiredMixin, services_mixin.ValidateEmailMixin, TemplateView):
    template_name = 'profile.html'

    def get(self, request, *args, **kwargs):
        """
        loads profile page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user
        account = user.account
        saved_cards = []
        ua = get_user_agent(request)

        if ua.is_mobile or ua.is_tablet:
            self.template_name = 'mobile_profile.html'

        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)

        try:
            banners = cache.fetch_cache_banners()
        except Exception, e:
            is_db = True
            banners = Banner.objects.filter(is_active=True).all()

        premium = account.is_premium
        received_cards = ReceivedCard.objects.filter(Q(recipient=user.email) | Q(recipient=user.account.phone)).all()
        # designs = SavedCardImage.objects.filter(saved_card__user=account.pk)
        cards = Card.objects.filter(user_id=user.account.pk, is_active=True)

        # user wallet
        wallet = Wallet.objects.filter(account=account.pk).first()
        if not wallet:
            wallet = Wallet.objects.create(account=account)

        # referral information
        try:
            if not hasattr(account, 'setting'):
                setting = Settings.objects.create(account=account, referral_code=helper.generate_referral_code())
            setting = account.setting
            random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                     body={'query': {
                                         'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                             'random_score': {'seed': random.randint(1, 1000000)}
                                         }]}}, 'filter': {
                                         'bool': {'must': [{'term': {'is_public': True}}]}
                                     }, 'sort': [{"_score": {"order": "desc"}}]}, size=1)

            random_hits = random_query['hits']

            if random_hits['total'] == 0:
                random_card = Card.objects.filter(is_active=True, is_public=True).order_by('?').first()

            else:
                _random = random_hits['hits']
                random_card = helper.Payload(_random[0]['_source'])

        except Exception, e:
            random_card = Card.objects.filter(is_active=True, is_public=True).order_by('?').first()

        return render(request, self.template_name, locals())


class SectionsView(services_mixin.MainMixin, services_mixin.ValidateEmailMixin, TemplateView):
    template_name = 'sections.html'

    def get(self, request, *args, **kwargs):
        """
        loads login page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user
        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)

        try:
            sections = cache.fetch_cache_sections(organization.code)
        except Exception, e:
            sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
                'position').all()
            is_db = True

        premium = False

        if user.is_authenticated():
            premium = user.account.is_premium

        return render(request, self.template_name, locals())


class SectionView(services_mixin.MainMixin, services_mixin.ValidateEmailMixin, View):
    template_name = 'section.html'

    def get(self, request, slug, *args, **kwargs):
        """
        loads sections page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        user = request.user
        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)
        saved_cards = []

        premium = False
        if user.is_authenticated():
            premium = user.account.is_premium

        try:
            if user.is_authenticated() and hasattr(user, 'account'):
                saved_cards = cache.fetch_cache_saved_cards(request.user.account.pk)

            sections = cache.fetch_cache_sections(organization.code)
            section = next(c for c in sections if c.slug == slug)
            if section:
                categories = filter(lambda x: x.is_active, section.categories)
                _cards = [redis_obj.hgetall('%s:cards' % cat.slug).values() for cat in categories]
                json_cards = list(set(list(chain.from_iterable(_cards))))
                cards = [helper.Payload(json.loads(c)) for c in json_cards]
            else:
                section = Section.objects.filter(slug=slug).first()
                categories = section.categories.filter(is_active=True).all()
                cards = section.cards
        except Exception, e:
            section = Section.objects.filter(slug=slug).first()
            categories = section.categories.filter(is_active=True).all()
            cards = section.cards
            sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
                'position').all()
            is_db = True
            if user.is_authenticated() and hasattr(user, 'account'):
                saved_cards = [c.card for c in user.account.saved_cards.all()]

        saved_ids = ','.join([str(c.pk) for c in saved_cards]) if len(saved_cards) > 0 else ''
        if not section:
            if sub is not None:
                return redirect('sections', sub=sub)
            else:
                return redirect('sections')

        return render(request, self.template_name, locals())


class CategoryView(services_mixin.MainMixin, services_mixin.ValidateEmailMixin, View):
    def get(self, request, pk, slug, *args, **kwargs):
        """
        loads login page
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        template_name = 'category.html'

        sub = kwargs.get('sub')
        saved_cards = []
        user = request.user

        premium = False
        if user.is_authenticated():
            premium = user.account.is_premium

        organization = helper.get_organization(sub)

        try:
            sections = cache.fetch_cache_sections(organization.code)
            _section = redis_obj.hget('%s_sections' % organization.code, str(pk))
            section = helper.Payload(json.loads(_section))

            if user.is_authenticated() and hasattr(user, 'account'):
                saved_cards = cache.fetch_cache_saved_cards(user.account.pk)

            _category = redis_obj.hget('category', '%s:%s' % (pk, slug))
            if _category:
                category = helper.Payload(json.loads(_category))
                cards = [helper.Payload(json.loads(c)) for c in redis_obj.hgetall('%s:cards' % category.slug).values()]
            else:
                category = Category.objects.filter(section=int(pk), slug=str(slug)).first()
                if not category:
                    raise Http404
                cards = category.cards.all()
        except Exception, e:
            category = Category.objects.filter(section=int(pk), slug=str(slug)).first()
            if not category:
                raise Http404
            cards = category.cards.all()
            sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
                'position').all()
            is_db = True
            if user.is_authenticated() and hasattr(user, 'account'):
                saved_cards = [c.card for c in user.account.saved_cards.all()]

        saved_ids = ','.join([str(c.pk) for c in saved_cards]) if len(saved_cards) > 0 else ''
        if not category:
            if sub is not None:
                return redirect('index', sub=sub)
            else:
                return redirect('index')

        return render(request, template_name, locals())


@services_mixin.main_mixin
def view_card(request, slug, *args, **kwargs):
    """
    publicly accessible page describing a card
    :param request:
    :param  slug: card slug
    :return:
    """
    sub = kwargs.get('sub')
    ua = get_user_agent(request)
    user = request.user
    organization = helper.get_organization(sub)
    saved_cards = []

    try:
        card_query = es.search(index=settings.ES_INDEX, doc_type='card', body={'query': {'match': {'slug': slug}}})
        card_hits = card_query['hits']

        if card_hits['total'] == 0:
            card = Card.objects.filter(slug=slug).first()

        else:
            _card = card_hits['hits'][0]
            card = helper.Payload(_card['_source'])

        if not card:
            raise Http404

        if user.is_authenticated():
            saved_cards = cache.fetch_cache_saved_cards(user.account.pk)

        sections = cache.fetch_cache_sections(organization.code)

        random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                 body={'query': {
                                     'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                         'random_score': {'seed': random.randint(1, 1000000)}
                                     }]}}, 'filter': {
                                     'bool': {'must': [{'term': {'is_public': True}}],
                                              'must_not': [{'term': {'slug': slug}}]}
                                 }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)

        random_hits = random_query['hits']

        if random_hits['total'] == 0:
            random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=card.slug).order_by('?')[
                           :6].all()

        else:
            _random = random_hits['hits']
            random_cards = filter(lambda x: x.slug != slug, [helper.Payload(i['_source']) for i in _random])

    except Exception, e:
        card = Card.objects.filter(slug=slug).first()

        if not card:
            raise Http404

        random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=card.slug).order_by('?')[
                       :6].all()
        sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by('position').all()
        is_db = True
        if user.is_authenticated():
            saved_cards = [c.card for c in user.account.saved_cards.all()]

    premium = False
    saved_ids = ','.join([str(c.pk) for c in saved_cards]) if len(saved_cards) > 0 else ''

    if user.is_authenticated():
        premium = user.account.is_premium

    return render(request, 'card.html', locals())


class EditCardView(services_mixin.MainMixin, View):
    template_name = 'forms/edit_card.html'

    def get(self, request, slug, *args, **kwargs):
        """
        Edit Card
        :param request:
        :param slug:
        :return:
        """
        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)

        if organization is None:
            raise Http404

        user = request.user
        premium = False
        if user.is_authenticated():
            premium = user.account.is_premium

        try:
            sections = cache.fetch_cache_sections(organization.code)

            card_query = es.search(index=settings.ES_INDEX, doc_type='card', body={'query': {'match': {'slug': slug}}})

            card_hits = card_query['hits']

            if card_hits['total'] == 0:
                card = Card.objects.filter(slug=slug).first()
                card_categories = ','.join([str(c.pk) for c in card.categories.all()])

            else:
                _card = card_hits['hits'][0]
                card = helper.Payload(_card['_source'])
                card_categories = ','.join([str(Category.objects.filter(slug=i).pk) for i in card.slugs])

            cat_query = es.search(index=settings.ES_INDEX, doc_type='category', body={'query': {'match_all': {}}})

            cat_hits = cat_query['hits']

            if cat_hits['total'] == 0:
                categories = Category.objects.all()

            else:
                _cats = cat_hits['hits']
                categories = [helper.Payload(_c['_source']) for _c in _cats]

        except Exception, e:
            categories = Category.objects.all()
            sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
                'position').all()
            is_db = True
            card = Card.objects.filter(slug=str(slug)).first()
            card_categories = ','.join([str(c.pk) for c in card.categories.all()])
            if user.is_authenticated():
                saved_cards = [c.card for c in user.account.saved_cards.all()]

        form = forms.PremiumCardForm(initial=card.__dict__)

        if not card:
            if sub is not None:
                return redirect('index', sub=sub)
            else:
                return redirect('index')

        image = card.image_url

        # try:
        #     if helper.validate_url(card.image_url):
        #         image = "data:image/png;base64,%s" % helper.get_as_base64(card.image_url)
        # except:
        #     messages.error(request, 'Please check your internet Connection!!!')
        #     if sub is not None:
        #         return redirect('home', sub=sub)
        #     else:
        #         return redirect('home')

        return render(request, self.template_name, locals())

    def post(self, request, slug, *args, **kwargs):
        """
        Update card with slug matching slug
        :param request:
        :param slug:
        :return:
        """
        sub = kwargs.get('sub')
        card = Card.objects.filter(slug=str(slug)).first()
        if not card:
            if sub is not None:
                return redirect('index', sub=sub)
            else:
                return redirect('index')

        form = forms.EditCardForm(request.POST)

        if form.is_valid():
            description = form.cleaned_data.get('description', card.description)
            image_url = form.cleaned_data.get('image_url', card.image_url)
            # uploaded = helper.upload_b64_s3(form.cleaned_data['card_image'], '%s.jpg' % card.title)
            # if uploaded:
            price = form.cleaned_data.get('price', card.price)
            # image_url = '%s/%s' % (settings.S3_BASE_ADDRESS, '%s.jpg' % card.title)
            card.description = description
            card.image_url = image_url
            card.price = price

            keys = form.cleaned_data['categories']
            if keys:
                categories = list(Category.objects.filter(pk__in=keys.split(',')).all())
                card.categories.clear()
                card.categories.add(*categories)
            card.is_active = False
            card.save()

            messages.success(request, "Your card has been successful updated and is being reviewed")

            try:
                helper.record_log(request, 'successfully updated', serializers.CardSerializer(card).data, 'object')
            except Exception, e:
                print('==============')
                print(e)
                print('==============')

            if sub is not None:
                return redirect('edit_card', slug=card.slug, sub=sub)
            else:
                return redirect('edit_card', slug=card.slug)

        else:
            messages.error(request, form.errors)
            if sub is not None:
                return redirect('edit_card', slug=card.slug, sub=sub)
            else:
                return redirect('edit_card', slug=card.slug)


class SendCardView(services_mixin.ValidateEmailMixin, services_mixin.MainMixin, View):
    template_name = 'forms/send_card.html'

    def get(self, request, slug, *args, **kwargs):
        """
        send card view
        :param request:
        :param slug:
        :return:
        """
        sub = kwargs.get('sub')
        user = request.user

        organization = helper.get_organization(sub)

        card = Card.objects.filter(slug=str(slug)).first()

        if not card:
            if sub is not None:
                return redirect('index', sub=sub)
            else:
                return redirect('index')

        json_card = json.dumps(serializers.CardSerializer(card).data)

        saved_cards = []
        premium = False
        sending_limit = settings.SENDING_LIMIT

        if user.is_authenticated() and hasattr(user, 'account'):
            premium = user.account.is_premium
            setting = Settings.objects.filter(account=user.account.pk).first()

            if setting:
                sending_limit = setting.sending_limit

        try:
            sections = cache.fetch_cache_sections(organization.code)

            if user.is_authenticated():
                saved_cards = cache.fetch_cache_saved_cards(user.account.pk)

            random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                     body={'query': {
                                         'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                             'random_score': {'seed': random.randint(1, 1000000)}
                                         }]}}, 'filter': {
                                         'bool': {'must': [{'term': {'is_public': True}}],
                                                  'must_not': [{'term': {'slug': card.slug}}]}
                                     }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)

            random_hits = random_query['hits']

            if random_hits['total'] == 0:
                random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=card.slug).order_by(
                    '?')[:6].all()

            else:
                _random = random_hits['hits']
                random_cards = filter(lambda x: x.slug != card.slug, [helper.Payload(i['_source']) for i in _random])

        except:
            random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=card.slug).order_by('?')[
                           :6].all()

            sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
                'position').all()
            is_db = True
            if user.is_authenticated():
                saved_cards = [c.card for c in user.account.saved_cards.all()]

        ua = get_user_agent(request)
        saved_ids = ','.join([str(c.pk) for c in saved_cards]) if len(saved_cards) > 0 else ''
        image = card.image_url
        if hasattr(card, 'b64_url'):
            image = card.b64_url

        # try:
        #     query = es.get(index=settings.ES_INDEX, doc_type='card', id=card_.pk)
        #
        #     if query['found']:
        #         card = helper.Payload(query['_source'])
        #         image = card.image_url
        #     else:
        #         # if _card.display_image is not None:
        #         image = "data:image/png;base64,%s" % helper.get_as_base64(card_.image_url)
        #         # image = "data:image/png;base64,%s" % helper.get_as_base64(card.display_image)
        #
        # except:
        #     messages.error(request, 'Please check your internet Connection!!!')
        #     if sub is not None:
        #         return redirect('home', sub=sub)
        #     else:
        #         return redirect('home')

        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        file_path = None
        sub = kwargs.get('sub')
        if request.FILES.get('csv'):
            _file = request.FILES['csv']
            fs = FileSystemStorage()
            filename = fs.save('uploads/%s_%s' % (time.time(), _file.name), _file)
            file_path = os.path.join(settings.MEDIA_ROOT, filename)

            if not helper.validate_file_uploaded(file_path):
                messages.error(request, 'Invalid File Format')

                if sub is not None:
                    return redirect('send_card', sub=sub)
                else:
                    return redirect('send_card')

        card_id = request.POST.get('card_id')
        card = Card.objects.get(pk=int(card_id))
        saved_cards = []
        as_json = request.POST.get('as_json')
        image = card.image_url
        random_cards = helper.load_random_cards(card.slug)
        json_card = json.dumps(serializers.CardSerializer(card).data)
        saved_ids = ','.join([str(c.pk) for c in saved_cards]) if len(saved_cards) > 0 else ''

        if len(request.POST.get('receipt')) == 0 and request.FILES.get('csv') is None:
            messages.error(request, 'Recipient(s) missing')
            storage = messages.get_messages(request)

            if as_json:
                return services_encoders.JSONResponse({'message': 'Recipient(s) missing', 'status': 'error',
                                                       'redirect_url': ''})

            return render(request, self.template_name,
                          {'card': card, 'user': request.user, 'sections': Section.objects.all(), 'is_db': True,
                           'saved_cards': saved_cards, 'storage': storage, 'image': image,
                           'random_cards': random_cards, 'json_card': json_card, 'saved_ids': saved_ids})

        email_recipients = filter(lambda x: x != '', request.POST.get('receipt').split(','))
        # phone_recipients = filter(lambda x: x != '', request.POST.get('receipt_phone').split(','))
        phone_recipients = []
        sms_counts = len(phone_recipients)
        card_image = card.image_url

        sender_name = request.POST.get('sender_name')
        message = request.POST.get('message')
        user = request.user
        scheduled_time = request.POST.get('scheduled_time')

        if user.is_authenticated():

            sender_email = request.user.email

            # count = len(email_recipients + phone_recipients)
            # sending_limit = settings.SENDING_LIMIT

            # setting = Settings.objects.filter(account=user.pk).first()

            # if hasattr(user.account, 'setting'):
            #     sending_limit = user.account.setting.sending_limit
            #
            # response_data = {'card': card, 'user': request.user, 'sections': Section.objects.all(),
            #                  'saved_cards': saved_cards, 'image': image,
            #                  'random_cards': random_cards, 'is_db': True}

            # if user.account.is_premium:
            #     # check sms credits
            #     credit = payment.check_sms_credit(user.id)
            #
            #     if 0 < credit < sms_counts:
            #         messages.error(request,
            #                        "You don't have enough Kaadie credits to complete this. You currently have only "
            #                        "%s. To send more cards, buy more Kaadie credits.")
            #
            #         storage = messages.get_messages(request)
            #         response_data.update({'storage': storage})
            #
            #         return render(request, self.template_name, response_data)
            # else:
            #     # check sms limit
            #     count_balance = payment.sms_limit_balance(user.id)
            #     threshold_balance = sending_limit - user.account.sent_count
            #
            #     if threshold_balance < count:
            #         messages.error(request, 'You have exceeded your sending limit. Become a premium user. '
            #                                 'You can only send %s more free cards'
            #                        % threshold_balance)
            #
            #         storage = messages.get_messages(request)
            #         response_data.update({'storage': storage})
            #
            #         return render(request, self.template_name, response_data)
            #
            #     if 0 < count_balance < sms_counts:
            #         messages.error(request, 'You have exceed your sending limit. Become a premium user and buy Kaadie '
            #                                 'credits to send more cards. You can only send %s more free cards'
            #                        % count_balance)
            #
            #         storage = messages.get_messages(request)
            #         response_data.update({'storage': storage})
            #
            #         return render(request, self.template_name, response_data)

            # detect domain
            domain = helper.get_domain(sub)

            if scheduled_time:
                transport.schedule_card.delay(scheduled_time, card.pk, user.account.pk, email_recipients, phone_recipients,
                                              sender_name, sender_email, domain, cover_image=card_image,
                                              file_path=file_path, personalized_message=message)
            else:
                # send card
                transport.send_card(card.pk, user.account.pk, email_recipients, phone_recipients,
                                          sender_name, sender_email, domain, cover_image=card_image,
                                          file_path=file_path, personalized_message=message)

            organization = helper.get_organization(sub)

            if as_json:
                if sub is not None:
                    return services_encoders.JSONResponse({'message': 'Card Successfully Sent', 'status': 'success',
                                                           'redirect_url': '/%s' % sub})
                else:
                    return services_encoders.JSONResponse({'message': 'Card Successfully Sent', 'status': 'success',
                                                           'redirect_url': '/'})

            return render(request, self.template_name,
                          {'card': card, 'user': user, 'sections': Section.objects.all(), 'saved_ids': saved_ids,
                           'saved_cards': saved_cards, 'sent_success': True, 'organization': organization,
                           'random_cards': random_cards, 'image': image, 'is_db': True, 'json_card': json_card})

        else:
            request.session['send_data'] = dict(card_id=card.pk, email_recipients=email_recipients,
                                                phone_recipients=phone_recipients, sender_name=sender_name,
                                                message=message, cover_image=card_image, file_path=file_path,
                                                scheduled_time=scheduled_time)

            if as_json:
                if sub is not None:
                    return services_encoders.JSONResponse({'message': 'Card Successfully Sent', 'status': 'success',
                                                           'redirect_url': '/%s/accounts/login/?next=/send/cards' % sub})
                else:
                    return services_encoders.JSONResponse({'message': 'Card Successfully Sent', 'status': 'success',
                                                           'redirect_url': '/accounts/login/?next=/send/cards'})

            if sub is not None:
                return redirect('/%s/accounts/login/?next=/send/cards' % sub)
            else:
                return redirect('/accounts/login/?next=/send/cards')


class PersonalizeCardView(mixins.LoginRequiredMixin, services_mixin.ValidateEmailMixin, services_mixin.MainMixin, View):
    template_name = 'forms/personalize_card.html'

    def get(self, request, slug, *args, **kwargs):
        """
        send card view
        :param request:
        :param slug:
        :return:
        """
        sub = kwargs.get('sub')
        user = request.user
        ua = get_user_agent(request)

        if ua.is_mobile or ua.is_tablet:
            self.template_name = 'mobile_personalize.html'

        organization = helper.get_organization(sub)

        card = Card.objects.filter(slug=str(slug)).first()

        if not card:
            if sub is not None:
                return redirect('index', sub=sub)
            else:
                return redirect('index')

        json_card = json.dumps(serializers.CardSerializer(card).data)

        saved_cards = []
        premium = False
        sending_limit = settings.SENDING_LIMIT

        if user.is_authenticated() and hasattr(user, 'account'):
            premium = user.account.is_premium
            setting = Settings.objects.filter(account=user.account.pk).first()

            if setting:
                sending_limit = setting.sending_limit

        try:
            sections = cache.fetch_cache_sections(organization.code)

            if user.is_authenticated():
                saved_cards = cache.fetch_cache_saved_cards(user.account.pk)

            random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                     body={'query': {
                                         'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                             'random_score': {'seed': random.randint(1, 1000000)}
                                         }]}}, 'filter': {
                                         'bool': {'must': [{'term': {'is_public': True}}],
                                                  'must_not': [{'term': {'slug': card.slug}}]}
                                     }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)

            random_hits = random_query['hits']

            if random_hits['total'] == 0:
                random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=card.slug).order_by(
                    '?')[:6].all()

            else:
                _random = random_hits['hits']
                random_cards = filter(lambda x: x.slug != card.slug, [helper.Payload(i['_source']) for i in _random])

        except Exception, e:
            print'======='
            print e
            random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=card.slug).order_by('?')[
                           :6].all()

            sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
                'position').all()
            is_db = True
            if user.is_authenticated():
                saved_cards = [c.card for c in user.account.saved_cards.all()]

        ua = get_user_agent(request)
        saved_ids = ','.join([str(c.pk) for c in saved_cards]) if len(saved_cards) > 0 else ''
        if hasattr(card, 'b64_url'):
            image = card.b64_url
        else:
            try:
                image = "data:image/jpeg;base64,{}".format(helper.get_as_base64(card.image_url))
            except Exception, e:
                return redirect('index')

        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        file_path = None
        sub = kwargs.get('sub')
        if request.FILES.get('csv'):
            _file = request.FILES['csv']
            fs = FileSystemStorage()
            filename = fs.save('uploads/%s_%s' % (time.time(), _file.name), _file)
            file_path = os.path.join(settings.MEDIA_ROOT, filename)

            if not helper.validate_file_uploaded(file_path):
                messages.error(request, 'Invalid File Format')

                if sub is not None:
                    return redirect('personalize_card', sub=sub)
                else:
                    return redirect('personalize_card')

        card_id = request.POST.get('card_id')
        card = Card.objects.get(pk=int(card_id))
        saved_cards = []
        as_json = request.POST.get('as_json')
        image = card.image_url
        random_cards = helper.load_random_cards(card.slug)

        if len(request.POST.get('receipt')) == 0 and request.FILES.get('csv') is None:
            messages.error(request, 'Recipient(s) missing')
            storage = messages.get_messages(request)

            if as_json:
                return services_encoders.JSONResponse({'message': 'Recipient(s) missing', 'status': 'error',
                                                       'redirect_url': ''})

            return render(request, self.template_name,
                          {'card': card, 'user': request.user, 'sections': Section.objects.all(), 'is_db': True,
                           'saved_cards': saved_cards, 'storage': storage, 'image': image,
                           'random_cards': random_cards})

        email_recipients = filter(lambda x: x != '', request.POST.get('receipt').split(','))
        card_image = request.POST.get('card_image', None)
        cover_image = request.POST.get('cover_image', image)

        sender_name = request.POST.get('sender_name')
        message = request.POST.get('message', '')
        user = request.user
        scheduled_time = request.POST.get('scheduled_time')

        if user.is_authenticated():

            sender_email = request.user.email

            # detect domain
            domain = helper.get_domain(sub)

            if scheduled_time:
                transport.schedule_card.delay(scheduled_time, card.pk, user.account.pk, email_recipients, [],
                                              sender_name, sender_email, domain, card_image=card_image,
                                              cover_image=cover_image, file_path=file_path,
                                              personalized_message=message)
            else:
                transport.send_card.delay(card.pk, user.account.pk, email_recipients, [], sender_name, sender_email,
                                          domain, card_image=card_image, cover_image=cover_image, file_path=file_path,
                                          personalized_message=message)

            organization = helper.get_organization(sub)

            if as_json:
                if sub is not None:
                    return services_encoders.JSONResponse({'message': 'Card Successfully Sent', 'status': 'success',
                                                           'redirect_url': '/%s' % sub})
                else:
                    return services_encoders.JSONResponse({'message': 'Card Successfully Sent', 'status': 'success',
                                                           'redirect_url': '/'})

            return render(request, self.template_name,
                          {'card': card, 'user': user, 'sections': Section.objects.all(),
                           'saved_cards': saved_cards, 'sent_success': True, 'organization': organization,
                           'random_cards': random_cards, 'image': image, 'is_db': True})

        else:
            request.session['send_data'] = dict(scheduled_time, card_id=card.pk, email_recipients=email_recipients,
                                                phone_recipients=[], sender_name=sender_name,
                                                message=message, card_image=card_image, cover_image=cover_image,
                                                file_path=file_path)

            if as_json:
                if sub is not None:
                    return services_encoders.JSONResponse({'message': 'Card Successfully Sent', 'status': 'success',
                                                           'redirect_url': '/%s/accounts/login/?next=/send/cards' % sub})
                else:
                    return services_encoders.JSONResponse({'message': 'Card Successfully Sent', 'status': 'success',
                                                           'redirect_url': '/accounts/login/?next=/send/cards'})

            if sub is not None:
                return redirect('/%s/accounts/login/?next=/send/cards' % sub)
            else:
                return redirect('/accounts/login/?next=/send/cards')


@services_mixin.main_mixin
def send_card(request, *args, **kwargs):
    """
    send card
    :param request:
    :return:
    """
    sub = kwargs.get('sub')
    organization = helper.get_organization(sub)
    data = request.session.get('send_data')
    if not len(data.keys()):
        if sub is not None:
            return redirect('index', sub=sub)
        else:
            return redirect('index')

    email_recipients = filter(lambda x: x != '', data.get('email_recipients', []))
    phone_recipients = filter(lambda x: x != '', data.get('phone_recipients', []))
    # sms_counts = len(phone_recipients)

    sender_name = data.get('sender_name')
    message = data.get('message', '')
    card_id = data.get('card_id')
    card_image = data.get('card_image')
    cover_image = data.get('cover_image')
    file_path = data.get('file_path')
    scheduled_time = data.get('scheduled_time')

    if not card_id or len(email_recipients) == 0:
        messages.error(request, 'Recipient(s) missing')
        if sub is not None:
            return redirect('index', sub=sub)
        else:
            return redirect('index')

    try:
        del request.session['send_data']
        sender_email = request.user.username
        user = request.user
        card = Card.objects.get(pk=card_id)
        json_card = json.dumps(serializers.CardSerializer(card).data)
        domain = helper.get_domain(sub)
        image = card.image_url
        saved_cards = [c.card for c in user.account.saved_cards.all()]
        random_cards = helper.load_random_cards(card.slug)

        # response_data = {'card': card, 'user': request.user, 'sections': Section.objects.all(),
        #                  'saved_cards': saved_cards, 'image': image,
        #                  'random_cards': random_cards, 'is_db': True}

        # if user.account.is_premium:
        #     # check sms credits
        #     credit = payment.check_sms_credit(user.id)
        #
        #     if 0 < credit < sms_counts:
        #         messages.error(request,
        #                        "You don't have enough Kaadie credits to complete this. You currently have only "
        #                        "%s. To send more cards, buy more Kaadie credits.")
        #
        #         storage = messages.get_messages(request)
        #         response_data.update({'storage': storage})
        #
        #         return render(request, 'forms/send_card.html', response_data)
        # else:
        #     # check sms limit
        #     count_balance = payment.sms_limit_balance(user.id)
        #     sending_limit = settings.SENDING_LIMIT
        #     if user.account.setting:
        #         sending_limit = user.account.setting.sending_limit
        #
        #     threshold_balance = sending_limit - user.account.sent_count
        #     count = len(email_recipients + phone_recipients)
        #
        #     if threshold_balance > count:
        #         messages.error(request, 'You have exceed your sending limit. Become a premium user. '
        #                                 'You can only send %s more free cards'
        #                        % threshold_balance)
        #
        #         storage = messages.get_messages(request)
        #         response_data.update({'storage': storage})
        #
        #         return render(request, 'forms/send_card.html', response_data)
        #
        #     if 0 < count_balance < sms_counts:
        #         messages.error(request, 'You have exceed your sending limit. Become a premium user and buy Kaadie '
        #                                 'credits to send more cards. You can only send %s more free cards'
        #                        % count_balance)
        #
        #         storage = messages.get_messages(request)
        #         response_data.update({'storage': storage})
        #
        #         return render(request, 'forms/send_card.html', response_data)

        # pub = transport.publish_message('app.helper', 'send_card', **{'card_id': card.pk, 'user_id': user.account.pk,
        #                                                               'email_recipients': email_recipients,
        #                                                               'phone_recipients': phone_recipients,
        #                                                               'personalized_message': message,
        #                                                               'sender_name': sender_name,
        #                                                               'sender': sender_email, 'domain': domain,
        #                                                               'card_image': card_image, 'file_path': file_path})
        #
        # if not pub:
        if scheduled_time:
            transport.schedule_card.delay(scheduled_time, card.pk, user.account.pk, email_recipients, phone_recipients,
                                          sender_name, sender_email, domain, card_image, cover_image,
                                          file_path=file_path, personalized_message=message)
        else:
            transport.send_card.delay(card.pk, user.account.pk, email_recipients, phone_recipients, sender_name,
                                      sender_email, domain, card_image, cover_image, file_path=file_path,
                                      personalized_message=message)

        return render(request, 'forms/send_card.html',
                      {'card': card, 'user': user, 'sections': Section.objects.all(),
                       'saved_cards': saved_cards, 'sent_success': True, 'organization': organization,
                       'random_cards': random_cards, 'image': image, 'is_db': True, 'json_card': json_card})
    except Exception, e:
        if sub is not None:
            return redirect('index', sub=sub)
        else:
            return redirect('index')


class CreateCardView(mixins.LoginRequiredMixin, services_mixin.MainMixin, View):
    template_name = 'forms/create_card.html'

    def get(self, request, *args, **kwargs):
        """
        premium user creating cards
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        sub = kwargs.get('sub')
        user = request.user
        organization = helper.get_organization(sub)

        form = forms.PremiumCardForm()

        try:
            sections = cache.fetch_cache_sections(organization.code)
            cat_query = es.search(index=settings.ES_INDEX, doc_type='category', body={'query': {'match_all': {}}})

            cat_hits = cat_query['hits']

            if cat_hits['total'] == 0:
                categories = Category.objects.all()

            else:
                _cats = cat_hits['hits']
                categories = [helper.Payload(_c['_source']) for _c in _cats]
        except Exception, e:
            sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
                'position').all()
            categories = Category.objects.all()
            is_db = True

        premium = request.user.account.is_premium

        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        creating card by a premium user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        sub = kwargs.get('sub')
        form = forms.PremiumCardForm(request.POST)

        if sub is not None:
            redirect_url = '/%s/cards/create' % sub
        else:
            redirect_url = '/cards/create'

        if form.is_valid():
            # create a new card
            title = form.cleaned_data['title']
            description = form.cleaned_data.get('description', '')
            keys = form.cleaned_data['categories'].split(',')
            categories = list(Category.objects.filter(pk__in=keys).all())
            price = form.cleaned_data.get('price', 0.00)
            image_url = form.cleaned_data.get('image_url', '')

            card = Card.objects.create(title=title, description=description, price=price,
                                       image_url=image_url, user=request.user.account, is_public=True)
            card.categories.add(*categories)
            card.save()
            card_url = '{}/admin/app/card/{}/change/'.format(settings.DOMAIN, card.pk)
            message = "<p>Hello Admin,</p>" \
                      "<p>A new card - {} has been created by {}. <br/> It needs your approval.</p>" \
                      "<p>Best Regards,<br />Kaadie</p>".format(card_url, request.user.account.name)

            transport.MailService.send_mail('New Card created', settings.ADMIN_ACCOUNT, 'Kaadie',
                                            settings.ADMIN_ACCOUNT, message, "Kaadie Cards")
            success_msg = "Your card has been successful created and is being reviewed."
            messages.success(request, success_msg)

            if sub is None:
                redirect_url = '/home'
            else:
                redirect_url = "/%s/home" % sub

            return redirect(redirect_url)

        else:
            messages.error(request, form.errors)
            return redirect(redirect_url)


@services_mixin.main_mixin
def sent_card(request, id, *args, **kwargs):
    """
    sent cards
    :param request:
    :param id:
    :param args:
    :param kwargs:
    :return:
    """
    template_name = 'sent_card.html'
    sub = kwargs.get('sub')
    organization = helper.get_organization(sub)
    saved_cards = []

    if organization is None:
        raise Http404

    user = request.user
    premium = False

    if user.is_authenticated():
        premium = user.account.is_premium

    # retrieves received card
    try:
        r_card = ReceivedCard.objects.filter(notification_id=id).first()
        s_card = r_card.sent_card
        card = s_card.card
        notification_id = s_card.notification_id or r_card.notification_id

        comments = Comments.objects.filter(Q(sent_card_id=s_card.pk) | Q(received_card_id=r_card.pk))
    except Exception, e:
        messages.error(request, 'The Card you requested does not exist on Kaadie')
        if sub is not None:
            return redirect('home', sub=sub)
        else:
            return redirect('home')

    # retrieve cards and sections
    try:
        sections = cache.fetch_cache_sections(organization.code)
        if user.is_authenticated():
            saved_cards = cache.fetch_cache_saved_cards(user.account.pk)
        random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                 body={'query': {
                                     'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                         'random_score': {'seed': random.randint(1, 1000000)}
                                     }]}}, 'filter': {
                                     'bool': {'must': [{'term': {'is_public': True}}],
                                              'must_not': [{'term': {'slug': card.slug}}]}
                                 }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)

        random_hits = random_query['hits']

        if random_hits['total'] == 0:
            random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=card.slug).order_by('?')[
                           :6].all()

        else:
            _random = random_hits['hits']
            random_cards = filter(lambda x: x.slug != card.slug, [helper.Payload(i['_source']) for i in _random])
    except Exception, e:
        random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=card.slug).order_by('?')[
                       :6].all()
        sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
            'position').all()
        is_db = True
        if user.is_authenticated() and hasattr(user, 'account'):
            saved_cards = [c.card for c in user.account.saved_cards.all()]

    saved_ids = ','.join([str(c.pk) for c in saved_cards]) if len(saved_cards) > 0 else ''

    return render(request, template_name, locals())


@services_mixin.main_mixin
def shared_card(request, id, *args, **kwargs):
    """
    shared cards
    :param request:
    :param id:
    :param args:
    :param kwargs:
    :return:
    """
    template_name = 'shared_card.html'
    sub = kwargs.get('sub')
    organization = helper.get_organization(sub)
    saved_cards = []

    user = request.user
    premium = False

    if user.is_authenticated():
        premium = user.account.is_premium

    # retrieves shared card
    try:
        s_card = SharedCard.objects.filter(notification_id=id).first()
        card = s_card.card
        notification_id = s_card.notification_id

        comments = Comments.objects.filter(shared_card_id=s_card.pk)
    except Exception, e:
        messages.error(request, 'The Card you requested does not exist on Kaadie')
        if sub is not None:
            return redirect('home', sub=sub)
        else:
            return redirect('home')

    # retrieve cards and sections
    try:
        sections = cache.fetch_cache_sections(organization.code)
        if user.is_authenticated():
            saved_cards = cache.fetch_cache_saved_cards(user.account.pk)
        random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                 body={'query': {
                                     'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                         'random_score': {'seed': random.randint(1, 1000000)}
                                     }]}}, 'filter': {
                                     'bool': {'must': [{'term': {'is_public': True}}],
                                              'must_not': [{'term': {'slug': card.slug}}]}
                                 }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)

        random_hits = random_query['hits']

        if random_hits['total'] == 0:
            random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=card.slug).order_by('?')[
                           :6].all()

        else:
            _random = random_hits['hits']
            random_cards = filter(lambda x: x.slug != card.slug, [helper.Payload(i['_source']) for i in _random])
    except Exception, e:
        random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=card.slug).order_by('?')[
                       :6].all()
        sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by('position').all()
        is_db = True
        if user.is_authenticated() and hasattr(user, 'account'):
            saved_cards = [c.card for c in user.account.saved_cards.all()]

    saved_ids = ','.join([str(c.pk) for c in saved_cards]) if len(saved_cards) > 0 else ''

    return render(request, template_name, locals())


@services_mixin.main_mixin
def about(request, *args, **kwargs):
    """
    About Us page
    :param request:
    :return:
    """
    template_name = 'about.html'
    premium = False
    if request.user.is_authenticated():
        premium = request.user.account.is_premium

    return render(request, template_name, locals())


@services_mixin.main_mixin
def faq(request, *args, **kwargs):
    """
    Frequently Ask Questions page
    :param request:
    :return:
    """
    template_name = 'faq.html'
    sub = kwargs.get('sub')
    user = request.user
    organization = helper.get_organization(sub)

    premium = False
    if request.user.is_authenticated():
        premium = request.user.account.is_premium

    return render(request, template_name, locals())


@services_mixin.main_mixin
def how_it_works(request, *args, **kwargs):
    """
    How it works
    :param request:
    :return:
    """

    template_name = 'how_it_works.html'
    sub = kwargs.get('sub')
    user = request.user
    organization = helper.get_organization(sub)

    premium = False
    if request.user.is_authenticated():
        premium = request.user.account.is_premium

    return render(request, template_name, locals())


class PremiumView(services_mixin.MainMixin, View):
    template_name = 'premium.html'

    def get(self, request, *args, **kwargs):
        """
        premium page
        :return:
        """
        sub = kwargs.get('sub')
        user = request.user
        organization = helper.get_organization(sub)

        premium = False
        if request.user.is_authenticated():
            premium = request.user.account.is_premium

        try:
            sections = cache.fetch_cache_sections(organization.code)
        except Exception, e:
            sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
                'position').all()
            is_db = True

        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        enable premium feature
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        sub = kwargs.get('sub')
        if not request.user.is_authenticated():
            if sub is not None:
                return redirect('/%s/accounts/login/?next=/%s/premium' % (sub, sub))
            else:
                return redirect('/accounts/login/?next=/premium')

        account = request.user.account

        if account.is_premium:
            if sub is not None:
                return redirect('home', sub=sub)
            else:
                return redirect('home')

        account.is_premium = True
        account.save()

        # premium_message = "<p>Hello {},</p>" \
        #                   "<p>Welcome to the Kaadie premium user club. <br/> You can now create your own custom " \
        #                   "designed cards.<br/> Add personalized messages or designs to our wide array of available " \
        #                   "designs to your loved one.</p><p>Best Regards,<br />Kaadie</p>".format(account.name)

        premium_message = "<p>Hello {},</p>" \
                          "<p>Welcome to the Kaadie premium. <br/> You can now personalize your kaadie experience." \
                          "<br/> Create your own custom designed cards, add personalized messages and pictures." \
                          "Leave your friends speechless with your own thought and custom design.</p><p>Best Regards," \
                          "<br />Kaadie Team</p>".format(account.name)

        transport.MailService.send_mail('Premium Features enabled', 'hello@kaadie.com', 'Kaadie', account.email,
                                        premium_message, account.name)

        messages.success(request, 'You are now a premium Kaadie user')

        if sub is not None:
            return redirect('home', sub=sub)
        else:
            return redirect('home')


class ReportView(services_mixin.MainMixin, View):
    template_name = 'report.html'

    def get(self, request, *args, **kwargs):
        """
        report page
        :return:
        """
        user = request.user

        return render(request, self.template_name, locals())


class ControlPanel(mixins.LoginRequiredMixin, services_mixin.AdminPanelMixin, services_mixin.MainMixin, View):
    template_name = 'new_control_panel.html'

    def get(self, request, *args, **kwargs):
        """
        Control Panel
        :param request:
        :return:
        """
        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)

        if organization is None:
            raise Http404

        date_created = datetime.strptime(organization.date_created, '%Y-%m-%dT%H:%M:%S.%fZ')

        # organization = helper.get_organization(request)
        user = request.user
        account = user.account
        wallet = Wallet.objects.filter(account=account.pk).first()
        if not wallet:
            wallet = Wallet.objects.create(account=account)

        storage = messages.get_messages(request)
        form = forms.UpdateOrganizationForm()

        premium = False
        if request.user.is_authenticated():
            premium = request.user.account.is_premium

        try:
            sections = cache.fetch_cache_sections(organization.code)
        except Exception, e:
            sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
                'position').all()
            is_db = True

        return render(request, self.template_name, locals())


class Notifications(mixins.LoginRequiredMixin, services_mixin.MainMixin, View):
    template_name = 'notifications.html'

    def get(self, request, *args, **kwargs):
        """
        Control Panel
        :param request:
        :return:
        """
        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)

        user = request.user
        notifications = user.account.notifications.filter(is_read=False).order_by('-date_created').all()

        premium = False
        if request.user.is_authenticated():
            premium = request.user.account.is_premium

        try:
            sections = cache.fetch_cache_sections(organization.code)
        except Exception, e:
            sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
                'position').all()
            is_db = True

        return render(request, self.template_name, locals())


@services_mixin.main_mixin
def privacy_policy(request, *args, **kwargs):
    """
    Privacy Policy
    :param request:
    :return:
    """

    template_name = 'privacy.html'
    sub = kwargs.get('sub')
    organization = helper.get_organization(sub)
    user = request.user

    premium = False
    if request.user.is_authenticated():
        premium = request.user.account.is_premium

    try:
        sections = cache.fetch_cache_sections(organization.code)
    except Exception, e:
        sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
            'position').all()
        is_db = True

    return render(request, template_name, locals())


@services_mixin.main_mixin
def terms_of_use(request, *args, **kwargs):
    """
    Terms of Use
    :param request:
    :return:
    """

    template_name = 'terms.html'
    sub = kwargs.get('sub')
    organization = helper.get_organization(sub)

    user = request.user

    premium = False
    if request.user.is_authenticated():
        premium = request.user.account.is_premium

    try:
        sections = cache.fetch_cache_sections(organization.code)
    except Exception, e:
        sections = Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
            'position').all()
        is_db = True

    return render(request, template_name, locals())


def view_artists(request, *args, **kwargs):
    """
    view artists
    """
    page = request.GET.get('page', 1)
    size = request.GET.get('size', 20)
    artists = Artist.objects
    # _banners = redis_obj.hgetall('banner')
    banners = ArtistBanner.objects.all()

    random_cards = list()

    if artists.count() == 0:
        random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                 body={'query': {
                                     'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                         'random_score': {'seed': random.randint(1, 1000000)}
                                     }]}}, 'filter': {
                                     'bool': {'must': [{'term': {'is_public': True}}]}
                                 }, 'sort': [{"_score": {"order": "desc"}}]}, size=8)

        random_hits = random_query['hits']

        if random_hits['total'] == 0:
            random_cards = Card.objects.filter(is_active=True, is_public=True).order_by('?')[
                           :8].all()

        else:
            _random = random_hits['hits']
            random_cards = [helper.Payload(i['_source']) for i in _random]

    return render(request, 'artists.html', locals())


@services_mixin.main_mixin
def view_artist(request, nickname):
    """
    view artists
    """
    person = Artist.objects.filter(nickname=nickname).first()

    if not person:
        raise Http404

    random_cards = list()

    _banners = redis_obj.hgetall('banner')
    banners = [json.loads(c) for c in _banners.values()] if len(_banners.values()) > 0 else Banner.objects.all()
    cards = Card.objects.filter(user_id=person.account.pk, is_active=True)

    if cards.count() == 0:
        random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                 body={'query': {
                                     'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                         'random_score': {'seed': random.randint(1, 1000000)}
                                     }]}}, 'filter': {
                                     'bool': {'must': [{'term': {'is_public': True}}]}
                                 }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)

        random_hits = random_query['hits']

        if random_hits['total'] > 0:
            _random = random_hits['hits']
            random_cards = [helper.Payload(i['_source']) for i in _random]
        else:
            random_cards = Card.objects.filter(is_active=True, is_public=True).order_by('?')[
                           :6].all()

    return render(request, 'artist.html', locals())


class OrganizationView(mixins.LoginRequiredMixin, services_mixin.PremiumMixin, View):
    def post(self, request, *args, **kwargs):
        """
        update basic organization information
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)

        # organization = helper.get_organization(request)
        user = request.user.account

        if user.pk != organization.account_id:
            messages.error(request, "You are not authorized to perform this request")

            if sub is not None:
                return redirect('home', sub=sub)
            else:
                return redirect('home')

        data = request.POST.copy()
        data['code'] = organization.code
        form = forms.UpdateOrganizationForm(data)

        if form.is_valid():
            _data = {
                'color': form.data.get('color'),
                'name': form.data.get('name'),
                'primary_color': form.data.get('primary_color'),
                'primary_color_class': form.data.get('primary_color_class'),
                'primary_background_color': form.data.get('primary_background_color'),
                'menu_color': form.data.get('menu_color'),
                'semantic_class': form.data.get('semantic_class'),
                'logo': form.data.get('logo')
            }
            organization = helper.set_attributes(organization, **_data)

            organization.save()
            messages.success(request, "Your brand information has been updated")
        else:
            messages.error(request, form.errors)

        if sub is not None:
            return redirect('control_panel', sub=sub)
        else:
            return redirect('control_panel')


def post_comment(request, id, *args, **kwargs):
    """
    post comment
    :param request:
    :return:
    """
    sub = kwargs.get('sub')
    received_card = ReceivedCard.objects.filter(notification_id=id).first()

    shared = SharedCard.objects.filter(notification_id=id).first()

    if not received_card and not shared:
        if sub is not None:
            return redirect('index', sub=sub)
        else:
            return redirect('index')

    comment = request.POST.get('comment')

    if not comment:
        messages.error(request, "Comment Missing")
        if sub is not None:
            if shared:
                return redirect('shared_card', id=id, sub=sub)
            return redirect('sent_card', id=received_card.notification_id, sub=sub)
        else:
            if shared:
                return redirect('shared_card', id=id)
            return redirect('sent_card', id=received_card.notification_id)

    user = request.user
    name = 'Anonymous'
    address = None

    if user.is_authenticated():
        name = user.account.name
        address = user.account.email

    if shared:
        comment = Comments.objects.create(poster=name, poster_address=address, comment=comment, shared_card=shared)
        email = shared.user.email
        alert_link = request.build_absolute_uri(reverse('shared_card', args=(id,)))
        alert_recipient = shared.user
        subject = '%s just commented on the card you shared on %s' % (name, shared.platform)
        alert_message = '<p>Hi %s,</p>' \
                        '<p> %s just commented on the card you shared on %s.</p>' \
                        '<p>Please click <a href="%s"> here</a> to view the comment.</p>' \
                        '<p>Best Regards,<br />Kaadie</p>' \
                        % (alert_recipient.name, name, shared.platform, alert_link)

        if user.is_authenticated():
            if shared.user.email != user.email:
                transport.send_feedback.delay(subject, email, alert_message, alert_link, alert_recipient.pk)
        else:
            transport.send_feedback.delay(subject, email, alert_message, alert_link, alert_recipient.pk)

            # helper.trigger_notification.delay('%s just commented on the card you shared on %s' % (name, shared.platform),
            #                                   request.build_absolute_uri(reverse('shared_card', args=(id,))),
            #                                   [shared.user.pk])

    else:
        comment = Comments.objects.create(poster=name, poster_address=address, received_card=received_card,
                                          comment=comment, sent_card=received_card.sent_card)
        alert_link = request.build_absolute_uri(reverse('sent_card', args=(id,)))
        alert_recipient = received_card.user
        email = received_card.user.email
        body = '<p>Hi %s,</p>' \
               '<p> %s just commented on the card you sent.</p>' \
               '<p>Please click <a href="%s"> here</a> to view the comment.</p>' \
               '<p>Best Regards,<br />Kaadie</p>' \
               % (alert_recipient.name, name, alert_link)

        subject = '%s just commented on the card you sent.' % name

        if user.is_authenticated():
            if received_card.user.email != user.email:
                transport.send_feedback.delay(subject, email, body, alert_link, alert_recipient.pk)
        else:
            transport.send_feedback.delay(subject, email, body, alert_link, alert_recipient.pk)

            # helper.trigger_notification.delay('%s just commented on the card you sent' % name,
            #                                   request.build_absolute_uri(reverse('sent_card', args=(id,))),
            #                                   [received_card.user.pk])

    try:
        helper.record_log(request, 'posted a comment for %s' % id, serializers.CommentSerializer(comment).data,
                          'object')
    except Exception, e:
        print('==============')
        print(e)
        print('==============')

    if sub is not None:
        if shared:
            return redirect('shared_card', id=id, sub=sub)
        return redirect('sent_card', sub=sub, id=received_card.notification_id)
    else:
        if shared:
            return redirect('shared_card', id=id)
        return redirect('sent_card', id=received_card.notification_id)


def schedule_reminder(request, pk, *args, **kwargs):
    """
    schedule birthday reminder for contact
    :param request:
    :return:
    """
    try:
        contact = Contact.objects.get(pk=pk)
        sub = kwargs.get('sub')
        domain = helper.get_domain(sub)
        # domain = helper.get_domain(request)
        helper.set_birthday_reminder(contact.pk, domain)

        messages.success(request, "A Birthday Card alert has been set for %s" % contact.name)
        if sub is not None:
            return redirect('home', sub=sub)
        else:
            return redirect('home')
    except:
        messages.error(request, "The contact you requested was not found!!!")
        return redirect('home')


@login_required
def create_contact(request, *args, **kwargs):
    """
    Create contacts
    :param request:
    :return:
    """

    user = models.User.objects.get(pk=request.user.pk)
    sub = kwargs.get('sub')
    ua = get_user_agent(request)

    first_name = request.POST.get('first_name', '')
    last_name = request.POST.get('last_name', '')
    email = request.POST.get('email')
    phone = request.POST.get('phone', '')
    city = request.POST.get('city', '')
    country = request.POST.get('country', '')
    address = request.POST.get('address', '')
    birthday = request.POST.get('birthday', '')

    email_contacts = [c.email for c in user.account.contacts.all()]
    phone_contacts = [c.phone for c in user.account.contacts.all()]

    if email not in email_contacts or (phone not in phone_contacts and len(phone) != 0):
        address = Address.objects.create(first_name=first_name, last_name=last_name, city=city,
                                         country=country, phone=phone, address=address,
                                         full_name='%s %s' % (first_name, last_name))
        if birthday:
            address.date_of_birth = datetime.strptime(birthday, '%Y-%m-%d').date()
            address.save()

        contact = Contact.objects.create(address=address, email=email, user=user.account)

        if contact.address.date_of_birth is not None:
            domain = helper.get_domain(sub)
            helper.set_birthday_reminder(contact.id, domain=domain)

        messages.success(request, 'Contact successfully added')
        if sub is not None:
            return redirect('home', sub=sub)
        else:
            return redirect('home')
    else:
        messages.error(request, 'Contact already exists')
        if sub is not None:
            return redirect('home', sub=sub)
        else:
            return redirect('home')


@login_required
def upload_contact(request, *args, **kwargs):
    """
    Upload contacts
    :param request:
    :return:
    """

    sub = kwargs.get('sub')
    ua = get_user_agent(request)

    if request.method == 'POST':
        form = forms.UploadContactForm(request.POST, request.FILES)
        if form.is_valid():
            _file = request.FILES['file']
            fs = FileSystemStorage()
            filename = fs.save('uploads/%s_%s' % (time.time(), _file.name), _file)
            path = os.path.join(settings.MEDIA_ROOT, filename)

            if not helper.validate_file_uploaded(path):
                messages.error(request, 'Invalid File Format')
                if sub is not None:
                    return redirect('home', sub=sub)
                else:
                    return redirect('home')

            try:
                wb = load_workbook(path)
                ws = wb.active
                for row in ws.iter_rows(min_row=2, min_col=1):
                    data = [cell.value for cell in row]
                    first_name = data[0]
                    last_name = data[1]
                    email = data[2]

                    full_name = '%s %s' % (first_name, last_name)
                    if not email or not full_name:
                        continue

                    email_contacts = filter(lambda x: x is not None,
                                            [c.email for c in request.user.account.contacts.all()])
                    phone_contacts = filter(lambda x: x is not None,
                                            [c.phone for c in request.user.account.contacts.all()])
                    phone = data[3]

                    if email in email_contacts or phone in phone_contacts:
                        continue

                    address = data[5]
                    city = data[6]
                    country = data[7]

                    address = Address.objects.create(first_name=first_name, last_name=last_name, phone=phone,
                                                     address=address, city=city, country=country)

                    birthday = data[4]
                    if birthday:
                        if isinstance(birthday, datetime):
                            address.date_of_birth = birthday.date()
                        else:
                            address.date_of_birth = datetime.strptime(birthday, '%Y-%m-%d').date()
                        address.save()

                    contact = Contact.objects.create(address=address, user=request.user.account, email=email)

                    if contact.address.date_of_birth:
                        domain = helper.get_domain(sub)
                        helper.set_birthday_reminder(contact.id, domain=domain)

                messages.success(request, 'Contact successfully uploaded')
                if sub is not None:
                    return redirect('home', sub=sub)
                else:
                    if sub is not None:
                        return redirect('home', sub=sub)
                    else:
                        return redirect('home')
            except:
                messages.error(request, 'Invalid File Format')
                if sub is not None:
                    return redirect('home', sub=sub)
                else:
                    return redirect('home')

        messages.error(request, 'Invalid File Format')
    if sub is not None:
        return redirect('home', sub=sub)
    else:
        return redirect('home')


@login_required
def update_account(request, *args, **kwargs):
    """
    Update account
    :param request:
    :return:
    """
    account = Account.objects.get(pk=request.user.pk)

    data = dict(first_name=request.POST.get('first_name', account.address.first_name),
                last_name=request.POST.get('last_name', account.address.last_name),
                phone=request.POST.get('phone', account.address.phone),
                city=request.POST.get('city', account.address.city),
                country=request.POST.get('country', account.address.country),
                address=request.POST.get('address', account.address.address)
                )

    birthday = request.POST.get('birthday', '')

    if birthday:
        # address.date_of_birth = datetime.strptime(birthday, '%d/%m/%Y').date()
        data['date_of_birth'] = datetime.strptime(birthday, '%Y-%m-%d')

    address = helper.set_attributes(account.address, **data)

    account.address = address
    account.address.full_name = '%s %s' % (account.address.first_name, account.address.last_name)
    account.address.save()

    account.user.first_name = data['first_name']
    account.user.last_name = data['last_name']
    account.user.save()

    sub = kwargs.get('sub')

    messages.success(request, 'Profile successfully updated')
    if sub is not None:
        return redirect('home', sub=sub)
    else:
        return redirect('home')


def check_card(request, *args, **kwargs):
    """
    ajax request to check if a card exists
    :param self:
    :param request:
    :param args:
    :param kwargs:
    :return:
    """
    try:
        q = request.GET['q']
        return services_encoders.JSONResponse({"response": Card.objects.filter(slug=slugify(q)).exists()})
    except:
        raise


def loader_io(request, *args, **kwargs):
    """
    loader io confirmation
    :param request:
    :param args:
    :param kwargs:
    :return:
    """
    return render(request, 'b3e4ccf5651467dea36644602ebb76c4.txt')


def email_template(request):
    """
    Email Default page
    :param request:
    :return:
    """

    card = Card.objects.get(pk=470)
    image_url = card.image_url
    custom_message = SystemMessage.objects.filter(tag='general').order_by('?').first()

    recipient = 'Samantha'
    sender = 'Frederick'
    link = 'http://google.com.ng'
    domain = "http://kaadie.com"
    thumbnail_url = card.thumbnail.image_url or settings.DEFAULT_THUMBNAIL

    message = custom_message.html_text % sender
    return render(request, 'emails/default.html', locals())


@login_required
def welcome_template(request):
    """
    Email Welcome Default page
    :param request:
    :return:
    """
    account = request.user.account

    cards = list(Card.objects.filter(is_active=True, is_public=True, is_featured=True).order_by('?')[:6].all())
    top_cards = cards[:3]
    down_cards = cards[3:]

    # message = custom_message.html_text % sender
    return render(request, 'emails/welcome.html', locals())


@login_required
def sms_template(request):
    """
    Email Default page
    :param request:
    :return:
    """
    card = Card.objects.first()
    image_url = card.image_url
    custom_message = SystemMessage.objects.filter(tag='general').order_by('?').first()

    message = custom_message.sms_text

    recipient = 'Samantha'
    sender = 'Frederick'
    link = 'http://google.com.ng'

    return render(request, 'sms/default.txt', locals())


def css_template(request):
    """
    Css Default page
    :param request:
    :return:
    """
    organization = Organization.objects.first()

    return render(request, 'css/default.txt', locals())


@login_required
def update_profile_image(request, *args, **kwargs):
    """
    Update user image url
    :return:
    """
    user = request.user
    sub = kwargs.get('sub')

    form = forms.UpdateProfileImage(request.POST, request.FILES)

    if form.is_valid():
        _file = request.FILES['image']
        fs = FileSystemStorage()
        name = hashlib.md5('%s_%s' % (time.time(), _file.name)).hexdigest()
        filename = fs.save('uploads/%s' % name, _file)
        file_path = os.path.join(settings.MEDIA_ROOT, filename)

        opened_file = open(file_path, 'rb')
        b64 = opened_file.read()
        uploaded = helper.upload_b64_s3(b64encode(b64), name)

        if uploaded:
            account = user.account
            account.image_url = '%s/%s' % (settings.S3_BASE_ADDRESS, name)
            account.save()
            messages.success(request, "Profile Image successfully updated")

            if sub is not None:
                return redirect('home', sub=sub)
            else:
                return redirect('home')

        messages.error(request, "Invalid Image Format")

        if sub is not None:
            return redirect('home', sub=sub)
        else:
            return redirect('home')

    else:
        messages.error(request, "Invalid Image Format")
        if sub is not None:
            return redirect('home', sub=sub)
        else:
            return redirect('home')


def build_index(request, *args, **kwargs):
    """
    ajax request to build homepage data
    :param request:
    :return:
    """
    if not request.is_ajax():
        raise Http404

    sub = kwargs.get('sub')
    organization = helper.get_organization(sub)
    is_search, cats_query = cache.fetch_categories(organization.code, size=3)

    categories = cats_query
    if not is_search:
        categories = [serializers.CategorySerializer(c).data for c in filter(lambda x: x.cards.count() > 4, categories)]

    return services_encoders.JSONResponse({'categories': categories})


def load_cards(request, *args, **kwargs):
    """
    ajax request to build homepage data
    :param request:
    :return:
    """
    if not request.is_ajax():
        raise Http404

    cards = [serializers.CardSerializer(c).data for c in Card.objects.filter(is_public=True,
                                                                             is_active=True).order_by('?')[:4].all()]

    return services_encoders.JSONResponse({'cards': cards})


@login_required
def ajax_notifications(request, *args, **kwargs):
    """
    ajax get request for notifications
    :param request:
    :param args:
    :param kwargs:
    :return:
    """
    if not request.is_ajax():
        raise Http404

    if not hasattr(request.user, 'account'):
        return {'messages': []}

    return services_encoders.JSONResponse(
        {'messages': [serializers.NotificationSerializer(c).data for c in modelNotification.objects.filter(
            user=request.user.account, is_read=False).all()]})


@login_required
def mark_as_read(request, pk, *args, **kwargs):
    """
    ajax get request for notifications
    :param request:
    :param args:
    :param kwargs:
    :return:
    """
    if not request.is_ajax():
        raise Http404

    notification = modelNotification.objects.get(pk=pk)
    notification.is_read = True
    notification.save()

    return services_encoders.JSONResponse(
        {'messages': [serializers.NotificationSerializer(c).data for c in modelNotification.objects.filter(
            user=request.user.account, is_read=False).all()]})


@login_required
def artist_request(request, *args, **kwargs):
    """
    request artist permission
    :param request:
    :param args:
    :param kwargs:
    :return:
    """
    if not request.is_ajax():
        raise Http404

    form = forms.ArtistRequestForm(request.POST)

    if form.is_valid():
        email = request.user.email
        name = request.user.account.name

        resp = transport.request_artist_permission(email, form.cleaned_data['portfolio'], name,
                                                   form.cleaned_data['nickname'])
        if resp:
            return services_encoders.JSONResponse({"status": 'success', 'errors': None})

    return services_encoders.JSONResponse({"status": 'error', 'errors': form.errors['portfolio']})


@login_required
def update_logo(request, *args, **kwargs):
    """
    Update user image url
    :return:
    """
    user = request.user
    sub = kwargs.get('sub')
    slug = sub if sub else 'kaadie'
    organization = Organization.objects.filter(code=slug.lower()).first()

    if organization is None:
        raise Http404

    form = forms.UpdateProfileImage(request.POST, request.FILES)

    if form.is_valid():
        _file = request.FILES['image']
        fs = FileSystemStorage()
        name = '%s_%s' % (time.time(), _file.name)
        filename = fs.save('uploads/%s' % name, _file)
        file_path = os.path.join(settings.MEDIA_ROOT, filename)
        opened_file = open(file_path, 'rb')
        b64 = opened_file.read()
        uploaded, _image = helper.upload_b64_s3(b64encode(b64), name)
        logo_url = '%s/%s' % (settings.S3_BASE_ADDRESS, name)

        if uploaded:
            organization.logo = logo_url
            organization.save()
            messages.success(request, "Organization Logo successfully updated")

            if sub is not None:
                return redirect('control_panel', sub=sub)
            else:
                return redirect('control_panel')

        messages.error(request, "Invalid Image Format")

        if sub is not None:
            return redirect('control_panel', sub=sub)
        else:
            return redirect('control_panel')

    else:
        messages.error(request, "Invalid Image Format")
        if sub is not None:
            return redirect('control_panel', sub=sub)
        else:
            return redirect('control_panel')


def save_designs(request, pk):
    """
    save card design
    """
    try:
        account = request.user.account
        card = Card.objects.get(pk=pk)

        card_design = request.POST.get('card_image')
        cover_design = request.POST.get('cover_image')

        saved_card = SavedCard.objects.filter(card=card.pk, user=account.pk).first()
        if not saved_card:
            saved_card = SavedCard.objects.create(**{
                'card_id': card.pk,
                'user_id': account.pk
            })

        card_image_url = None
        cover_image_url = card.image_url

        # upload card image base64 image
        _id = hashlib.md5('{}_{}_{}'.format(card.title, account.pk, time.time())).hexdigest()
        title = "custom-card-%s.jpg" % _id
        card_image_uploaded = helper.upload_b64_s3(card_design, title)
        if card_image_uploaded:
            card_image_url = '%s/%s' % (settings.S3_BASE_ADDRESS, title)

        # upload cover image base64 image
        cover_id = hashlib.md5('{}_{}_{}'.format(card.title, account.pk, time.time())).hexdigest()
        cover_title = "custom-cover-%s.jpg" % cover_id
        cover_image_uploaded = helper.upload_b64_s3(cover_design, cover_title)
        if cover_image_uploaded:
            cover_image_url = '%s/%s' % (settings.S3_BASE_ADDRESS, cover_title)

        SavedCardImage.objects.create(saved_card_id=saved_card.pk, cover_image_url=cover_image_url,
                                      card_image_url=card_image_url)
        return services_encoders.JSONResponse({'message': "Card Successfully saved", 'status': 'success'})
    except Exception, e:
        print(e)
        return services_encoders.JSONResponse({'message': "Card not saved", 'status': "error"})