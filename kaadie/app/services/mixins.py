from django.contrib.auth.mixins import AccessMixin
from django.shortcuts import redirect
from django.conf import settings
from django.http import Http404

from rest_framework.authentication import SessionAuthentication

from . import helper


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return


class AnonymousMixin(AccessMixin):
    """
    Custom mixin to prevent access to routes by an authenticated user
    """

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(settings.HOME_URL)
        return super(AnonymousMixin, self).dispatch(request, *args, **kwargs)


class AdminPanelMixin(AccessMixin):
    """
    Custom mixin to prevent access to routes by an authenticated user
    """

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            # organization = get_organization(request)
            sub = kwargs.get('sub')
            organization = helper.get_organization(sub)
            if organization.account_id != request.user.account.pk:
                return redirect(settings.HOME_URL)
        return super(AdminPanelMixin, self).dispatch(request, *args, **kwargs)


class PremiumMixin(AccessMixin):
    """
    Custom mixin to prevent access to routes authorized only for artists
    """

    def dispatch(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            return redirect(settings.LOGIN_URL)

        if not request.user.is_staff and not request.user.account.is_premium:
            return redirect(settings.HOME_URL)

        return super(PremiumMixin, self).dispatch(request, *args, **kwargs)


class ValidateEmailMixin(AccessMixin):
    """
    Custom mixin to force user to change email
    """

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and hasattr(request.user, 'account'):
            if not request.user.account.email_valid:
                return redirect(settings.VALIDATE_EMAIL_URL)
        return super(ValidateEmailMixin, self).dispatch(request, *args, **kwargs)


class ArtistMixin(AccessMixin):
    """
    Custom mixin restricting non artist access to certain pages
    """
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and hasattr(request.user, 'account'):
            if hasattr(request.user.account, 'artist'):
                return super(ArtistMixin, self).dispatch(request, *args, **kwargs)
        return redirect(settings.HOME_URL)


class NonArtistMixin(AccessMixin):
    """
    Custom mixin to restrict artist access to these pages
    """
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and hasattr(request.user, 'account'):
            if not hasattr(request.user.account, 'artist'):
                return super(NonArtistMixin, self).dispatch(request, *args, **kwargs)
        return redirect(settings.HOME_URL)


def anonymous_user(func, redirect_url=settings.HOME_URL):
    """
    redirects logged_in user
    :param func:
    :param redirect_url:
    :return:
    """

    def decorated(request, *args, **kwargs):
        """
        redirects authenticated user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        if not request.user.is_authenticated:
            return func(request)
        return redirect(redirect_url)

    return decorated


def is_premium(func, redirect_url=settings.HOME_URL, login_url=settings.LOGIN_URL):
    """
    redirects regular user from accessing artist pages
    :param func:
    :param redirect_url:
    :return:
    """

    def decorated(request, *args, **kwargs):
        """
        redirects non-artist user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        if not request.user.is_authenticated:
            return redirect(login_url)

        if request.user.account.is_premium or request.user.is_staff:
            return func(request, *args, **kwargs)

        return redirect(redirect_url)

    return decorated


def validate_email(func, redirect_url=settings.VALIDATE_EMAIL_URL):
    """
    forces user to verify email
    :param func:
    :param redirect_url:
    :return:
    """

    def decorated(request, *args, **kwargs):
        """
        redirects user with unverified email account
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        # check if user is logged in
        if not request.is_authenticated():
            return redirect(settings.LOGIN_URL)

        if request.user.account.email_valid:
            return func(request, *args, **kwargs)
        return redirect(redirect_url)

    return decorated


class MainMixin(AccessMixin):
    """
    Middleware to gather common variables
    """
    def dispatch(self, request, *args, **kwargs):
        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)

        if not organization:
            raise Http404

        try:
            helper.record_log(request, 'visited', request.path, 'page')
        except Exception, e:
            print('==============')
            print(e)
            print('==============')

        return super(MainMixin, self).dispatch(request, *args, **kwargs)


def main_mixin(view_method):
    """
    raise 404 redirect if organization not found
    """
    def decorated(request, *args, **kwargs):
        sub = kwargs.get('sub')
        organization = helper.get_organization(sub)

        if not organization:
            raise Http404

        try:
            helper.record_log(request, 'visited', request.path, 'page')
        except Exception, e:
            print('==============')
            print(e)
            print('==============')

        return view_method(request, *args, **kwargs)

    return decorated