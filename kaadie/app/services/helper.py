from __future__ import absolute_import, unicode_literals
import base64, urlparse
from datetime import datetime, time as dt_time
import json, string
import logging, re, requests
from mimetypes import guess_type
import csv, codecs
import random
import io
import os

from PIL import Image
from urllib2 import urlopen

from django.conf import settings
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django_user_agents.utils import get_user_agent

from django_celery_beat.models import CrontabSchedule, PeriodicTask
from boto3 import client
from pyshorteners import Shortener
from xlrd import open_workbook, xldate_as_tuple
from premailer import transform
from pymongo import MongoClient
# from celery import shared_task
from kaadie import celery_app, es, mongo_client, pusher_client, redis_obj
from app import serializers as app_serializers, models
from . import cache

log = logging.getLogger('app')


# def get_domain(request):
#     """
#     set domain address based on cuurent location
#     """
#
#     domain = 'http://%s' % settings.SCHEME
#     host = request.get_host()
#     addresses = host.split('.')
#     if Organization.objects.filter(code=addresses[0]).exists() and len(addresses) > 2:
#         domain = 'http://%s.%s' % (addresses[0], settings.SCHEME)
#
#     return domain


# def get_organization(request):
#     """
#     returns an organization
#     :param request:
#     :return:
#     """
#     host = request.get_host()
#     addresses = host.split('.')
#     code = addresses[0]
#
#     if not Organization.objects.filter(code=addresses[0]).exists():
#         code = 'kaadie'
#
#     organization = Organization.objects.filter(code=code).first()
#
#     return organization

# def fetch_sections(code):
#     """
#     fetch sections matching this organization code
#     :param code: organization code
#     :return:
#     """
#     section_query = es.search(index=settings.ES_INDEX, doc_type='section',
#                               body={'query': {'terms': {
#                                   'permissible_organizations': [code]
#                               }}})
#     section_hits = section_query['hits']
#
#     if section_hits['total'] == 0:
#
#         organization = Organization.objects.filter(code=code).first()
#         if not organization:
#             return False, []
#
#         return True, Section.objects.filter(permissible_organizations__in=[organization]).order_by('position').all()
#
#     _sections = section_hits['hits']
#     return True, [Payload(c['_source']) for c in _sections]


# def fetch_banners():
#     """
#     fetch all banners
#     :return:
#     """
#     banner_query = es.search(index=settings.ES_INDEX, doc_type='banner', body={'query': {'match_all': {}}})
#     banner_hits = banner_query['hits']
#
#     if banner_hits['total'] == 0:
#         return Banner.objects.all()
#
#     _banners = banner_hits['hits']
#     return [Payload(c['_source']) for c in _banners]

def get_domain(slug=None):
    """
    set domain address based on current location
    """

    domain = 'http://%s' % settings.SCHEME
    # host = request.get_host()
    # addresses = host.split('.')

    if slug is None:
        return domain

    if models.Organization.objects.filter(code=slug.lower()).exists():
        domain = 'http://%s/%s' % (settings.SCHEME, slug.lower())

    return domain


def get_organization(slug=None):
    """
    returns an organization
    :param slug:
    :return:
    """

    # code = slug.lower()
    # if not Organization.objects.filter(code=code).exists():
    #     code = 'kaadie'

    if slug is None:
        slug = 'kaadie'

    try:
        organ = redis_obj.hget('organization', slug)
        if organ:
            return Payload(json.loads(organ))
        organization = models.Organization.objects.filter(code=slug.lower()).first()
    except Exception, e:
        organization = models.Organization.objects.filter(code=slug.lower()).first()

    return organization


def upload_b64_s3(base64_image, filename):
    """
    Upload base64 encoded image to s3
    :return:
    """
    s3 = client('s3', aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
                aws_access_key_id=settings.AWS_ACCESS_KEY_ID)

    try:
        _file = re.sub("data:image/jpeg;base64,", '', base64_image)
        resp = s3.put_object(ACL='public-read-write', Bucket=settings.AWS_STORAGE_BUCKET_NAME,
                             Body=_file.decode('base64'), Key=filename)
        return True, resp
    except:
        return False, {}


def get_as_base64(url):
    """
    base64 encodes an image url
    :param url:
    :return:
    """
    try:
        return base64.b64encode(requests.get(url).content)
    except:
        raise


def shorten_url(url):
    """
    Shorten url using Google URL shortener API
    :param url:
    :return:
    """
    try:
        shorten = Shortener('Google', api_key=settings.GOOGLE_API_KEY)
        return shorten.short(url)
    except:
        return url


def build_html_message(sender, domain, link, image_url, thumbnail_url, tag='general', recipient=''):
    """
    build sms message based on recipient information and message tag
    :return:
    """
    custom_message = models.SystemMessage.objects.filter(tag=tag).order_by('?').first()
    message = custom_message.html_text % sender
    html = render_to_string('emails/default.html', locals())
    return transform(''' %s ''' % html)

    # return custom_message.html_text % (recipient, sender, domain, link)


def build_sms_message(sender, domain, link, tag='general', recipient=''):
    """
    build sms message based on recipient information and message tag
    :return:
    """
    custom_message = models.SystemMessage.objects.filter(tag=tag).order_by('?').first()
    message = custom_message.html_text % sender
    return render_to_string('sms/default.txt', locals())
    # return custom_message.sms_text % (recipient, sender, domain, link)


def create_contacts_from_spreadsheet(user_id, file_path, domain='kaadie.com'):
    """
    create contacts from a csv file
    :param user_id
    :param file_path
    :param domain
    :return:
    """
    user = User.objects.get(pk=user_id)
    phones = []
    emails = []
    email_contacts = filter(lambda x: x is not None,
                            [c.email for c in user.account.contacts.all()])
    phone_contacts = filter(lambda x: x is not None,
                            [c.phone for c in user.account.contacts.all()])

    mime = guess_type(file_path)
    _type = mime[0]

    # try:
    if _type in ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel']:
        _file = open_workbook(file_path)
        for sheet_num in range(_file.nsheets):
            sheet = _file.sheet_by_index(sheet_num)
            for row_num in range(1, sheet.nrows):
                data = sheet.row_values(row_num)

                first_name = data[0]
                last_name = data[1]
                email = data[2]
                phone = data[3]

                if first_name is None or first_name == '':
                    continue

                if (phone is None or phone == '') and (email is None or email == ''):
                    continue

                emails.append(email)
                phones.append(phone)

                if email not in email_contacts or phone not in phone_contacts:
                    address = data[5]
                    city = data[6]
                    country = data[7]

                    full_name = '%s %s' % (first_name, last_name) if last_name is not None else first_name

                    address = models.Address.objects.create(first_name=first_name, last_name=last_name, phone=phone,
                                                            address=address, city=city, country=country,
                                                            full_name=full_name)

                    birthday = data[4]
                    if birthday:
                        dob = None
                        if isinstance(birthday, str):
                            dob = datetime.strptime(birthday, '%Y-%m-%d').date()
                        elif isinstance(birthday, float):
                            dob = datetime(*xldate_as_tuple(data[4], _file.datemode)).date()
                        address.date_of_birth = dob
                        address.save()

                    con = models.Contact.objects.create(address=address, user=user.account, email=email)
                    if con.address.date_of_birth is not None:
                        set_birthday_reminder(con.pk, domain)

    else:
        _file = csv.reader(codecs.open(file_path, 'rU', 'utf-16'))
        for data in _file:
            first_name = data[0]
            last_name = data[1]
            email = data[2]
            phone = data[3]

            if not first_name and (not phone or not email):
                continue

            if email is not None or email != '':
                emails.append(email)

            if phone is not None or phone != '':
                phones.append(phone)

            if (email not in email_contacts) or (phone not in phone_contacts):
                address = data[5]
                city = data[6]
                country = data[7]

                full_name = '%s %s' % (first_name, last_name) if last_name is not None else first_name

                address = models.Address.objects.create(first_name=first_name, last_name=last_name, phone=phone,
                                                        address=address, city=city, country=country,
                                                        full_name=full_name)

                birthday = data[4]
                if birthday:
                    dob = None
                    if isinstance(birthday, str):
                        dob = datetime.strptime(birthday, '%Y-%m-%d').date()
                    elif isinstance(birthday, float):
                        dob = datetime(*xldate_as_tuple(data[4], _file.datemode)).date()
                    address.date_of_birth = dob
                    address.save()

                con = models.Contact.objects.create(address=address, user=user.account, email=email)

                if con.address.date_of_birth is not None:
                    set_birthday_reminder(con.pk, domain)

    return True, phones, emails
    # except Exception, e:
    #     raise e


def set_attributes(_object, **kwargs):
    """

    :param _object:
    :param kwargs:
    :return:
    """
    for key, value in kwargs.items():
        if hasattr(_object, key):
            setattr(_object, key, value)

    return _object


def validate_file_uploaded(file_path):
    """
    checks if uploaded file is a permitted file
    :param file_path:
    :return:
    """
    try:
        _mime = guess_type(file_path)
        return _mime[0] in settings.ACCEPTED_MIME_TYPES
    except:
        return False


def record_log(request, verb, _object, object_type):
    """
    record logs
    :param request:
    :param verb:
    :param _object:
    :param object_type:
    :return:
    """
    ua = get_user_agent(request)

    device_type = 'pc'

    if ua.is_mobile:
        device_type = 'mobile'

    if ua.is_tablet:
        device_type = 'tablet'

    username = 'anonymous' if not request.user.is_authenticated() else request.user.email
    name = 'Anonymous' if not request.user.is_authenticated() else request.user.account.name

    record_activity_log.delay(username, name, verb, _object, object_type,
                              datetime.now(), ua.browser.family, ua.device.family, device_type, ua.os.family)

    return True


@celery_app.task
def record_activity_log(identifier, actor, verb, _object, object_type, timestamp, browser, device, device_type, os):
    """
    records activity log
    :param identifier:
    :param actor:
    :param verb:
    :param _object:
    :param object_type:
    :param timestamp
    :param browser:
    :param device:
    :param device_type
    :param os:
    :return:
    """
    try:
        db = mongo_client[settings.MONGOD_DATABASE]

        activities = db.activities
        record = {
            'id': identifier,
            'actor': actor,
            'verb': verb,
            'object': _object,
            'object_type': object_type,
            'browser': browser,
            'device': device,
            'device_type': device_type,
            'os': os,
            'timestamp': timestamp
        }
        activities.insert_one(record)
        return True
    except:
        log.error('[ACTOR: %s][VERB: %s][ACTION: %s][STATUS: FAILED]' % (actor, verb, _object))
        return False


def rewrite_css_file(organization_id):
    """
    rewrite css file for that organization
    :param organization_id:
    :return:
    """
    try:
        import os
        organization = models.Organization.objects.get(pk=organization_id)
        path = os.path.join(settings.BASE_DIR, 'staticfiles/css/brands/%s.css' % organization.code)

        _file = open(path, 'wb')
        css_data = render_to_string('css/default.txt', locals())
        _file.write(css_data)
        _file.close()

        return True
    except Exception, e:
        log.error('[FAILED TO REWRITE CSS FILE BECAUSE %s] [OBJECT: %s]' % (e, organization_id))
        return False


def retrieve_logs(identifier):
    """
    retrieve logs for a particular user
    :param identifer:
    :return:
    """
    client = MongoClient(settings.MONGOD_HOST, settings.MONGOD_PORT)
    db = client[settings.MONGOD_DATABASE]

    activities = db.activities

    count = activities.find({'id': identifier}).count()
    logs = map(lambda x: x, activities.find({'id': identifier}))

    return {
        'count': count,
        'results': logs
    }


def get_custom_domain(request):
    """
    get sub domain
    :param request:
    :return:
    """
    domain = 'http://%s' % settings.SCHEME

    try:
        path = request.path
        split_path = path.split('/')

        if split_path[0] not in settings.RESTRICTED_ROUTES and len(split_path[0]) > 0:

            if models.Organization.objects.filter(code=split_path[0].lower()).exists():
                domain = 'http://%s/%s' % (settings.SCHEME, split_path[0].lower())

        return domain
    except:
        return domain


@celery_app.task
def index_instance(model_name, pk):
    """
    index model instance in elasticsearch
    :return:
    """
    # try:
    from app import models
    title = model_name.title().replace(' ', '')
    model_klass = getattr(models, title)

    klass = getattr(app_serializers, '%sSerializer' % title)
    instance = model_klass.objects.get(pk=pk)

    serialized = klass(instance)
    data = serialized.data
    data['pk'] = instance.pk

    if model_name == 'card':
        data['slugs'] = data['categories']
        data['b64_url'] = "data:image/png;base64,%s" % get_as_base64(instance.image_url)
        data['sent_counts'] = instance.sent_counts
        data['saved_counts'] = instance.saved_counts
        data['shared_count'] = instance.shared_count
        # data['thumbnail_url'] = instance.thumbnail.image_url if hasattr(instance, 'thumbnail') else instance.image_url

        # clean up directory cache
        clean_card_categories(instance.pk, data['slugs'])

    elif model_name == 'category':
        _section = models.Section.objects.get(pk=data['section_id'])
        klass = getattr(app_serializers, 'SectionSerializer')
        section = klass(_section)
        _data = section.data
        data['permissible_organizations'] = _data['permissible_organizations']

    # elif model_name == 'banner':
    #     image_url = data['image_url']
    #     data['b64_url'] = "data:image/png;base64,%s" % get_as_base64(image_url)

    elif model_name == 'organization':
        rewrite_css_file(instance.pk)
        #     if data.get('logo'):
        #         image_url = data['logo']
        #         data['logo'] = "data:image/png;base64,%s" % get_as_base64(image_url)

    es.index(index=settings.ES_INDEX, doc_type=model_name, id=instance.pk, body=data)
    cache.cache_single_record(model_name, **data)

    return True
    # except:
    #     return False


@celery_app.task
def delete_instance(model_name, serialized_instance):
    """
    index model instance in elasticsearch
    :return:
    """
    try:
        instance = Payload(serialized_instance)
        instance_id = instance.pk
        try:
            es.delete(index=settings.ES_INDEX, doc_type=model_name, id=instance_id)
        except Exception, e:
            print '==============='
            print e

        if model_name == 'card':
            redis_obj.hdel('card', instance_id)
            redis_obj.hdel('featured_cards', instance_id)

            for category in instance.categories:
                redis_obj.hdel('%s:cards' % category, instance.slug)

        elif model_name == 'category':
            redis_obj.delete('%s:cards' % instance.slug)
            section = instance.section

            klass = getattr(app_serializers, 'SectionSerializer')
            serialized = klass(section)
            data = serialized.data

            for org in models.Organization.objects.all():
                if redis_obj.hexists('%s_sections' % org.code):
                    cache.cache_records('%s_sections' % org.code, section.pk, **data)

        elif model_name == 'section':
            for org in models.Organization.objects.all():
                if redis_obj.hexists('%s_sections' % org.code, instance_id):
                    redis_obj.hdel('%s_sections' % org.code, instance_id)

        elif model_name == 'organization':
            redis_obj.hdel('organization', instance.code)

        else:
            redis_obj.hdel(model_name, instance_id)

        return True
    except:
        return False


@celery_app.task
def trigger_notification(message, link, target_user_ids=[], event=None, **kwargs):
    """
    send notifications using pusher
    :param message:
    :param link:
    :param event:
    :param target_user_ids:
    :return:
    """
    if not event:
        event = settings.PUSHER_DEFAULT_EVENT

    try:
        for user_id in target_user_ids:
            _user = models.Account.objects.get(pk=user_id)
            notification = models.Notifications.objects.create(body=message, link=link, event=event, user=_user)
            data = kwargs.copy()
            data.update({'message': notification.body, 'link': notification.link})
            pusher_client.trigger(str(user_id), event, data)
        return True
    except Exception, e:
        print '====={}===='.format(e)
        return False


class Payload(object):
    def __init__(self, j):
        for key, value in j.items():
            if isinstance(value, (list, tuple)):
                setattr(self, key, [Payload(item) if isinstance(item, dict) else item for item in value])
            else:
                setattr(self, key, Payload(value) if isinstance(value, dict) else value)

    def __getitem__(self, item):
        return getattr(self, item)


def load_random_cards(slug):
    try:
        random_query = es.search(index=settings.ES_INDEX, doc_type='card',
                                 body={'query': {
                                     'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
                                         'random_score': {'seed': random.randint(1, 1000000)}
                                     }]}}, 'filter': {
                                     'bool': {'must': [{'term': {'is_public': True}}],
                                              'must_not': [{'term': {'slug': slug}}]}
                                 }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)

        random_hits = random_query['hits']

        if random_hits['total'] == 0:
            random_cards = models.Card.objects.filter(is_active=True, is_public=True).exclude(slug=slug).order_by(
                '?')[:6].all()

        else:
            _random = random_hits['hits']
            random_cards = filter(lambda x: x.slug != slug, [Payload(i['_source']) for i in _random])
    except:
        random_cards = models.Card.objects.filter(is_active=True, is_public=True).exclude(slug=slug).order_by(
            '?')[:6].all()

    return random_cards


def validate_url(url):
    """
    check if url is valid
    :param url:
    :return:
    """
    parsed_url = urlparse.urlparse(str(url))
    return bool(parsed_url.scheme)


@celery_app.task
def set_birthday_reminder(contact_id, domain='kaadie.com', **kwargs):
    """
    Adds a periodic task that schedules a birthday reminder for a contact
    :param contact_id:
    :param domain
    :param kwargs:
    :return:
    """
    try:
        contact = models.Contact.objects.get(pk=contact_id)
        account = contact.user

        scheduled_time = dt_time(settings.BIRTHDAY_ALERT_HOUR, settings.BIRTHDAY_ALERT_MINUTE)

        if models.Settings.objects.filter(account=account.pk).exists():
            setting = models.Settings.objects.filter(account=account.pk).first()
            scheduled_time = setting.birthday_alert_time

        dob = contact.address.date_of_birth

        crontab_, created = CrontabSchedule.objects.get_or_create(
            minute=scheduled_time.minute,
            hour=scheduled_time.hour,
            day_of_week=dob.isoweekday(),
            day_of_month=dob.day,
            month_of_year=dob.month
        )

        task = PeriodicTask.objects.filter(name='Birthday Schedule for %s[%s %s %s]'
                                                % (contact.name, contact.phone, contact.email, account.user.email)
                                           ).first()
        if not task:
            PeriodicTask.objects.create(
                crontab=crontab_,
                name='Birthday Schedule for %s[%s %s %s]' % (
                    contact.name, contact.phone, contact.email, account.user.email),
                task='app.tasks.send_birthday_reminder',
                kwargs=json.dumps({
                    'contact_id': contact.id,
                    'domain': domain
                })
            )
        else:
            task.crontab = crontab_
            task.save()

        contact.is_reminder_set = True
        contact.save()

        return True
    except Exception, e:
        return False


def generate_referral_code():
    """
    generate referral code per account id
    :return:
    """
    code = ''.join(random.choice(string.ascii_letters + string.digits) for c in range(8))

    while models.Settings.objects.filter(referral_code=code).exists():
        code = ''.join(random.choice(string.ascii_letters + string.digits) for c in range(8))

    return code


def clean_card_categories(card_id, categories):
    """
    clean card categories
    :return:
    """
    card = models.Card.objects.get(pk=card_id)
    card_categories = [c.slug for c in card.categories.all()]
    filtered_categories = filter(lambda x: x not in categories, card_categories)

    for filtered in filtered_categories:
        redis_obj.hdel('%s:cards' % filtered, card.slug)

    return True


@celery_app.task
def test_scheduler():
    print(u"This is the oddest thing i've seen in a while")


def create_thumbnail(image_url, filename, size=(360,360)):
    """
    create thumbnail
    """
    try:
        imageUrl = urlopen(image_url)
        image_file = io.BytesIO(imageUrl.read())
        image = Image.open(image_file)
        image.thumbnail(size, Image.ANTIALIAS)
        image.save(filename, 'JPEG')

        return filename
    except Exception, e:
        raise e


def create_card_thumbnail(card_id, size=(360,360)):
    """
    create and upload card thumbnail
    """
    try:
        card = models.Card.objects.get(pk=card_id)
        filename = '{}.jpg'.format(card.slug)
        image_path = create_thumbnail(card.image_url, filename, size)

        with open(image_path) as f:
            b64_code = base64.b64encode(f.read())
            thumb_filename = 'thumbnails_%s' % filename
            resp = upload_b64_s3(b64_code, thumb_filename)
            if resp:
                os.remove(image_path)
                thumbnail_url = '{}/{}'.format(settings.S3_BASE_ADDRESS, thumb_filename)
                if hasattr(card, 'thumbnail'):
                    card.image_url = thumbnail_url
                    card.save()
                else:
                    models.CardThumbnail.objects.create(card=card, image_url=thumbnail_url)
                return True, thumbnail_url

        return False, {}
    except Exception, e:
        raise e
