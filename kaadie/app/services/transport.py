import logging
import time
import hashlib
import json
import os
from datetime import datetime

from django.conf import settings
from django.db.models import Q

from sendgrid import *
from sendgrid.helpers import mail as sendgrid_mail
from twilio import TwilioRestException
from twilio.rest import TwilioRestClient
from kafka import KafkaProducer
from django_celery_beat.models import CrontabSchedule, PeriodicTask

from app import models
from app.signals import save_thread
from kaadie import celery_app
from . import helper

log = logging.getLogger('app')


class MailService(object):
    @classmethod
    def send_mail(cls, subject, sender, sender_name, recipient, body, recipient_name='', attachments=[], filepaths=[]):
        """
        send email via sendgrid
        :param subject:
        :param sender:
        :param sender_name:
        :param recipient:
        :param recipient_name:
        :param body:
        :return: success/error response
        """
        _mail = sendgrid_mail.Mail()
        _mail.set_from(Email(sender, sender_name))
        _mail.set_subject(subject)

        personalization = sendgrid_mail.Personalization()
        personalization.add_to(Email(recipient, recipient_name))
        _mail.add_personalization(personalization)
        _mail.add_content(sendgrid_mail.Content("text/html", "<html><body>%s</body></html>" % body))

        try:
            sg = SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)
            sg.client.mail.send.post(request_body=_mail.get())

            log_message = '[MESSAGE: %s][RECIPIENTS: %s][STATUS: SUCCESS][SENDER: %s]' % (body, recipient, sender)
            log.info(log_message)
            log.debug(log_message)

            return True
        except:
            log_message = '[MESSAGE: %s][RECIPIENTS: %s][STATUS: FAILED][SENDER: %s]' % (body, recipient, sender)
            log.info(log_message)
            log.debug(log_message)
            return False

    @classmethod
    def send_ses_mail(cls, subject, sender, sender_name, recipient, body, recipient_name):
        pass

    @classmethod
    def send_card(cls, card_id, user_id, email_recipients, phone_recipients, sender_name, sender,
                  domain, card_image=None, cover_image=None, subject=None, personalized_message='', tag='general',
                  **kwargs):
        """
        send card to recipients
        :param card_id:
        :param user_id
        :param email_recipients: email recipients
        :param phone_recipients: sms recipients
        :param personalized_message:
        :param domain:
        :param sender_name:
        :param sender:
        :param card_image:
        :param cover_image:
        :param subject:
        :param tag
        :param kwargs:
        :return:
        """
        card = models.Card.objects.get(pk=card_id)
        if not subject:
            subject = 'Hello, %s just sent you a kaadie e-card' % sender_name

        recp = ','.join(phone_recipients + email_recipients)

        user = models.Account.objects.get(pk=user_id)

        _id = '%s_%s_%s' % (recp, sender, str(time.time()))
        notification_id = hashlib.md5(_id).hexdigest()
        link = helper.shorten_url('%s/sent/%s' % (domain, notification_id))

        # upload cover image base64 image
        cover_image_url = card.image_url
        cover_image_title = "cover-%s.jpg" % notification_id

        if helper.validate_url(cover_image):
            cover_image_url = cover_image
        else:
            cover_uploaded = helper.upload_b64_s3(cover_image, cover_image_title)
            if cover_uploaded:
                cover_image_url = '%s/%s' % (settings.S3_BASE_ADDRESS, cover_image_title)

        # upload card interior image base64 image
        image_url = None
        if card_image:
            if helper.validate_url(card_image):
                image_url = card_image
            else:
                title = 'image-%s.jpg' % notification_id
                uploaded = helper.upload_b64_s3(card_image, title)
                if uploaded:
                    image_url = '%s/%s' % (settings.S3_BASE_ADDRESS, title)

        sent_card = models.SentCard.objects.create(card=card, user=user, message=personalized_message,
                                                   notification_id=notification_id, image_url=cover_image_url,
                                                   recipients=recp)

        # try:
        cls.send_sms(sent_card.pk, phone_recipients, sender_name, domain=domain,
                     personalized_message=personalized_message, tag=tag, link=link, notification_id=notification_id,
                     card_image=card_image, cover_image=cover_image)

        # extract thumbnail
        thumbnail_url = settings.DEFAULT_THUMBNAIL
        if not hasattr(card, 'thumbnail'):
            resp, _thumbnail_url = helper.create_card_thumbnail(card.pk, (150,150))
            if resp is True:
                thumbnail_url = _thumbnail_url

        sg = SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)

        for rec in email_recipients:
            if len(rec) != 0:
                _user = models.User.objects.filter(email=rec).first()
                name = _user.account.name if _user else ''

                content = helper.build_html_message(sender_name, domain, link, image_url, thumbnail_url, tag, name)

                data = {
                    "personalizations": [
                        {
                            "to": [
                                {
                                    "email": rec
                                }
                            ],
                            "subject": subject
                        }
                    ],
                    "from": {
                        "email": sender,
                        "name": sender_name
                    },
                    "content": [
                        {
                            "type": "text/html",
                            "value": content
                        }
                    ]
                }

                sg.client.mail.send.post(request_body=data)

                # upload edited image to S3
                models.ReceivedCard.objects.create(user=sent_card.user, sent_card=sent_card, card=card,
                                                   message=personalized_message, recipient=rec,
                                                   notification_id=notification_id, image_url=image_url,
                                                   cover_image_url=cover_image_url)

                if _user:
                    helper.trigger_notification.delay('You just received a card from %s' % user.name, link, [_user.pk],
                                                      image=cover_image_url)

        return True
        # except Exception, e:
        #     print '========'
        #     print(e)
        #     print '========'
        #     return False

    @staticmethod
    def send_sms(sent_card_id, recipients, sender_name, domain, personalized_message, tag, link, notification_id,
                 cover_image, card_image=None, sender='+12074820335'):
        """
        send sms using twilio
        :param recipients:
        :param sent_card_id
        :param sender:
        :param sender_name
        :param personalized_message
        :param tag
        :param link
        :param notification_id
        :param card_image
        :param cover_image
        :return:
        """
        account_sid = settings.TWILIO_ACCOUNT_SID
        auth_token = settings.TWILIO_ACCOUNT_TOKEN

        client = TwilioRestClient(account_sid, auth_token)

        try:
            sent_card = models.SentCard.objects.get(pk=sent_card_id)
            for recipient in recipients:
                if len(recipient) != 0:
                    num = '+%s%s' % (settings.DEFAULT_COUNTRY_CODE, int(recipient))
                    address = models.Address.objects.filter(Q(phone=recipient) | Q(phone=num)).first()
                    account = None
                    if address:
                        account = models.Account.objects.filter(address=address.pk).first()

                    name = account.name if account else ''

                    body = helper.build_sms_message(sender_name, domain, link, tag, name)

                    if recipient.startswith(settings.DEFAULT_COUNTRY_CODE):
                        num = '+%s' % int(recipient)
                    elif recipient.startswith('+%s' % settings.DEFAULT_COUNTRY_CODE):
                        num = recipient
                    client.messages.create(body=body, to=num, from_=sender)

                    # upload cover image base64 image
                    cover_image_url = sent_card.card.image_url
                    cover_image_title = "cover-%s.jpg" % notification_id
                    cover_uploaded = helper.upload_b64_s3(cover_image, cover_image_title)
                    if cover_uploaded:
                        cover_image_url = '%s/%s' % (settings.S3_BASE_ADDRESS, cover_image_title)

                    # upload card interior image base64 image
                    card_image_url = None
                    if card_image:
                        title = 'image-%s.jpg' % notification_id
                        image_uploaded = helper.upload_b64_s3(card_image, title)
                        if image_uploaded:
                            card_image_url = '%s/%s' % (settings.S3_BASE_ADDRESS, title)

                    models.ReceivedCard.objects.create(user=sent_card.user, sent_card=sent_card, card=sent_card.card,
                                                       recipient=recipient, notification_id=notification_id,
                                                       image_url=card_image_url, cover_image_url=cover_image_url,
                                                       message=personalized_message)
                    if account:
                        helper.trigger_notification.delay('You just received a card from %s' % sender_name, link,
                                                          [account.pk], image=cover_image_url)
            return True
        except TwilioRestException as e:
            return False


@celery_app.task
def send_card(card_id, user_id, email_recipients, phone_recipients, sender_name, sender, domain,
              card_image=None, cover_image=None, subject=None, file_path=None, personalized_message='', tag='general'):
    """
    queue celery send task
    :param card_id:
    :param user_id
    :param email_recipients:
    :param phone_recipients:
    :param personalized_message:
    :param sender_name:
    :param sender:
    :param domain:
    :param card_image:
    :param cover_image:
    :param subject
    :param file_path
    :return:
    """
    card = models.Card.objects.get(pk=card_id)
    account = models.Account.objects.get(pk=user_id)

    if file_path:
        resp, phones, emails = helper.create_contacts_from_spreadsheet(user_id, file_path, domain)
        phone_recipients = list(sorted(phones + phone_recipients))
        email_recipients = list(sorted(email_recipients + emails))

    resp = MailService.send_card(card.pk, account.pk, email_recipients, phone_recipients, sender_name, sender, domain,
                                 card_image, cover_image, subject, personalized_message, tag=tag)

    if resp is False:
        log.error('[CARD: %s][RECIPIENTS: %s][STATUS: FAILED][SENDER: %s]' % (card.title,
                                                                              (email_recipients + phone_recipients),
                                                                              account.name))
        return True

    save_thread.send(sender=None, emails=email_recipients, phones=phone_recipients, user=account.user)

    log_message = '[CARD: %s][RECIPIENTS: %s][STATUS: SUCCESS][SENDER: %s]' % (card.title,
                                                                               (email_recipients + phone_recipients),
                                                                               account.name)
    log.info(log_message)
    log.debug(log_message)

    return True


def publish_message(module_name, function_name, *args, **kwargs):
    """
    publish message via kafka
    :param module_name:
    :param function_name:
    :param args:
    :param kwargs:
    :return:
    """
    # Block for 'synchronous' sends

    file_path = os.path.realpath(__file__)
    # try:
    producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                             value_serializer=lambda m: json.dumps(m).encode('ascii'))
    # request_timeout_ms=1000, max_block_ms=1000

    print(producer)

    # Asynchronous by default
    future = producer.send(str('messages'), {'mod_name': module_name, 'fxn_name': function_name, 'args': args,
                                             'data': kwargs})

    print(future)

    future.get(timeout=10)
    if future.succeeded():
        return True
    return False
    # except errors.KafkaError, e:
    #     # Decide what to do if produce request failed...
    #     print(e)
    #     return False


@celery_app.task
def request_artist_permission(email, portfolio, name, nickname):
    """
    request artist permission
    :param email:
    :param portfolio:
    :param name:
    :param nickname
    :return:
    """
    try:
        art_request = models.ArtistRequest.objects.create(portfolio=portfolio, nickname=nickname, email=email)
        request_url = '{}/admin/app/artistrequest/{}/change'.format(settings.DOMAIN, art_request.pk)
        request_message = "<p>Hello Admin,</p>" \
                          "<p>%s (%s) wants to join Kaadie as an artist.</p><p>Click here to view his portfolio %s</p>" \
                          "<p>As an artist, he can create cards that can be" \
                          "viewed, sent and saved by all visitors to the platform.</p>" \
                          "<p>To grant access, accept this user's request on the admin panel - %s.</p>" % (
                              name, email, portfolio, request_url)

        return MailService.send_mail('Artist Rights Permission', settings.ARTIST_MANAGER, 'Kaadie',
                                     settings.ALERT_ACCOUNT,
                                     request_message, 'Kaadie')
    except Exception, e:
        print('=======')
        print e
        print('=======')


def send_via_twilio(sender, body, recipients=list()):
    """

    :param sender:
    :param body:
    :param recipient:
    :return:
    """
    account_sid = settings.TWILIO_ACCOUNT_SID
    auth_token = settings.TWILIO_ACCOUNT_TOKEN

    client = TwilioRestClient(account_sid, auth_token)

    errs = []

    for val in recipients:
        try:
            client.messages.create(body=body, to=val, from_=sender)
        except TwilioRestException as e:
            errs.append(e)

    return errs


# def send_comment_alert(recipient, subject, body, link, user_id):
#     """
#     send an alert to a recipient's email address on post comment
#     :param recipient:
#     :param subject
#     :param body:
#     :param link:
#     :param user_id:
#     :return:
#     """
#     email_alert = MailService.send_mail(subject, 'notifications@kaadie.com', 'Kaadie | Notification Alert', recipient,
#                                         body)
#     notification_alert = helper.trigger_notification.delay(subject, link, [user_id])
#
#     return email_alert, notification_alert


@celery_app.task
def send_feedback(subject, email, body, alert_link, recipient_id):
    """
    send feedback on comment response
    :return:
    """
    email_alert = MailService.send_mail(subject, 'notifications@kaadie.com', 'Kaadie | Notification Alert', email,
                                        body)
    notification_alert = helper.trigger_notification.delay(subject, alert_link, [recipient_id])

    return email_alert, notification_alert


@celery_app.task
def schedule_card(scheduled_time, card_id, user_id, email_recipients, phone_recipients, sender_name, sender, domain,
                  card_image=None, cover_image=None, subject=None, file_path=None, personalized_message='',
                  tag='general'):
    """
    :param scheduled_time
    :param card_id
    :param user_id
    :param email_recipients
    :param phone_recipients
    :param sender_name
    :param sender
    :param domain
    :param card_image
    :param cover_image
    :param subject
    :param file_path
    :param personalized_message
    :param tag
    """
    try:
        schedule = datetime.strptime(scheduled_time, '%B %d, %Y %I:%M %p')
        crontab_, created = CrontabSchedule.objects.get_or_create(
                minute=schedule.minute,
                hour=schedule.hour,
                day_of_week=schedule.isoweekday(),
                day_of_month=schedule.day,
                month_of_year=schedule.month
            )

        card = models.Card.objects.get(pk=card_id)
        account = models.Account.objects.get(pk=user_id)
        return PeriodicTask.objects.create(
            crontab=crontab_,
            name='Card Scheduled by %s for %s at %s' % (account.user.email, scheduled_time, time.time()),
            task='app.services.transport.send_card',
            kwargs=json.dumps({ 'card_id': card.id, 'user_id': account.id, 'email_recipients': email_recipients,
                                'phone_recipients': phone_recipients, 'sender_name': sender_name, 'sender': sender,
                                'domain': domain, 'card_image': card_image, 'cover_image': cover_image, 'subject': subject,
                                'file_path': file_path, 'personalized_message': personalized_message, 'tag': tag})
        )
    except Exception, e:
        log.error('[FAILED TO SCHEDULE CARD] [OBJECT: %s]' % e)
        return False

