import uuid

from django.conf import settings
from app import models


def check_sms_credit(user_id):
    """
    returns sms credit available for user matching user_id
    :param user_id:
    :return:
    """
    account = models.Account.objects.filter(pk=user_id).first()

    if not account:
        raise Exception

    wallet = models.Wallet.objects.filter(account=account.pk).first()

    if not wallet:
        wallet = models.Wallet.objects.create(account=account)

    return wallet.sms_credit


def sms_limit_balance(user_id):
    """
    returns True if sms still available for this user
    :param user_id:
    :return:
    """
    account = models.Account.objects.filter(pk=user_id).first()

    if not account:
        return -1

    sending_limit = settings.SENDING_LIMIT

    setting = models.Settings.objects.filter(account=account.pk).first()

    if setting:
        sending_limit = setting.sending_limit

    return sending_limit - account.sent_count


def generate_transction_id(user_id):
    """
    generate transaction id for an sms purchase
    :param user_id:
    :return:
    """
    unique_id = str(uuid.uuid4())
    while models.SMSTransaction.objects.filter(account=user_id, uuid=unique_id).exists():
        unique_id = str(uuid.uuid4())

    return unique_id


def credit_user(user_id, transaction_id):
    """
    :param user_id:
    :param transaction_id
    :return:
    """
    account = models.Account.objects.filter(pk=user_id).first()

    if not account:
        raise Exception

    transaction = models.SMSTransaction.objects.filter(account=account.pk, uuid=transaction_id).first()

    if not transaction:
        raise Exception

    wallet = models.Wallet.objects.filter(account=account.pk).first()

    if not wallet:
        wallet = models.Wallet.objects.create(account=account)

    wallet.sms_credit += transaction.credit.sms_credit
    wallet.save()

    return True


def debit_sms_credit(user_id):
    """
    debit sms credit
    :param user_id:
    :return:
    """
    account = models.Account.objects.filter(pk=user_id).first()

    if not account:
        raise Exception

    wallet = models.Wallet.objects.filter(account=account.pk).first()

    if not wallet:
        wallet = models.Wallet.objects.create(account=account)

    wallet.sms_credit -= 1
    wallet.save()

    return True

