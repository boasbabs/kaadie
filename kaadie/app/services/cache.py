import json, random
from itertools import chain

from django.conf import settings

from app import models, serializers
from kaadie import es, redis_obj


class Payload(object):
    def __init__(self, j):
        for key, value in j.items():
            if isinstance(value, (list, tuple)):
                setattr(self, key, [Payload(item) if isinstance(item, dict) else item for item in value])
            else:
                setattr(self, key, Payload(value) if isinstance(value, dict) else value)

    def __getitem__(self, item):
        return getattr(self, item)


def get_organization(slug=None):
    """
    returns an organization
    :param slug:
    :return:
    """

    # code = slug.lower()
    # if not Organization.objects.filter(code=code).exists():
    #     code = 'kaadie'

    if slug is None:
        slug = 'kaadie'

    organ = redis_obj.hget('organization', slug)
    if organ:
        return Payload(json.loads(organ))

    organization = models.Organization.objects.filter(code=slug.lower()).first()

    return organization


def cache_single_record(model_name, pk, **kwargs):
    """
    cache individual record in redis
    :param model_name:
    :param pk:
    :param kwargs:
    :return:
    """
    title = model_name.title().replace(' ', '')
    model_klass = getattr(models, title)

    instance = model_klass.objects.get(pk=pk)

    orgs = models.Organization.objects.all()

    if model_name in settings.CACHED_MODELS:
        if model_name == 'section':
            for org in orgs:
                cache_records('%s_sections' % org.code, pk, **kwargs)

        elif model_name == 'category':
            cache_records('category', '%s:%s' % (instance.section_id, instance.slug), **kwargs)
            cards = (serializers.CardSerializer(c).data for c in instance.cards.all())
            for _card in cards:
                cache_records('%s:cards' % instance.slug, _card['slug'], **_card)

            section = models.Section.objects.filter(pk=instance.section_id).first()
            if section:
                section_data = serializers.SectionSerializer(section).data
                for _org in section.permissible_organizations.all():
                    cache_records('%s_sections' % _org.code, section.pk, **section_data)

        elif model_name == 'saved card':
            serialized = serializers.CardSerializer(instance.card).data
            cache_records('saved_cards:%s' % instance.user.pk, serialized['pk'], **serialized)

        elif model_name == 'card':

            categories = (c for c in instance.categories.all())
            for category in categories:
                if instance.is_active:
                    cache_records('%s:cards' % category.slug, instance.slug, **kwargs)
                else:
                    redis_obj.hdel('%s:cards' % category.slug, instance.slug)

            if instance.is_featured:
                cache_records('featured_cards', instance.pk, **kwargs)
            else:
                redis_obj.hdel('featured_cards', instance.pk)

        else:
            cache_records(model_name, instance.pk, **kwargs)

    return True


def cache_banners():
    """
    fetch all banners
    :return:
    """
    banner_query = es.search(index=settings.ES_INDEX, doc_type='banner',
                            body={'query': {'function_score': {'query': {'bool': {'must': [
                                {'match': {'is_active': True}}
                            ]}}}}})

    # banner_query = es.search(index=settings.ES_INDEX, doc_type='banner', body={'query': {'match_all': {}}})
    banner_hits = banner_query['hits']

    if banner_hits['total'] == 0:
        _banners = (serializers.BannerSerializer(i).data for i in models.Banner.objects.all())
    else:
        _banners = (c['_source'] for c in banner_hits['hits'])

    for ban in _banners:
        cache_records('banner', ban['pk'], **ban)

    return True


def cache_featured_cards():
    """
    Fetch all featured cards
    :return:
    """
    # fetch_query = es.search(index=settings.ES_INDEX, doc_type='card',
    #                         body={'query': {'function_score': {'query': {'bool': {'must': [
    #                             {'match': {'is_active': True}},
    #                             {'match': {'is_featured': True}},
    #                             {'match': {'is_public': True}}
    #                         ]}}}}})
    #
    # fetch_hits = fetch_query['hits']
    #
    # if fetch_hits['total'] == 0:
    #     _cards = [serializers.CardSerializer(c).data for c in models.Card.objects.filter(is_featured=True,
    #                                                                                      is_active=True,
    #                                                                                      is_public=True).order_by(
    #         '?').all()]
    # else:
    #     _cards = [c['_source'] for c in fetch_hits['hits']]

    _cards = [serializers.CardSerializer(c).data for c in models.Card.objects.filter(is_featured=True, is_active=True,
                                                                                     is_public=True).order_by('?').all()
              ]

    for _card in _cards:
        card_id = _card.get('pk')
        cache_records('featured_cards', obj_id=card_id, **_card)

    return True


def cache_categories():
    """
    fetch sections matching this organization code
    :param code: organization code
    :return:
    """
    # category_query = es.search(index=settings.ES_INDEX, doc_type='category',
    #                           body={'query': {'match_all': {}}})
    # cat_hits = category_query['hits']
    #
    # if cat_hits['total'] == 0:
    #
    # else:
    #     categories = [c['_source'] for c in cat_hits['hits']]

    categories = models.Category.objects.all()

    for item in categories:
        cache_records('category', '%s:%s' % (item.section_id, item.slug),
                      **serializers.CategorySerializer(item).data)
        cards = (serializers.CardSerializer(c).data for c in item.cards.all())
        for _card in cards:
            cache_records('%s:cards' % item.slug, _card['slug'], **_card)

    return True


# def fetch_saved_cards(user_id):
#     """
#     fetch saved cards by user id
#     :return:
#     """
#     saved_query = es.search(index=settings.ES_INDEX, doc_type='saved card', body={'query': {'match': {'user_id':
#                                                                                                           user_id}}})
#     saved_hits = saved_query['hits']
#
#     if saved_hits['total'] == 0:
#         account = Account.objects.get(pk=user_id)
#         return [app_serializers.CardSerializer(c.card).data for c in account.saved_cards.all()]
#
#     _saved = saved_hits['hits']
#
#     return [Payload(c['_source']['card']) for c in _saved]


def cache_saved_cards(user_id):
    """
    fetch saved cards by user id
    :return:
    """
    saved_query = es.search(index=settings.ES_INDEX, doc_type='saved card', body={'query': {'match': {'user_id':
                                                                                                          user_id}}})
    saved_hits = saved_query['hits']

    if saved_hits['total'] == 0:
        account = models.Account.objects.get(pk=user_id)
        _saved = (serializers.CardSerializer(c.card).data for c in account.saved_cards.all())
    else:
        _saved = [c['_source']['card'] for c in saved_hits['hits']]

    for s in _saved:
        cache_records('saved_cards:%s' % user_id, s['pk'], **s)

    return True


def cache_records(hash_name, obj_id, **kwargs):
    """
    cache dictionary
    :param obj_id:
    :param hash_name:
    :param kwargs:
    :return:
    """
    if redis_obj.hexists(hash_name, obj_id):
        redis_obj.hdel(hash_name, obj_id)

    res = redis_obj.hset(hash_name, obj_id, json.dumps(kwargs))
    return res


def cache_organization():
    """
    save organization into redis cache
    :return:
    """
    organs_query = es.search(index=settings.ES_INDEX, doc_type='organization', body={'query': {'match_all': {}}})

    organs_hits = organs_query['hits']

    if organs_hits['total'] == 0:
        _organs = [serializers.OrganizationSerializer(c).data for c in models.Organization.objects.all()]
    else:
        _organs = [c['_source'] for c in organs_hits['hits']]

    for s in _organs:
        cache_records('organization', s['code'], **s)

    return True


def fetch_categories(code, size=6):
    """
    fetch categories by organization code
    :param code:
    :return:
    """
    organization = models.Organization.objects.filter(code=code).first()
    if not organization:
        return False, models.Category.objects.filter(is_active=True).order_by('?')[:size].all()

    try:
        cat_query = es.search(index=settings.ES_INDEX, doc_type='category',
                              body={'query': {'function_score': {'query': {'terms': {
                                  'permissible_organizations': [code]
                              }}, 'functions': [{
                                  'random_score': {'seed': random.randint(1, 1000000)}
                              }]}}, 'filter': {
                                  'bool': {'must': [{'term': {'is_active': True}}]}
                              }, 'sort': [{"_score": {"order": "desc"}}]}, size=size)

        cat_hits = cat_query['hits']

        if cat_hits['total'] == 0:
            return False, models.Category.objects.filter(is_active=True,
                                                         section__permissible_organizations__in=[organization
                                                                                                 ]).order_by('?')[
                          :size].all()
        _categories = [c['_source'] for c in cat_hits['hits']]
        slugs = [c['slug'] for c in _categories]

        query = es.search(index=settings.ES_INDEX, doc_type='card', body={'query': {'terms': {'slugs': slugs}}},
                          size=50)

        query_hits = query['hits']

        if query_hits['total'] == 0:
            return False, models.Category.objects.filter(is_active=True,
                                                         section__permissible_organizations__in=[organization
                                                                                                 ]).order_by('?')[
                          :size].all()

        hits = query_hits['hits']

        data = [i.get('_source') for i in hits]

        unique_keys = list(set(list(chain.from_iterable([c['slugs'] for c in data]))))
        categories = []

        for key in unique_keys:
            try:
                cat = (item for item in _categories if item['slug'] == key).next()
                cat['cards'] = [j for j in data if key in j['slugs']]
                categories.append(cat)
            except:
                continue

        print('------------------------------')
        __categories = filter(lambda x: len(x['cards']) > 0, categories)
        return True, __categories
    except Exception, e:
        print('==============================')
        print(e)
        return False, models.Category.objects.filter(is_active=True,
                                                     section__permissible_organizations__in=[organization
                                                                                             ]).order_by('?')[:size].all()


def fetch_from_cache(code, user_id=None):
    """
    fetch cached elements
    :param user_id:
    :param code:
    :return:
    """
    _banners = redis_obj.hgetall('banner')
    banners = [json.loads(c) for c in _banners.values()]

    _featured = redis_obj.hgetall('featured_cards')
    featured_cards = [json.loads(c) for c in _featured.values()]

    # _sections = redis_obj.hgetall('%s_sections' % code)
    # __sections = [json.loads(c) for c in _sections.values()]
    # sections = sorted(__sections, key=lambda x: (x['position'] is None, x['position']))
    #
    # saved = []
    # if user_id:
    #     _saved = redis_obj.hgetall('saved_cards:%s' % user_id)
    #     saved = [Payload(json.loads(i)) for i in _saved.values()]

    return banners, featured_cards


def fetch_cache_saved_cards(user_id):
    """
    fetch saved cards from redis matching user_id
    :param user_id:
    :return:
    """
    saved = []
    try:
        _saved = redis_obj.hgetall('saved_cards:%s' % user_id)
        if len(_saved.values()) > 0:
            return [Payload(json.loads(i)) for i in _saved.values()]

        _user = models.Account.objects.filter(pk=user_id).first()
        if _user:
            saved = [c.card for c in _user.saved_cards.all()]
        return saved

    except Exception, e:
        _user = models.Account.objects.filter(pk=user_id).first()
        saved = []
        if _user:
            saved = [c.card for c in _user.saved_cards.all()]
        return saved


def fetch_cache_sections(code):
    """
    fetch cached sections
    :param code:
    :return:
    """
    try:
        _sections = redis_obj.hgetall('%s_sections' % code)
        sections = [Payload(json.loads(j)) for j in _sections.values()]
        return sorted(sections, key=lambda x: (x.position is None, x.position))
    except Exception, e:
        organization = get_organization(code)
        return models.Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by('position').all()


def cache_sections():
    """
    cache sections in redis
    :param
    :return:
    """
    organizations = models.Organization.objects.all()
    for org in organizations:
        cache_organization_sections(org.code)

    return True


def cache_organization_sections(code):
    """
    fetch sections matching this organization code
    :param code: organization code
    :return:
    """
    section_query = es.search(index=settings.ES_INDEX, doc_type='section',
                              body={'query': {'terms': {
                                  'permissible_organizations': [code]
                              }}})
    section_hits = section_query['hits']

    if section_hits['total'] == 0:

        organization = models.Organization.objects.filter(code=code).first()
        if not organization:
            return False, []

        _sections = [serializers.SectionSerializer(c).data for c in models.Section.objects.filter(
            permissible_organizations__in=[organization]).order_by('position').all()]
    else:
        _sections = [c['_source'] for c in section_hits['hits']]

    for item in _sections:
        cache_records('%s_sections' % code, item['pk'], **item)

    return True


def fetch_cache_banners():
    """
    fetch banners from cache
    :return:
    """
    _banners = redis_obj.hgetall('banner')
    return [Payload(json.loads(c)) for c in _banners.values()]
