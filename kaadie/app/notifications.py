from .models import Account

def received_card_notification(sender, recipient, received_card_id):
    """
    notification for when you receive a card
    :param sender:
    :param recipient:
    :param received_card_id:
    :return:
    """
    pass