#!/usr/bin/env python

from __future__ import absolute_import, unicode_literals
import logging
import threading, json, os
from importlib import import_module
from imp import load_source

from django.conf import settings

from kafka import KafkaConsumer


class Consumer(threading.Thread):
    daemon = True

    def run(self):
        consumer = KafkaConsumer(auto_offset_reset='latest', enable_auto_commit=False)
        consumer.subscribe(['messages'])

        for message in consumer:
            t = threading.Thread(target=Consumer.process_message(**json.loads(message.value)))
            t.start()

    @classmethod
    def process_message(cls, file_path, mod_name, fxn_name, *args, **kwargs):
        """
        process received message
        :param mod_name:
        :param fxn_name:
        :param args:
        :param kwargs:
        :return:
        """
        print '========= processing message ============'
        mod = load_source(mod_name, file_path)
        print(mod)
        fxn = getattr(mod, fxn_name)
        print(fxn)
        print '========= terminating message =========='


def main():
    thread = Consumer()
    thread.run()


if __name__ == "__main__":
    logging.basicConfig(
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.INFO
        )

    main()