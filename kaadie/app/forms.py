from django.forms import Form, ModelForm, EmailField, CharField, PasswordInput, BooleanField, \
    URLField, Textarea, IntegerField, FileField, RegexField, ChoiceField, RadioSelect
from django.template.defaultfilters import slugify
from s3direct.widgets import S3DirectWidget
from froala_editor.widgets import FroalaEditor
from tinymce.widgets import TinyMCE

from .models import Card, Organization, ArtistRequest


class CommaSeperatedStringField(CharField):
    """
    custom field for comma seperated strings
    """

    def formfield(self, **kwargs):
        """
        custom form field
        :param kwargs:
        :return:
        """
        defaults = {
            'form_class': RegexField,
            'regex': '^[\w,]+$',
            'max_length': self.max_length,
            'error_messages': {
                'invalid': u'Enter only digits separated by commas.',
            }
        }
        defaults.update(kwargs)
        return super(CommaSeperatedStringField, self).formfield(**defaults)


class LoginForm(Form):
    """
    LoginForm to authorize user access to application
    """
    email = EmailField(required=True)
    password = CharField(required=True, widget=PasswordInput)
    social_login = CharField(required=False)
    image_url = CharField(required=False)
    first_name = CharField(required=False)
    last_name = CharField(required=False)


class ForgotPasswordForm(Form):
    """
    forgot password
    """
    email = EmailField(required=True)


class ResetPasswordForm(Form):
    """
    forgot password
    """
    email = EmailField(required=True)
    token = CharField(required=True)
    password = CharField(required=True)


class RegistrationForm(Form):
    """
    LoginForm to authorize user access to application
    """
    username = EmailField(required=True)
    password = CharField(required=True, widget=PasswordInput)
    confirm_password = CharField(required=True, widget=PasswordInput)
    social_signup = CharField(required=False)
    image_url = URLField(required=False)
    name = CharField(max_length=255, required=True)
    referral_code = CharField(max_length=255, required=False)

    def clean(self):
        """
        check if passwords match
        :return:
        """
        cleaned_data = super(RegistrationForm, self).clean()

        if cleaned_data['password'] != cleaned_data['confirm_password']:
            self._errors['confirm_password'] = self.error_class(['Your passwords do not match.'])
            del self.cleaned_data['confirm_password']
        return cleaned_data


class ArtistRegistrationForm(Form):
    """
    LoginForm to authorize user access to application
    """
    username = EmailField(required=True)
    password = CharField(required=True, widget=PasswordInput)
    confirm_password = CharField(required=True, widget=PasswordInput)
    name = CharField(max_length=255, required=True)
    nickname = CharField(max_length=50, required=True)
    referral_code = CharField(max_length=255, required=False)
    portfolio = URLField(required=True)

    def clean(self):
        """
        check if passwords match
        :return:
        """
        cleaned_data = super(ArtistRegistrationForm, self).clean()

        if cleaned_data['password'] != cleaned_data['confirm_password']:
            self._errors['confirm_password'] = self.error_class(['Your passwords do not match.'])
            del self.cleaned_data['confirm_password']
        return cleaned_data


class ApiRegForm(Form):
    """
    Register a new account
    """
    username = EmailField(required=True)
    password = CharField(required=True, widget=PasswordInput)
    social_signup = CharField(required=False)
    image_url = URLField(required=False)
    name = CharField(max_length=255, required=True)


class BrandRegistrationForm(ApiRegForm):
    """
    Register a brand
    """
    brand_name = CharField(required=True)
    logo = URLField(required=False)


class ArtistRequestForm(Form):
    """
    Request to be an artist
    """
    nickname = CharField(required=True)
    portfolio = URLField(required=True)


class AdminRequestForm(ModelForm):
    """
    Request to be an artist
    """
    class Meta:
        model = ArtistRequest
        fields = ('email', 'nickname', 'portfolio', 'is_accepted')

    nickname = CharField(required=True)


class ProfileForm(Form):
    """
    ProfileForm to update a user's account information
    """
    first_name = CharField(required=True)
    last_name = CharField(required=False)
    phone = CharField(required=False)
    city = CharField(required=False)
    country = CharField(required=False)
    address = CharField(required=False)
    image_url = URLField(required=False)
    gender = CharField(required=False)
    language = CharField(required=False)


class BannerForm(ModelForm):
    title = CharField(required=True)
    link = CharField(required=True)
    image_url = URLField(widget=S3DirectWidget(dest='destination'))


class CategoryForm(ModelForm):
    title = CharField(required=True)
    description = CharField(widget=Textarea, required=False)
    image_url = URLField(widget=S3DirectWidget(dest='destination'), required=False)


class SectionForm(ModelForm):
    title = CharField(required=True)
    description = CharField(widget=Textarea, required=False)
    image_url = URLField(widget=S3DirectWidget(dest='destination'), required=False)


class CardForm(ModelForm):
    title = CharField(required=True)
    image_url = URLField(widget=S3DirectWidget(dest='destination'))
    display_image = URLField(widget=S3DirectWidget(dest='destination'), required=False)


class UploadContactForm(Form):
    file = FileField()


class UpdateProfileImage(Form):
    image = FileField()


class ContactForm(Form):
    """
    ContactForm to update a user's account information
    """
    first_name = CharField(required=True)
    last_name = CharField(required=False)
    phone = CharField(required=False)
    email = CharField(required=False)
    city = CharField(required=False)
    country = CharField(required=False)
    address = CharField(required=False)
    date_of_birth = CharField(required=False)


class OrganizationForm(ModelForm):
    logo = URLField(widget=S3DirectWidget(dest='destination'), required=False)


class UpdateOrganizationForm(ModelForm):
    class Meta:
        model = Organization
        fields = ('logo', 'color', 'semantic_class', 'primary_background_color', 'primary_color', 'primary_color_class',
                  'name', 'menu_color', 'code')

    logo = URLField(widget=S3DirectWidget(dest='destination'), required=False)


class CommentForm(Form):
    comment = CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}), required=True)


# class PostCommentForm(Form):
#     comment = CharField(required=False)
#     received_card_id = IntegerField(required=True)


class BaseCardForm(Form):
    """
    Base Form for creating cards
    """
    description = CharField(widget=Textarea, required=False)
    card_image = CharField(required=True)
    categories = CharField(required=True)


class PremiumCardForm(Form):
    """
    Extension of CreateCard Form
    """
    title = CharField(required=True)
    image_url = URLField(widget=S3DirectWidget(dest='destination'), label='Card Image')
    plan = CharField(required=False)
    description = CharField(widget=Textarea, required=False)
    categories = CharField(required=True)

    def clean(self):
        """
        check if card already exists
        :return:
        """
        cleaned_data = super(PremiumCardForm, self).clean()
        card = Card.objects.filter(slug=slugify(cleaned_data['title'])).first()

        if card:
            self._errors['title'] = self.error_class(['A Card with this title already exists'])
        return cleaned_data


class EditCardForm(Form):
    """
    Base Form for creating cards
    """
    description = CharField(widget=Textarea, required=False)
    image_url = URLField(widget=S3DirectWidget(dest='destination'), label='Card Image')
    categories = CharField(required=True)


class SendCardForm(Form):
    """
    Send Card API form
    """
    csv = FileField(allow_empty_file=True, required=False)
    email_recipients = CommaSeperatedStringField(required=True)
    phone_recipients = CharField(required=False)
    sender_name = CharField(required=True)
    message = CharField(widget=Textarea, required=False)
    card_image = CharField(required=False)
    card_id = IntegerField(required=True)

    def clean(self):
        """
        check if card already exists
        :return:
        """
        cleaned_data = super(SendCardForm, self).clean()

        if cleaned_data.get('card_id') is not None:
            if not Card.objects.filter(pk=cleaned_data.get('card_id')).exists():
                self._errors['card_id'] = self.error_class(['Card not found'])

        # phone_empty = cleaned_data['phone_recipients'] is None or cleaned_data['phone_recipients'] != ''
        # email_empty = cleaned_data['email_recipients'] is None or cleaned_data['email_recipients'] != ''
        #
        # if phone_empty is False and email_empty is False and cleaned_data['csv'] is None:
        #     self._errors['recipients'] = self.error_class(['You must include at least one of phone_recipients, '
        #                                                    'email_recipients or csv file'])
        return cleaned_data


class DefaultSectionForm(Form):
    """
    default section form
    """
    slug = CharField(required=True)


class SMSForm(Form):
    """
    sms form
    """
    sender = CharField(required=True)
    recipients = CommaSeperatedStringField(required=True)
    message = CharField(required=True)
