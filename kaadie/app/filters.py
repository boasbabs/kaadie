import django_filters
from django.db.models import Q
from django.contrib import admin

from .models import Card, Category, Section


class M2MFilter(django_filters.Filter):

    def filter(self, qs, value):
        if not value:
            return qs

        filters = []
        values = value.split(',')
        for v in values:
            _v = v.strip()
            section = Section.objects.filter(slug=_v.lower()).first()

            if section:
                filters += [Q(categories__slug__contains=c.slug) for c in section.categories.all()]
            else:
                filters += [Q(categories__slug__contains=_v)]
                # qs = qs.filter(categories__slug__contains=v)

        q = reduce(lambda x, y: x | y, filters, Q())

        qs = qs.filter(q)

        return qs


class CardFilter(django_filters.FilterSet):
    categories = M2MFilter(name='categories')
    sections = M2MFilter(name='sections')

    class Meta:
        model = Card
        fields = ('categories',)

