from django.contrib.auth.models import User
from django.core import serializers

# from django_celery_beat.models import PeriodicTask
from rest_framework.serializers import Serializer, ModelSerializer, CharField, ModelField, SerializerMethodField, \
    IntegerField, URLField, SlugRelatedField, BooleanField, FloatField

from .models import Card, Category, Message, Contact, Account, Address, SavedCard, ReceivedCard, SentCard, Section, \
    Comments, Settings, Artist, Banner, Notifications, Organization


class CustomModelField(ModelField):
    def get_attribute(self, obj):
        # We pass the object instance onto `to_representation`,
        # not just the field attribute.
        return obj

    def to_representation(self, obj):
        """
        Serialize the object's class name.
        """
        return serializers.serialize('json', [obj])


class CommentSerializer(ModelSerializer):
    """
    comments serializer
    """
    poster = CharField(required=True)
    poster_address = CharField(required=True)
    comment = CharField(required=True)
    received_card_id = IntegerField(write_only=True)
    sent_card_id = IntegerField(write_only=True)

    class Meta:
        model = Comments
        fields = ('date_created', 'date_modified', 'poster', 'poster_address', 'received_card_id', 'sent_card_id', 'comment')
        read_only_fields = ['date_created', 'date_modified', 'poster', 'poster_address', 'comment']

    def create(self, validated_data):
        return self.model.objects.create(**validated_data)


class AccountSerializer(ModelSerializer):
    """
    JSON serializer for card model
    """
    address = SerializerMethodField()
    user = SerializerMethodField()
    user_id = IntegerField()
    image_url = CharField(required=False)
    is_premium = BooleanField(required=False, read_only=True)

    class Meta:
        model = Account
        fields = ('image_url', 'date_created', 'date_modified', 'user', 'address', 'user_id', 'is_premium')
        read_only_fields = ['date_created', 'date_modified', 'user', 'address', 'is_premium']

    def get_address(self, obj):
        return {
            'id': obj.address.pk,
            'first_name': obj.address.first_name,
            'last_name': obj.address.last_name,
            'full_name': obj.address.full_name,
            'phone': obj.address.phone,
            'address': obj.address.address,
            'city': obj.address.city,
            'country': obj.address.country
        }

    def get_user(self, obj):
        return {
            'id': obj.user.pk,
            'email': obj.user.email,
            'username': obj.user.username
        }

    def create(self, validated_data):
        return self.model.objects.create(**validated_data)

    def update(self, instance, validated_data):
        # instance.address = validated_data.get('address', instance.address)
        # instance.user = validated_data.get('user', instance.user)
        instance.image_url = validated_data.get('image_url', instance.image_url)
        instance.save()

        return instance


class CardSerializer(ModelSerializer):
    """
    JSON serializer for card model
    """
    pk = IntegerField(read_only=True)
    title = CharField(required=True)
    description = CharField(required=False)
    user = CustomModelField(required=False, model_field=Card()._meta.get_field('user'), write_only=True)
    # user = IntegerField(required=True)
    image_url = URLField(required=True)
    slug = URLField(read_only=True)
    user_id = IntegerField(read_only=True, required=False)
    is_active = BooleanField(read_only=True)
    is_featured = BooleanField(read_only=True)
    is_public = BooleanField(read_only=True)
    price = FloatField(required=False)
    categories = SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='slug'
    )

    def get_account(self, obj):
        return {
            'id': obj.user.pk,
            'email': obj.user.email,
            'name': obj.user.name
            # 'username': obj.user.username
        }

    class Meta:
        model = Card
        fields = ('pk', 'date_created', 'date_modified', 'title', 'description', 'user', 'image_url', 'slug',
                  'user_id', 'categories', 'is_public', 'is_featured', 'is_active', 'price')
        read_only_fields = ['pk', 'date_created', 'date_modified', 'slug', 'user_id', 'categories', 'is_public',
                            'is_featured', 'is_active']

    def create(self, validated_data):
        return self.model.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.user = validated_data.get('user', instance.user)
        instance.image_url = validated_data.get('image_url', instance.image_url)
        instance.save()

        return instance


class SavedCardSerializer(ModelSerializer):
    """
    JSON serializer for card model
    """
    # user = CustomModelField(required=True, model_field=Card()._meta.get_field('user'), write_only=True)
    # card = CustomModelField(required=True, model_field=SavedCard()._meta.get_field('card'), write_only=True)
    card = CardSerializer(many=False, read_only=True)
    # user = AccountSerializer(many=False, read_only=True)
    count = IntegerField(read_only=True)
    card_id = IntegerField(read_only=False, required=True, write_only=True)
    user_id = IntegerField(read_only=True)

    # card_object = SerializerMethodField(read_only=True)

    def get_card_object(self, obj):
        """
        json serialized form for card
        :return:
        """
        return {
            'pk': obj.card.pk,
            'title': obj.card.title,
            'description': obj.card.description,
            'sent_counts': obj.card.sent_counts,
            'saved_counts': obj.card.saved_counts,
            'shared_counts': obj.card.shared_count,
            'image_url': obj.card.image_url
        }

    class Meta:
        model = SavedCard
        fields = ('date_created', 'date_modified', 'card', 'pk', 'card_id', 'count', 'user_id')
        read_only_fields = ['date_created', 'date_modified', 'pk', 'card_id', 'count', 'user_id']

    def create(self, validated_data):
        return self.model.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.user = validated_data.get('user', instance.user)
        instance.card = validated_data.get('card', instance.card)
        instance.save()

        return instance


class SentCardSerializer(ModelSerializer):
    """
    JSON serializer for card model
    """
    # user = CustomModelField(required=True, model_field=Card()._meta.get_field('user'), write_only=True)
    # card = CustomModelField(required=True, model_field=SavedCard()._meta.get_field('card'), write_only=True)
    message = CharField(read_only=True)
    recipients = CharField(read_only=True)
    image_url = CharField(read_only=True)
    card_id = IntegerField(read_only=True, required=False)
    user_id = IntegerField(read_only=True, required=False)
    card = CardSerializer(many=False, read_only=True)
    comments = CommentSerializer(many=True, read_only=True)

    class Meta:
        model = SentCard
        fields = (
            'date_created', 'date_modified', 'card', 'pk', 'recipients', 'message', 'image_url',
            'notification_id', 'card_id', 'user_id', 'comments')
        read_only_fields = ['date_created', 'date_modified', 'pk', 'message', 'recipients', 'image_url',
                            'notification_id', 'user_id', 'card_id']

    def create(self, validated_data):
        return self.model.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.user = validated_data.get('user', instance.user)
        instance.card = validated_data.get('card', instance.card)
        instance.save()

        return instance


class ReceivedCardSerializer(ModelSerializer):
    """
    JSON serializer for card model
    """
    notification_id = CharField(read_only=True)
    # card = SerializerMethodField(read_only=True)
    # sent_card = SerializerMethodField(read_only=True)
    message = CharField(read_only=True)
    image_url = URLField(read_only=True)
    recipient = CharField(read_only=True)
    card_id = IntegerField(read_only=True, required=False)
    user_id = IntegerField(read_only=True, required=False)

    def get_card(self, obj):
        """
        json serialized form for card
        :return:
        """
        return {
            'pk': obj.card.pk,
            'title': obj.card.title,
            'description': obj.card.description,
            'sent_counts': obj.card.sent_counts,
            'saved_counts': obj.card.saved_counts,
            'shared_counts': obj.card.shared_count,
            'image_url': obj.card.image_url
        }

    class Meta:
        model = ReceivedCard
        fields = ('date_created', 'date_modified', 'card', 'pk', 'message', 'image_url', 'notification_id',
                  'recipient', 'user_id', 'card_id')
        read_only_fields = ['date_created', 'date_modified', 'pk', 'message',
                            'notification_id', 'user_id', 'card_id']


class CategorySerializer(ModelSerializer):
    """
    JSON serializer for category model
    """
    title = CharField(required=True)
    description = CharField(required=False)
    slug = CharField(required=True)
    section_id = IntegerField(read_only=True, required=False)
    is_active = BooleanField(read_only=True)
    cards = CardSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ('date_created', 'date_modified', 'title', 'description', 'slug', 'section_id', 'is_active', 'cards')
        read_only_fields = ['date_created', 'date_modified', 'section_id', 'is_active', 'cards']

    def create(self, validated_data):
        return self.model.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.slug = validated_data.get('slug', instance.slug)
        instance.save()

        return instance


class SectionSerializer(ModelSerializer):
    """
    JSON serializer for category model
    """
    pk = IntegerField(read_only=True)
    title = CharField(required=True)
    description = CharField(required=False)
    slug = CharField(required=True)
    categories = CategorySerializer(many=True, read_only=True)
    permissible_organizations = SlugRelatedField(many=True, read_only=True, slug_field='code')
    position = CharField(read_only=True)

    class Meta:
        model = Section
        fields = ('pk', 'date_created', 'date_modified', 'title', 'description', 'slug', 'categories',
                  'permissible_organizations', 'position')
        read_only_fields = ['date_created', 'date_modified', 'categories', 'permissible_organizations', 'pk']

    def create(self, validated_data):
        return self.model.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.slug = validated_data.get('slug', instance.slug)
        instance.save()

        return instance


class ContactSerializer(ModelSerializer):
    """
    JSON serializer for card model
    """
    user_id = IntegerField()
    email = CharField()
    pk = IntegerField()
    address = SerializerMethodField()
    user = SerializerMethodField()

    class Meta:
        model = Contact
        fields = ('date_created', 'date_modified', 'user_id', 'email', 'pk', 'address', 'user')
        read_only_fields = ['date_created', 'date_modified', 'pk']

    def get_user(self, obj):
        return {
            # 'id': obj.user.pk,
            'email': obj.user.email
        }

    def get_address(self, obj):
        return {
            # 'id': obj.address.pk,
            'first_name': obj.address.first_name,
            'last_name': obj.address.last_name,
            'full_name': obj.address.full_name,
            'phone': obj.address.phone,
            'address': obj.address.address,
            'city': obj.address.city,
            'country': obj.address.country,
            'date_of_birth': obj.address.date_of_birth
        }

    def create(self, validated_data):
        return self.model.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.address = validated_data.get('address', instance.address)
        instance.user = validated_data.get('user', instance.user)
        instance.save()

        return instance


class AddressSerializer(ModelSerializer):
    """
    JSON serializer for card model
    """
    first_name = CharField(required=True)
    last_name = CharField(required=False)
    full_name = CharField(required=True)
    phone = CharField(required=False)
    address = CharField(required=False)
    city = CharField(required=False)
    country = CharField(required=False)

    class Meta:
        model = Address
        fields = (
            'date_created', 'date_modified', 'first_name', 'last_name', 'full_name', 'phone', 'address', 'city',
            'country')
        read_only_fields = ['date_created', 'date_modified']

    def create(self, validated_data):
        return self.model.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.full_name = validated_data.get('full_name', instance.full_name)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.address = validated_data.get('address', instance.address)
        instance.city = validated_data.get('city', instance.city)
        instance.country = validated_data.get('country', instance.country)
        instance.save()

        return instance


class ArtistSerializer(ModelSerializer):
    """
    JSON serializer for artist model
    """
    class Meta:
        model = Artist
        fields = (
            'date_created', 'date_modified', 'account', 'portfolio', 'is_active')

    account = AccountSerializer(many=False, read_only=True)
    portfolio = URLField(required=True)
    is_active = BooleanField(default=False)


class BannerSerializer(ModelSerializer):
    """
    JSON serializer for banner model
    """
    class Meta:
        model = Banner
        fields = ('title', 'link', 'image_url', 'pk', 'is_active')

    pk = IntegerField(read_only=True)
    title = CharField(required=True)
    image_url = URLField(required=True)
    link = URLField(required=False)
    is_active = BooleanField()


class SettingsSerializer(ModelSerializer):
    """
    JSON serializer for account preferences
    """
    # account = AccountSerializer(many=False, read_only=True)
    default_sections  = SectionSerializer(many=True, read_only=True)

    class Meta:
        model = Settings
        fields = ('default_sections',)


class NotificationSerializer(ModelSerializer):
    """
    JSON Serializer for notifications
    """
    class Meta:
        model = Notifications
        fields = ('body', 'link', 'is_read', 'event', 'pk')


class OrganizationSerializer(ModelSerializer):
    """
    JSON serializer for organization model
    """
    class Meta:
        model = Organization
        fields = ('pk', 'date_created', 'date_modified', 'name', 'code', 'logo', 'primary_color', 'primary_background_color',
                  'color', 'primary_color_class', 'menu_color', 'semantic_class', 'account', 'sms_api_url',
                  'email_api_url', 'account_id')


# class PeriodicTaskSerializer(ModelSerializer):
#     """
#     JSON serializer for periodic tasks
#     """
#     class Meta:
#         model = PeriodicTask