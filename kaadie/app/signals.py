#signals.py

from django.dispatch import Signal
# new_contact = Signal(providing_args=["emails", "user", "phones"])
save_thread = Signal(providing_args=["user", "sent_card", "emails", "phones"])
new_user_registered = Signal(providing_args=["account", "request"])
record_referral = Signal(providing_args=['account_id', 'referral_code'])