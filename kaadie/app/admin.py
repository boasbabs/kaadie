import inspect, sys

from django.contrib import admin
from import_export import resources, fields
from import_export.admin import ImportExportModelAdmin, ExportActionModelAdmin

from rest_framework.authtoken.admin import TokenAdmin

from . import models
from .forms import CategoryForm, BannerForm, SectionForm, CardForm, OrganizationForm, AdminRequestForm


TokenAdmin.raw_id_fields = ('user',)


class AdminSchema(object):
    @classmethod
    def create_schema(cls, model_object, form_class=None, readonly=(), list_filters=(), exclude=(), model_fields=()):
        """
        create schema for model object
        :param model_object:
        :param form_class:
        :param readonly:
        :param list_filters:
        :param exclude
        :param model_fields
        :return:
        """

        class BaseSchema(ImportExportModelAdmin):
            _fields = model_fields if len(model_fields) else model_object.fields()
            _fields = filter(lambda x: x not in exclude, _fields)
            resource_class = ResourceFactory.create_resource(getattr(models, model_object._meta.object_name), exclude,
                                                             _fields)
            list_display = model_object.fields()
            list_filter = list_filters
            readonly_fields = ('date_created', 'date_modified') + readonly
            date_hierarchy = 'date_created'
            empty_value_display = '-----'
            __name__ = '%sAdmin' % model_object.__name__
            if form_class:
                form = form_class

        admin.site.register(table, BaseSchema)

        return BaseSchema

    # @classmethod
    # def create_admin(cls, model_object):
    #
    #     class BaseAdmin(ImportExportModelAdmin):
    #         # import export class
    #         resource_class = ResourceFactory.create_resource(getattr(models, model_object._meta.object_name))
    #
    #     return BaseAdmin


class ResourceFactory(object):

    @classmethod
    def create_resource(cls, model_name, exclude, model_fields):

        __MODEL_NAME__ = model_name
        __EXCLUDE__ = exclude
        __FIELDS__ = model_fields

        class BaseResource(resources.ModelResource):
            if __MODEL_NAME__._meta.model_name:
                email = fields.Field(attribute='email')
                phone = fields.Field(attribute='phone')
                name = fields.Field(attribute='name')

            class Meta:
                exclude = __EXCLUDE__
                model = __MODEL_NAME__
                if len(model_fields):
                    fields = __FIELDS__

        return BaseResource


tables = filter(lambda x: x._meta.abstract is False, [getattr(models, m[0]) for m in
                                                      inspect.getmembers(models, inspect.isclass) if m[1].__module__
                                                      == 'app.models'])

for table in tables:
    form_class = None
    readonly = ()
    list_filter = list()
    model_fields = list()
    exclude = list()
    if table._meta.verbose_name == 'category':
        form_class = CategoryForm
    elif table._meta.verbose_name == 'banner':
        form_class = BannerForm
    elif table._meta.verbose_name == 'section':
        form_class = SectionForm
    elif table._meta.verbose_name == 'card':
        list_filter = ('is_featured', 'is_active', 'is_public')
        form_class = CardForm
        readonly = ('sent_count', 'saved_count', 'shared_count')
    elif table._meta.verbose_name == 'organization':
        form_class = OrganizationForm
    elif table._meta.verbose_name == 'account':
        model_fields = ('date_created', 'phone', 'name', 'email')
    elif table._meta.verbose_name == 'artist request':
        exclude = ('is_closed',)
        form_class = AdminRequestForm
    AdminSchema.create_schema(table, form_class, readonly, list_filter, model_fields=model_fields, exclude=exclude)


