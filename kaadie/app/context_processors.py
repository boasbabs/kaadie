from __future__ import unicode_literals

from django_user_agents.utils import get_user_agent
from django.contrib import messages
from django.conf import settings

from .services import helper, cache
from . import models


def main_context(request, *args, **kwargs):
    """
    main context available in all templates
    """
    sub = kwargs.get('sub')
    organization = helper.get_organization(sub)

    account = request.user.account if hasattr(request.user, 'account') else None
    ua = get_user_agent(request)
    storage = messages.get_messages(request)
    artist = None
    setting = None
    saved_cards = list()

    if account and hasattr(account, 'artist'):
        artist = getattr(account, 'artist')

    artist_request = None
    if request.user.is_authenticated():
        artist_request = models.ArtistRequest.objects.filter(email=request.user.email, is_closed=False).first()

    try:
        is_db = False
        sections = cache.fetch_cache_sections(organization.code)
        if account:
            saved_cards = cache.fetch_cache_saved_cards(account.pk)
    except Exception, e:
        sections = models.Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
            'position').all()
        is_db = True
        if account:
            saved_cards = [c.card for c in request.user.account.saved_cards.all()]

    saved_ids = ','.join([str(c.pk) for c in saved_cards]) if len(saved_cards) > 0 else ''
    referral_url = ''
    if request.user.is_authenticated():
        if not hasattr(account, 'setting'):
            models.Settings.objects.create(account=account, referral_code=helper.generate_referral_code())
        setting = account.setting
        referral_url = '%s/register?code=%s' % (settings.DOMAIN, setting.referral_code)

    return dict(account=account, organization=organization, ua=ua, storage=storage, artist=artist, saved_ids=saved_ids,
                artist_request=artist_request, saved_cards=saved_cards, sections=sections, is_db=is_db, setting=setting,
                referral_url=referral_url, forum_url=settings.FORUM_URL)