from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0014_auto_20170922_1651'),
    ]

    operations = [
        migrations.AddField(
            model_name='receivedcard',
            name='cover_image_url',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
    ]