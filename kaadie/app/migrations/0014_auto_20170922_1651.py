# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-09-22 16:51
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0013_auto_20170922_1651'),
    ]

    operations = [
        migrations.AddField(
            model_name='artistrequest',
            name='email',
            field=models.EmailField(default=1, max_length=254),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='artistrequest',
            name='is_accepted',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='artistrequest',
            name='is_closed',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='artistrequest',
            name='nickname',
            field=models.TextField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='artistrequest',
            name='portfolio',
            field=models.URLField(default=1),
            preserve_default=False,
        ),
    ]
