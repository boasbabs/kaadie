# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-10-11 15:46
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone
import s3direct.fields


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0016_auto_20171009_1452'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArtistBanner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now)),
                ('date_modified', models.DateTimeField(default=django.utils.timezone.now)),
                ('title', models.CharField(blank=True, max_length=255, null=True)),
                ('image_url', s3direct.fields.S3DirectField(null=True)),
                ('link', models.URLField(blank=True, null=True)),
                ('slug', models.SlugField(editable=False)),
                ('is_active', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name_plural': 'banners',
            },
        ),
    ]
