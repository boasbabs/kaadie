# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0015_auto_20171006_1652'),
    ]

    operations = [
        migrations.AlterField(
            model_name='receivedcard',
            name='cover_image_url',
            field=models.TextField(null=True, blank=True),
        ),
    ]
