import time, os
import urlparse
from datetime import datetime
import yaml
from bson import json_util
import json

from django.core.files.storage import FileSystemStorage
from django.db.models import Q
from django.contrib.auth import authenticate
from django.conf import settings
from django.utils.crypto import get_random_string

from rest_framework_expiring_authtoken.models import ExpiringToken
from rest_framework import status, response, views as rest_views, permissions, schemas, compat
from .signals import new_user_registered
from .serializers import AccountSerializer, CommentSerializer, SettingsSerializer
import forms
import models, serializers
from services import helper, transport, encoders


class SchemaGenerator(schemas.SchemaGenerator):
    def get_link(self, path, method, callback, view):
        """Custom the coreapi using the func.__doc__ .

        if __doc__ of the function exsit, use the __doc__ building the coreapi. else use the default serializer.

        __doc__ in yaml format, eg:

        desc: the desc of this api.
        ret: when success invoked, return xxxx
        err: when error occured, return xxxx
        input:
        - name: mobile
          desc: the mobile number
          type: string
          required: true
          location: form
        - name: promotion
          desc: the activity id
          type: int
          required: true
          location: form
        """
        fields = self.get_path_fields(path, method, callback, view)
        yaml_doc = None
        func = getattr(view, view.action) if getattr(view, 'action', None) else None
        if func and func.__doc__:
            try:
                yaml_doc = yaml.load(func.__doc__)
            except:
                yaml_doc = None
        if yaml_doc and 'desc' in yaml_doc:
            desc = yaml_doc.get('desc', '')
            ret = yaml_doc.get('ret', '')
            err = yaml_doc.get('err', '')
            _method_desc = desc + '<br>' + 'return: ' + ret + '<br>' + 'error: ' + err
            params = yaml_doc.get('input', [])
            for i in params:
                _name = i.get('name')
                _desc = i.get('desc')
                _required = i.get('required', True)
                _type = i.get('type', 'string')
                _location = i.get('location', 'form')
                field = compat.coreapi.Field(
                    name=_name,
                    location=_location,
                    required=_required,
                    description=_desc,
                    type=_type
                )
                fields.append(field)
        else:
            _method_desc = func.__doc__ if func and func.__doc__ else ''
            fields += self.get_serializer_fields(path, method, callback, view)
        fields += self.get_pagination_fields(path, method, callback, view)
        fields += self.get_filter_fields(path, method, callback, view)

        if fields and any([field.location in ('form', 'body') for field in fields]):
            encoding = self.get_encoding(path, method, callback, view)
        else:
            encoding = None

        if self.url and path.startswith('/'):
            path = path[1:]

        return compat.coreapi.Link(
            url=urlparse.urljoin(self.url, path),
            action=method.lower(),
            encoding=encoding,
            fields=fields,
            description=_method_desc
        )


class LoginAPIView(rest_views.APIView):
    """
    This text is the description for this API
    param1 -- A first parameter
    param2 -- A second parameter
    """
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        """
        Attempts to login user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.LoginForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['email']
            password = form.cleaned_data['password']

            user = models.User.objects.filter(Q(username=username) | Q(email=username)).first()
            if user and authenticate(username=username, password=password):
                token = ExpiringToken.objects.filter(user_id=user.pk).first()

                if token:
                    token.delete()

                token = ExpiringToken.objects.create(user=user)

                return response.Response({'token': token.key, 'message': 'successfully logged in'},
                                         status=status.HTTP_200_OK)

            return response.Response({'message': 'Invalid username/password combination'},
                                     status=status.HTTP_401_UNAUTHORIZED)

        return response.Response(form.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class RegisterAPIView(rest_views.APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        """
        Attempts to login user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.ApiRegForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            name = form.cleaned_data.get('name')
            _name = str(name).split()
            first_name = _name[0]
            last_name = ''.join(_name[1:])
            image_url = form.cleaned_data.get('image_url')

            user = models.User.objects.filter(username=username).first()

            if user:
                return response.Response({'message': 'User already exists'}, status=status.HTTP_400_BAD_REQUEST)

            user = models.User.objects.create_user(username=username, email=username, password=password,
                                                   first_name=first_name, last_name=last_name)

            account = models.Account.objects.filter(user=user.id).first()
            account.image_url = image_url
            account.source = 'mobile'
            account.save()

            # send message to user
            new_user_registered.send(sender=None, account=account, request=request)

            token = ExpiringToken.objects.filter(user_id=user.pk).first()
            if not token or token.expired():
                token = ExpiringToken.objects.create(user=user)
            return response.Response({'token': token.key, 'message': 'account successfully created'},
                                     status=status.HTTP_201_CREATED)

        return response.Response(form.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class BrandRegisterAPIView(rest_views.APIView):
    """
        A view that can accept POST requests with JSON content.
    """

    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        """
        Attempts to login user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.BrandRegistrationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            name = form.cleaned_data.get('name')
            _name = str(name).split()
            first_name = _name[0]
            last_name = ''.join(_name[1:])
            image_url = form.cleaned_data.get('image_url')
            brand_name = form.cleaned_data.get('brand_name')
            logo = form.cleaned_data.get('logo')

            user = models.User.objects.filter(username=username).first()

            if user:
                return response.Response({'message': 'User already exists'}, status=status.HTTP_400_BAD_REQUEST)

            org = models.Organization.objects.filter(name=brand_name).first()

            if org:
                return response.Response({'message': 'Brand with this name already exists'},
                                         status=status.HTTP_400_BAD_REQUEST)

            user = models.User.objects.create_user(username=username, email=username, password=password,
                                                   first_name=first_name, last_name=last_name)

            account = models.Account.objects.filter(user=user.id).first()
            account.image_url = image_url
            account.source = 'mobile'
            account.save()

            # send message to user
            # new_user_registered.send(sender=None, account=account, request=request)

            models.Organization.objects.create(name=brand_name, logo=logo, account=account)

            token = ExpiringToken.objects.filter(user_id=user.pk).first()
            if not token or token.expired():
                token = ExpiringToken.objects.create(user=user)
            return response.Response({'token': token.key, 'message': 'account successfully created'},
                                     status=status.HTTP_201_CREATED)

        return response.Response(form.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class LogoutAPIView(rest_views.APIView):
    def post(self, request, *args, **kwargs):
        """
        Attempts to logout user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            user = request.user
            token = ExpiringToken.objects.get(user=user)
            token.delete()
            return response.Response({'message': 'success'}, status=status.HTTP_200_OK)
        except Exception, e:
            return response.Response(e, status=status.HTTP_400_BAD_REQUEST)


class ForgotPasswordAPIView(rest_views.APIView):
    """
        A view that can accept POST requests with JSON content.
    """

    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        """
        Attempts to login user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.ForgotPasswordForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('email')

            user = models.User.objects.filter(username=username).first()

            if not user:
                return response.Response({'message': 'No account with this email address exists'},
                                         status=status.HTTP_400_BAD_REQUEST)

            token = models.AuthorizationToken.objects.filter(user=user.account, is_verification=False,
                                                             is_password_change=True, is_expired=False).first()

            if not token:
                token = models.AuthorizationToken.objects.create(code=get_random_string(6), user=user.account,
                                                                 is_verification=False, is_password_change=True)

            # send welcome email to new user
            reset_message = "<p>Hello {},</p>" \
                            "<p> You have requested a password reset to access your account. " \
                            "If you didn't request a password change, " \
                            "ignore this message. </p><p>To reset your password, use this token <strong>{}</strong></p>" \
                            "<p>Best Regards,<br />Kaadie</p>".format(user.account.name, token.code)

            transport.MailService.send_mail('Password Reset', 'system@kaadie.com', 'Kaadie', user.account.email,
                                            reset_message, user.account.name)

            return response.Response(
                {'message': 'Reset password instructions has been sent to the email address attached to this account'},
                status=status.HTTP_200_OK)

        return response.Response(form.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class ResetPasswordAPIView(rest_views.APIView):
    """
        A view that can accept POST requests with JSON content.
    """

    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        """
        Attempts to login user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.ResetPasswordForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('email')
            key = form.cleaned_data.get('token')
            password = form.cleaned_data.get('password')

            user = models.User.objects.filter(username=username).first()

            if not user:
                return response.Response({'message': 'No account with this email address exists'},
                                         status=status.HTTP_400_BAD_REQUEST)

            token = models.AuthorizationToken.objects.filter(user=user.account, is_verification=False,
                                                             is_password_change=True, is_expired=False,
                                                             code=key).first()

            if not token:
                return response.Response({'message': 'Invalid reset code'},
                                         status=status.HTTP_400_BAD_REQUEST)

            user.set_password(password)
            user.save()

            token.is_expired = True
            token.save()

            return response.Response({'message': 'Your password has been reset'}, status=status.HTTP_200_OK)

        return response.Response(form.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class ProfileAPIView(rest_views.APIView):
    def get(self, request):
        """
        Retrieve User profile information
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            user = request.user
            account = AccountSerializer(user.account)
            data = account.data

            setting = models.Settings.objects.filter(account=user.account.pk).first()

            if setting:
                data['default_sections'] = SettingsSerializer(setting).data['default_sections']

            return response.Response(data, status=status.HTTP_200_OK)
        except Exception, e:
            return response.Response(e, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, *args, **kwargs):
        """
        Update profile information
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        account = request.user.account

        form = forms.ProfileForm(request.POST)

        if form.is_valid():
            data = dict(first_name=request.POST.get('first_name', account.address.first_name),
                        last_name=request.POST.get('last_name', account.address.last_name),
                        phone=request.POST.get('phone', account.address.phone),
                        city=request.POST.get('city', account.address.city),
                        country=request.POST.get('country', account.address.country),
                        address=request.POST.get('address', account.address.address),
                        image_url=request.POST.get('image_url', account.image_url),
                        gender=request.POST.get('gender', account.gender),
                        language=request.POST.get('language', account.language)
                        )

            birthday = request.POST.get('birthday', None)

            if birthday:
                data['date_of_birth'] = datetime.strptime(birthday, '%Y-%m-%d')

            address = helper.set_attributes(account.address, **data)

            account.address = address
            account.address.full_name = '%s %s' % (account.address.first_name, account.address.last_name)
            account.address.save()

            account.user.first_name = data['first_name']
            account.user.last_name = data['last_name']
            account.user.save()

            account.image_url = data['image_url']
            account.gender = data['gender']
            account.language = data['language']
            account.save()

            account = AccountSerializer(account)
            return response.Response(account.data,
                                     status=status.HTTP_200_OK)

        return response.Response(form.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class SendCardAPIView(rest_views.APIView):
    def post(self, request, *args, **kwargs):
        """
        Update profile information
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        form = forms.SendCardForm(request.POST)

        if form.is_valid():

            file_path = None
            if request.FILES.get('csv'):
                _file = request.FILES['csv']
                fs = FileSystemStorage()
                filename = fs.save('uploads/%s_%s' % (time.time(), _file.name), _file)
                file_path = os.path.join(settings.MEDIA_ROOT, filename)

                if not helper.validate_file_uploaded(file_path):
                    return response.Response({'message': 'Invalid File Format'},
                                             status=status.HTTP_400_BAD_REQUEST)

            card = models.Card.objects.get(pk=form.cleaned_data.get('card_id'))
            # pub = transport.publish_message('app.helper', 'send_card',
            #                                 **{'card_id': card.pk, 'user_id': request.user.account.pk,
            #                                    'email_recipients': form.cleaned_data.get('email_recipients'),
            #                                    'phone_recipients': form.cleaned_data.get('phone_recipients'),
            #                                    'personalized_message': form.cleaned_data.get('message'),
            #                                    'sender_name': form.cleaned_data.get('sender_name'),
            #                                    'sender': request.user.email, 'domain': 'kaadie.com',
            #                                    'card_image': form.cleaned_data.get('card_image'),
            #                                    'file_path': file_path})

            emails = filter(lambda x: x != '', form.cleaned_data.get('email_recipients').split(',')) or []

            published = transport.send_card.delay(card.pk, request.user.account.pk, emails, [],
                                            form.cleaned_data.get('message'),
                                            form.cleaned_data.get('sender_name'),
                                            request.user.email, 'kaadie.com',
                                            card_image=form.cleaned_data.get('card_image'),
                                            file_path=file_path)

            if published:
                return response.Response({'message': 'task queued'},
                                         status=status.HTTP_202_ACCEPTED)

            return response.Response(form.errors,
                                     status=status.HTTP_400_BAD_REQUEST)

        return response.Response(form.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class PremiumAPIView(rest_views.APIView):
    def post(self, request, *args, **kwargs):
        """
        Update profile information
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        account = models.Account.objects.get(pk=request.user.account.pk)

        if account.is_premium:
            return response.Response({'message': 'Already a premium user'},
                                     status=status.HTTP_400_BAD_REQUEST)

        account.is_premium = True
        account.save()

        _account = AccountSerializer(account)

        return response.Response(_account.data,
                                 status=status.HTTP_200_OK)


class ActivityAPIView(rest_views.APIView):
    def get(self, request, *args, **kwargs):
        """
        retrieve activity logs for a user
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        data = helper.retrieve_logs(request.user.email)

        return encoders.JSONResponse(data, status=status.HTTP_200_OK)


class CommentAPIView(rest_views.APIView):
    """
    api resource endpoints for comments
    """

    def get(self, request, pk, **kwargs):
        """
        retrieve comments with sent_card_id matching pk
        :param request:
        :param pk: sent card id
        :param kwargs:
        :return:
        """
        comments = models.Comments.objects.filter(received_card_id=pk)

        return response.Response({
            'count': comments.count(),
            'results': comments.all()
        })

    def post(self, request, pk, **kwargs):
        """
        post comment
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.CommentForm(request.POST)

        if form.is_valid():

            received_card = models.ReceivedCard.objects.filter(pk=pk).first()

            if not received_card:
                return response.Response("No received card matching this ID - %s" % pk,
                                         status=status.HTTP_400_BAD_REQUEST)

            user = request.user
            data = form.cleaned_data.copy()
            data['poster'] = user.account.name
            data['poster_address'] = user.email
            data['sent_card_id'] = received_card.sent_card.pk
            data['received_card_id'] = received_card.pk
            comment = models.Comments.objects.create(**data)
            comment_serializer = CommentSerializer(comment)

            return response.Response(comment_serializer.data, status=status.HTTP_201_CREATED)

        return response.Response(form.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class AddSectionAPIView(rest_views.APIView):
    """
    Add a new default section for a user
    """

    def post(self, request, *args, **kwargs):
        """
        add a new default section
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.DefaultSectionForm(request.POST)

        if form.is_valid():
            slug = form.cleaned_data['slug']
            section = models.Section.objects.filter(slug=slug.lower()).first()

            if not section:
                return response.Response('No section found matching this slug - %s' % slug,
                                         status=status.HTTP_404_NOT_FOUND)

            user = request.user
            setting = models.Settings.objects.filter(account=user.account.pk).first()

            if not setting:
                setting = models.Settings.objects.create(account=user.account)
                default_sections = models.Section.objects.order_by('position')[:3]
                for sec in default_sections:
                    setting.default_sections.add(sec)
                    setting.save()

            if section in setting.default_sections.all():
                return response.Response({'message': 'Section with slug - %s already added to your defaults' % slug},
                                         status=status.HTTP_400_BAD_REQUEST)
            setting.default_sections.add(section)
            setting.save()

            account = AccountSerializer(user.account)
            data = account.data

            data['default_sections'] = SettingsSerializer(setting).data['default_sections']

            return response.Response(data, status=status.HTTP_200_OK)

        return response.Response(form.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class DeleteSectionAPIView(rest_views.APIView):
    """
    delete a default section for a user
    """

    def delete(self, request, slug):
        """
        add a new default section
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        section = models.Section.objects.filter(slug=slug.lower()).first()

        if not section:
            return response.Response({'message': 'No section found matching this slug - %s' % slug},
                                     status=status.HTTP_404_NOT_FOUND)

        user = request.user
        setting = models.Settings.objects.filter(account=user.account.pk).first()

        if not setting:
            setting = models.Settings.objects.create(account=user.account)
            default_sections = models.Section.objects.order_by('position')[:3]
            for sec in default_sections:
                setting.default_sections.add(sec)
                setting.save()

        if section not in setting.default_sections.all():
            return response.Response('Section with slug - %s not in your default sections' % section,
                                     status=status.HTTP_400_BAD_REQUEST)

        setting.default_sections.remove(section)
        setting.save()

        account = AccountSerializer(user.account)
        data = account.data

        data['default_sections'] = SettingsSerializer(setting).data['default_sections']

        return response.Response(data,
                                 status=status.HTTP_204_NO_CONTENT)


class SMSAPIView(rest_views.APIView):
    """
    sending sms api
    """

    def post(self, request, *args, **kwargs):
        """
        send sms
        :params request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.SMSForm(request.POST)

        if form.is_valid():
            errors = transport.send_via_twilio(form.cleaned_data['sender'], form.cleaned_data['body'],
                                               form.cleaned_data['recipients'])

            if len(errors) > 0:
                return response.Response({'status': 'error', 'errors': errors},
                                         status=status.HTTP_202_ACCEPTED)
            else:
                return response.Response({'status': 'success'},
                                         status=status.HTTP_200_OK)

        return response.Response(form.errors,
                                 status=status.HTTP_400_BAD_REQUEST)


class EmailAPIView(rest_views.APIView):
    """
    sending email api
    """

    def post(self, *args, **kwargs):
        """
        send sms
        :param args:
        :param kwargs:
        :return:
        """
        pass
