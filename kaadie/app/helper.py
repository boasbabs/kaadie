# from __future__ import absolute_import, unicode_literals
# import base64, time, urlparse
# from datetime import datetime, time as dt_time, date
# from urllib import urlretrieve
# import json, hashlib
# import logging, re, requests
# from mimetypes import guess_type
# import csv, codecs
# from bson import json_util, ObjectId
# import six, random
# from itertools import chain
#
# from django.contrib.auth.mixins import AccessMixin
# from django.http.response import HttpResponse
# from django.conf import settings
# from django.shortcuts import redirect
# from django.contrib.auth.models import User
# from django.db.models import Q
# from django.template.loader import render_to_string
# from django_user_agents.utils import get_user_agent
#
# from django_celery_beat.models import CrontabSchedule, PeriodicTask, PeriodicTasks, PERIOD_CHOICES
# from rest_framework.renderers import JSONRenderer
# from rest_framework.authentication import SessionAuthentication
# from rest_framework.compat import (
#     INDENT_SEPARATORS, LONG_SEPARATORS, SHORT_SEPARATORS, coreapi,
#     template_render
# )
# from sendgrid import *
# from sendgrid.helpers import mail as sendgrid_mail
# from twilio import TwilioRestException
# from twilio.rest import TwilioRestClient
# from boto3 import resource, client
# from pyshorteners import Shortener
# from xlrd import open_workbook, xldate_as_tuple
# from premailer import transform
# from pymongo import MongoClient
# from kafka import KafkaProducer
# from kafka.errors import KafkaError
# import msgpack
#
# # from celery import shared_task
# from .signals import save_thread
# from .models import SentCard, ReceivedCard, Card, Account, Organization, Contact, Address, Category, Settings, \
#     SystemMessage, Notifications, Section, Banner
# from kaadie import celery_app, es, mongo_client, pusher_client, redis_obj
# from . import serializers as app_serializers
#
# log = logging.getLogger('app')
#
#
# class DateTimeEncoder(json.JSONEncoder):
#     def default(self, o):
#         if isinstance(o, datetime):
#             return o.isoformat()
#
#         return json.JSONEncoder.default(self, o)
#
#
# class CsrfExemptSessionAuthentication(SessionAuthentication):
#     def enforce_csrf(self, request):
#         return
#
#
# class AnonymousMixin(AccessMixin):
#     """
#     Custom mixin to prevent access to routes by an authenticated user
#     """
#
#     def dispatch(self, request, *args, **kwargs):
#         if request.user.is_authenticated:
#             return redirect(settings.HOME_URL)
#         return super(AnonymousMixin, self).dispatch(request, *args, **kwargs)
#
#
# class AdminPanelMixin(AccessMixin):
#     """
#     Custom mixin to prevent access to routes by an authenticated user
#     """
#
#     def dispatch(self, request, *args, **kwargs):
#         if request.user.is_authenticated:
#             # organization = get_organization(request)
#             sub = kwargs.get('sub')
#             organization = get_organization(sub)
#             if organization.account_id != request.user.account.pk:
#                 return redirect(settings.HOME_URL)
#         return super(AdminPanelMixin, self).dispatch(request, *args, **kwargs)
#
#
# class PremiumMixin(AccessMixin):
#     """
#     Custom mixin to prevent access to routes authorized only for artists
#     """
#
#     def dispatch(self, request, *args, **kwargs):
#
#         if not request.user.is_authenticated:
#             return redirect(settings.LOGIN_URL)
#
#         if not request.user.is_staff and not request.user.account.is_premium:
#             return redirect(settings.HOME_URL)
#
#         return super(PremiumMixin, self).dispatch(request, *args, **kwargs)
#
#
# class ValidateEmailMixin(AccessMixin):
#     """
#     Custom mixin to force user to change email
#     """
#
#     def dispatch(self, request, *args, **kwargs):
#         if request.user.is_authenticated:
#             if not request.user.account.email_valid:
#                 return redirect(settings.VALIDATE_EMAIL_URL)
#         return super(ValidateEmailMixin, self).dispatch(request, *args, **kwargs)
#
#
# class JSONEncoder(json.JSONEncoder):
#     def default(self, o):
#         if isinstance(o, ObjectId):
#             return str(o)
#         return json.JSONEncoder.default(self, o)
#
#
# class CustomJSONRenderer(JSONRenderer):
#     """
#     update json renderer
#     """
#
#     def render(self, data, accepted_media_type=None, renderer_context=None):
#         """
#         Render `data` into JSON, returning a bytestring.
#         """
#         if data is None:
#             return bytes()
#
#         renderer_context = renderer_context or {}
#         indent = self.get_indent(accepted_media_type, renderer_context)
#
#         if indent is None:
#             separators = SHORT_SEPARATORS if self.compact else LONG_SEPARATORS
#         else:
#             separators = INDENT_SEPARATORS
#
#         ret = json.dumps(
#             data, cls=self.encoder_class,
#             indent=indent, ensure_ascii=self.ensure_ascii,
#             separators=separators, default=json_util.default
#         )
#
#         if isinstance(ret, six.text_type):
#             ret = ret.replace('\u2028', '\\u2028').replace('\u2029', '\\u2029')
#             return bytes(ret.encode('utf-8'))
#         return ret
#
#
# class JSONResponse(HttpResponse):
#     """
#     An HttpResponse that renders its content into JSON.
#     """
#
#     def __init__(self, data, **kwargs):
#         content = CustomJSONRenderer().render(data)
#         kwargs['content_type'] = 'application/json'
#         super(JSONResponse, self).__init__(content, **kwargs)
#
#
# class MailService(object):
#     @classmethod
#     def send_mail(cls, subject, sender, sender_name, recipient, body, recipient_name='', attachments=[], filepaths=[]):
#         """
#         send email via sendgrid
#         :param subject:
#         :param sender:
#         :param sender_name:
#         :param recipient:
#         :param recipient_name:
#         :param body:
#         :return: success/error response
#         """
#         _mail = sendgrid_mail.Mail()
#         _mail.set_from(Email(sender, sender_name))
#         _mail.set_subject(subject)
#
#         personalization = sendgrid_mail.Personalization()
#         personalization.add_to(Email(recipient, recipient_name))
#         _mail.add_personalization(personalization)
#         _mail.add_content(sendgrid_mail.Content("text/html", "<html><body>%s</body></html>" % body))
#
#         # for _attachment in attachments:
#         #     _mail.add_attachment(_attachment)
#
#         # try:
#         #     for _path in filepaths:
#         #         _file = open(_path)
#         #         encoded_string = base64.b64encode(_file.read())
#         #         attachment = sendgrid_mail.Attachment()
#         #         attachment.set_content(encoded_string)
#         #         attachment.set_type('image/jpeg')
#         #         attachment.set_disposition("attachment")
#         #         _mail.add_attachment(attachment)
#         # except Exception:
#         #     raise Exception('File not attached')
#
#         try:
#             sg = SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)
#             sg.client.mail.send.post(request_body=_mail.get())
#
#             log_message = '[MESSAGE: %s][RECIPIENTS: %s][STATUS: SUCCESS][SENDER: %s]' % (body, recipient, sender)
#             log.info(log_message)
#             log.debug(log_message)
#
#             return True
#         except:
#             log_message = '[MESSAGE: %s][RECIPIENTS: %s][STATUS: FAILED][SENDER: %s]' % (body, recipient, sender)
#             log.info(log_message)
#             log.debug(log_message)
#             return False
#
#     @classmethod
#     def send_card(cls, card_id, user_id, email_recipients, phone_recipients, personalized_message, sender_name, sender,
#                   domain, card_image=None, subject=None, tag='general', **kwargs):
#         """
#         send card to recipients
#         :param card_id:
#         :param user_id
#         :param email_recipients: email recipients
#         :param phone_recipients: sms recipients
#         :param message:
#         :param sender_name:
#         :param sender:
#         :param tag
#         :param kwargs:
#         :return:
#         """
#         card = Card.objects.get(pk=card_id)
#         if not subject:
#             subject = 'Hello, %s just sent you a kaadie e-card' % sender_name
#
#         recp = ','.join(phone_recipients + email_recipients)
#
#         user = Account.objects.get(pk=user_id)
#
#         _id = '%s_%s_%s' % (recp, sender, str(time.time()))
#         notification_id = hashlib.md5(_id).hexdigest()
#         link = shorten_url('%s/sent/%s' % (domain, notification_id))
#
#         image_url = card.image_url
#         if card_image:
#             title = '%s.jpg' % notification_id
#             uploaded = upload_b64_s3(card_image, title)
#             if uploaded:
#                 image_url = '%s/%s' % (settings.S3_BASE_ADDRESS, title)
#
#         sent_card = SentCard.objects.create(card=card, user=user, message=personalized_message,
#                                             notification_id=notification_id, image_url=image_url, recipients=recp)
#
#         # try:
#         cls.send_sms(sent_card.pk, phone_recipients, sender_name, domain=domain,
#                      personalized_message=personalized_message, tag=tag, link=link, notification_id=notification_id,
#                      card_image=card_image)
#         # _file = urlretrieve(card.image_url)
#         #
#         # image_file = open(_file[0], 'rb')
#         # filename = card.image_url.split('/')[-1]
#         # name = filename.split('.')[0]
#         # _type = filename.split('.')[1]
#
#         sg = SendGridAPIClient(apikey=settings.SENDGRID_API_KEY)
#
#         for rec in email_recipients:
#             if len(rec) != 0:
#                 _user = User.objects.filter(email=rec).first()
#                 name = _user.account.name if _user else ''
#
#                 content = build_html_message(sender_name, domain, link, image_url, tag, name)
#
#                 data = {
#                     # "attachments": [
#                     #     {
#                     #         "content": base64.b64encode(image_file.read()),
#                     #         "content_id": '%s_%s' % (card.slug, datetime.now().strftime('%Y-%m-%d %H:%s').replace(" ", "")),
#                     #         "disposition": "attachment",
#                     #         "filename": filename,
#                     #         "name": name,
#                     #         "type": _type
#                     #     }
#                     # ],
#                     "personalizations": [
#                         {
#                             "to": [
#                                 {
#                                     "email": rec
#                                 }
#                             ],
#                             "subject": subject
#                         }
#                     ],
#                     "from": {
#                         "email": sender,
#                         "name": sender_name
#                     },
#                     "content": [
#                         {
#                             "type": "text/html",
#                             "value": content
#                         }
#                     ]
#                 }
#
#                 sg.client.mail.send.post(request_body=data)
#
#                 # upload edited image to S3
#                 ReceivedCard.objects.create(user=sent_card.user, sent_card=sent_card, card=card,
#                                             message=personalized_message, recipient=rec,
#                                             notification_id=notification_id, image_url=image_url)
#
#                 if _user:
#                     trigger_notification.delay('You just received a card from %s' % user.name, link, [_user.pk])
#
#         return True
#         # except Exception, e:
#         #     return False
#
#     @staticmethod
#     def send_sms(sent_card_id, recipients, sender_name, domain, personalized_message, tag, link, notification_id,
#                  card_image=None, sender='+12074820335'):
#         """
#         send sms using twilio
#         :param recipients:
#         :param sent_card_id
#         :param sender:
#         :param sender_name
#         :param personalized_message
#         :param tag
#         :param link
#         :param notification_id
#         :param card_image
#         :return:
#         """
#         account_sid = settings.TWILIO_ACCOUNT_SID
#         auth_token = settings.TWILIO_ACCOUNT_TOKEN
#
#         client = TwilioRestClient(account_sid, auth_token)
#
#         try:
#             sent_card = SentCard.objects.get(pk=sent_card_id)
#             for recipient in recipients:
#                 if len(recipient) != 0:
#                     num = '+%s%s' % (settings.DEFAULT_COUNTRY_CODE, int(recipient))
#                     address = Address.objects.filter(Q(phone=recipient) | Q(phone=num)).first()
#                     account = None
#                     if address:
#                         account = Account.objects.filter(address=address.pk).first()
#
#                     name = account.name if account else ''
#
#                     body = build_sms_message(sender_name, domain, link, tag, name)
#
#                     if recipient.startswith(settings.DEFAULT_COUNTRY_CODE):
#                         num = '+%s' % int(recipient)
#                     elif recipient.startswith('+%s' % settings.DEFAULT_COUNTRY_CODE):
#                         num = recipient
#                     client.messages.create(body=body, to=num, from_=sender)
#                     image_url = None
#                     title = '%s.jpg' % notification_id
#                     uploaded = upload_b64_s3(card_image, title)
#                     if uploaded:
#                         image_url = '%s/%s' % (
#                             settings.S3_BASE_ADDRESS, title)
#
#                     ReceivedCard.objects.create(user=sent_card.user, sent_card=sent_card, card=sent_card.card,
#                                                 recipient=recipient, notification_id=notification_id,
#                                                 image_url=image_url, message=personalized_message)
#                     if account:
#                         trigger_notification.delay('You just received a card from %s' % sender_name, link, [account.pk])
#             return True
#         except TwilioRestException as e:
#             return False
#
#
# def anonymous_user(func, redirect_url=settings.HOME_URL):
#     """
#     redirects logged_in user
#     :param func:
#     :param redirect_url:
#     :return:
#     """
#
#     def decorated(request, *args, **kwargs):
#         """
#         redirects authenticated user
#         :param request:
#         :param args:
#         :param kwargs:
#         :return:
#         """
#         if not request.user.is_authenticated:
#             return func(request)
#         return redirect(redirect_url)
#
#     return decorated
#
#
# def is_premium(func, redirect_url=settings.HOME_URL, login_url=settings.LOGIN_URL):
#     """
#     redirects regular user from accessing artist pages
#     :param func:
#     :param redirect_url:
#     :return:
#     """
#
#     def decorated(request, *args, **kwargs):
#         """
#         redirects non-artist user
#         :param request:
#         :param args:
#         :param kwargs:
#         :return:
#         """
#         if not request.user.is_authenticated:
#             return redirect(login_url)
#
#         if request.user.account.is_premium or request.user.is_staff:
#             return func(request)
#
#         return redirect(redirect_url)
#
#     return decorated
#
#
# def validate_email(func, redirect_url=settings.VALIDATE_EMAIL_URL):
#     """
#     forces user to verify email
#     :param func:
#     :param redirect_url:
#     :return:
#     """
#
#     def decorated(request, *args, **kwargs):
#         """
#         redirects user with unverified email account
#         :param request:
#         :param args:
#         :param kwargs:
#         :return:
#         """
#         # check if user is logged in
#         if not request.is_authenticated():
#             return redirect(settings.LOGIN_URL)
#
#         if request.user.account.email_valid:
#             return func(request)
#         return redirect(redirect_url)
#
#     return decorated
#
#
# # celery tasks
# @celery_app.task
# def send_card(card_id, user_id, email_recipients, phone_recipients, personalized_message, sender_name, sender, domain,
#               card_image=None, subject=None, file_path=None, tag='general'):
#     """
#     queue celery send task
#     :param card_id:
#     :param user_id
#     :param email_recipients:
#     :param phone_recipients:
#     :param personalized_message:
#     :param sender_name:
#     :param sender:
#     :param domain:
#     :param card_image:
#     :param subject
#     :param file_path
#     :return:
#     """
#     card = Card.objects.get(pk=card_id)
#     user = Account.objects.get(pk=user_id)
#     # _card = SentCard.objects.filter(card=card.pk, user=user.pk).first()
#     # if _card:
#     #     _card.count += 1
#     #     _card.save()
#     # else:
#     #     _card = SentCard.objects.create(card=card, user=user)
#
#     if file_path:
#         resp, phones, emails = create_contacts_from_spreadsheet(user_id, file_path, domain)
#         phone_recipients = list(sorted(phones + phone_recipients))
#         email_recipients = list(sorted(email_recipients + emails))
#
#     resp = MailService.send_card(card.pk, user.pk, email_recipients, phone_recipients, personalized_message,
#                                  sender_name,
#                                  sender, domain, card_image, subject, tag=tag)
#
#     if resp is False:
#         # reverse sent count
#         # _card = SentCard.objects.filter(card=card_id, user=user.pk).first()
#         #
#         # if _card:
#         #     _card.count -= 1
#         #     _card.save()
#
#         log.error('[CARD: %s][RECIPIENTS: %s][STATUS: FAILED][SENDER: %s]' % (card.title,
#                                                                               (email_recipients + phone_recipients),
#                                                                               user.name))
#         return True
#
#     save_thread.send(sender=None, emails=email_recipients, phones=phone_recipients, user=user)
#
#     log_message = '[CARD: %s][RECIPIENTS: %s][STATUS: SUCCESS][SENDER: %s]' % (card.title,
#                                                                                (email_recipients + phone_recipients),
#                                                                                user.name)
#     log.info(log_message)
#     log.debug(log_message)
#
#     return True
#
#
# # def get_domain(request):
# #     """
# #     set domain address based on cuurent location
# #     """
# #
# #     domain = 'http://%s' % settings.SCHEME
# #     host = request.get_host()
# #     addresses = host.split('.')
# #     if Organization.objects.filter(code=addresses[0]).exists() and len(addresses) > 2:
# #         domain = 'http://%s.%s' % (addresses[0], settings.SCHEME)
# #
# #     return domain
#
#
# def get_domain(slug=None):
#     """
#     set domain address based on current location
#     """
#
#     domain = 'http://%s' % settings.SCHEME
#     # host = request.get_host()
#     # addresses = host.split('.')
#
#     if slug is None:
#         return domain
#
#     if Organization.objects.filter(code=slug.lower()).exists():
#         domain = 'http://%s/%s' % (settings.SCHEME, slug.lower())
#
#     return domain
#
#
# # def get_organization(request):
# #     """
# #     returns an organization
# #     :param request:
# #     :return:
# #     """
# #     host = request.get_host()
# #     addresses = host.split('.')
# #     code = addresses[0]
# #
# #     if not Organization.objects.filter(code=addresses[0]).exists():
# #         code = 'kaadie'
# #
# #     organization = Organization.objects.filter(code=code).first()
# #
# #     return organization
#
#
# def get_organization(slug=None):
#     """
#     returns an organization
#     :param slug:
#     :return:
#     """
#
#     # code = slug.lower()
#     # if not Organization.objects.filter(code=code).exists():
#     #     code = 'kaadie'
#
#     if slug is None:
#         slug = 'kaadie'
#
#     organ = redis_obj.hget('organization', slug)
#     if organ:
#         return Payload(json.loads(organ))
#
#     organization = Organization.objects.filter(code=slug.lower()).first()
#
#     return organization
#
#
# def upload_b64_s3(base64_image, filename):
#     """
#     Upload base64 encoded image to s3
#     :return:
#     """
#     s3 = client('s3', aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
#                 aws_access_key_id=settings.AWS_ACCESS_KEY_ID)
#
#     try:
#         _file = re.sub("data:image/jpeg;base64,", '', base64_image)
#         resp = s3.put_object(ACL='public-read-write', Bucket=settings.AWS_STORAGE_BUCKET_NAME,
#                              Body=_file.decode('base64'), Key=filename)
#         return True, resp
#     except:
#         return False, {}
#
#
# def get_as_base64(url):
#     """
#     base64 encodes an image url
#     :param url:
#     :return:
#     """
#     try:
#         return base64.b64encode(requests.get(url).content)
#     except:
#         raise
#
#
# def shorten_url(url):
#     """
#     Shorten url using Google URL shortener API
#     :param url:
#     :return:
#     """
#     try:
#         shorten = Shortener('Google', api_key=settings.GOOGLE_API_KEY)
#         return shorten.short(url)
#     except:
#         return url
#
#
# def build_html_message(sender, domain, link, image_url, tag='general', recipient=''):
#     """
#     build sms message based on recipient information and message tag
#     :return:
#     """
#     custom_message = SystemMessage.objects.filter(tag=tag).order_by('?').first()
#     message = custom_message.html_text % sender
#     html = render_to_string('emails/default.html', locals())
#     # return html
#     return transform(''' %s ''' % html)
#
#     # return custom_message.html_text % (recipient, sender, domain, link)
#
#
# def build_sms_message(sender, domain, link, tag='general', recipient=''):
#     """
#     build sms message based on recipient information and message tag
#     :return:
#     """
#     custom_message = SystemMessage.objects.filter(tag=tag).order_by('?').first()
#     message = custom_message.html_text % sender
#     return render_to_string('sms/default.txt', locals())
#     # return custom_message.sms_text % (recipient, sender, domain, link)
#
#
# def create_contacts_from_spreadsheet(user_id, file_path, domain='kaadie.com'):
#     """
#     create contacts from a csv file
#     :param user_id
#     :param file_path
#     :param domain
#     :return:
#     """
#     user = User.objects.get(pk=user_id)
#     phones = []
#     emails = []
#     email_contacts = filter(lambda x: x is not None,
#                             [c.email for c in user.account.contacts.all()])
#     phone_contacts = filter(lambda x: x is not None,
#                             [c.phone for c in user.account.contacts.all()])
#
#     mime = guess_type(file_path)
#     _type = mime[0]
#
#     # try:
#     if _type in ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel']:
#         _file = open_workbook(file_path)
#         for sheet_num in range(_file.nsheets):
#             sheet = _file.sheet_by_index(sheet_num)
#             for row_num in range(1, sheet.nrows):
#                 data = sheet.row_values(row_num)
#
#                 first_name = data[0]
#                 last_name = data[1]
#                 email = data[2]
#                 phone = data[3]
#
#                 if first_name is None or first_name == '':
#                     continue
#
#                 if (phone is None or phone == '') and (email is None or email == ''):
#                     continue
#
#                 emails.append(email)
#                 phones.append(phone)
#
#                 if email not in email_contacts or phone not in phone_contacts:
#                     address = data[5]
#                     city = data[6]
#                     country = data[7]
#
#                     full_name = '%s %s' % (first_name, last_name) if last_name is not None else first_name
#
#                     address = Address.objects.create(first_name=first_name, last_name=last_name, phone=phone,
#                                                      address=address, city=city, country=country,
#                                                      full_name=full_name)
#
#                     birthday = data[4]
#                     if birthday:
#                         dob = None
#                         if isinstance(birthday, str):
#                             dob = datetime.strptime(birthday, '%Y-%m-%d').date()
#                         elif isinstance(birthday, float):
#                             dob = datetime(*xldate_as_tuple(data[4], _file.datemode)).date()
#                         address.date_of_birth = dob
#                         address.save()
#
#                     con = Contact.objects.create(address=address, user=user.account, email=email)
#                     if con.address.date_of_birth is not None:
#                         set_birthday_reminder(con.pk, domain)
#
#     else:
#         _file = csv.reader(codecs.open(file_path, 'rU', 'utf-16'))
#         for data in _file:
#             first_name = data[0]
#             last_name = data[1]
#             email = data[2]
#             phone = data[3]
#
#             if not first_name and (not phone or not email):
#                 continue
#
#             if email is not None or email != '':
#                 emails.append(email)
#
#             if phone is not None or phone != '':
#                 phones.append(phone)
#
#             if (email not in email_contacts) or (phone not in phone_contacts):
#                 address = data[5]
#                 city = data[6]
#                 country = data[7]
#
#                 full_name = '%s %s' % (first_name, last_name) if last_name is not None else first_name
#
#                 address = Address.objects.create(first_name=first_name, last_name=last_name, phone=phone,
#                                                  address=address, city=city, country=country, full_name=full_name)
#
#                 birthday = data[4]
#                 if birthday:
#                     dob = None
#                     if isinstance(birthday, str):
#                         dob = datetime.strptime(birthday, '%Y-%m-%d').date()
#                     elif isinstance(birthday, float):
#                         dob = datetime(*xldate_as_tuple(data[4], _file.datemode)).date()
#                     address.date_of_birth = dob
#                     address.save()
#
#                 con = Contact.objects.create(address=address, user=user.account, email=email)
#
#                 if con.address.date_of_birth is not None:
#                     set_birthday_reminder(con.pk, domain)
#
#     return True, phones, emails
#     # except Exception, e:
#     #     raise e
#
#
# @celery_app.task
# def set_birthday_reminder(contact_id, domain='kaadie.com', **kwargs):
#     """
#     Adds a periodic task that schedules a birthday reminder for a contact
#     :param contact_id:
#     :param domain
#     :param kwargs:
#     :return:
#     """
#     try:
#         contact = Contact.objects.get(pk=contact_id)
#         account = contact.user
#
#         scheduled_time = dt_time(settings.BIRTHDAY_ALERT_HOUR, settings.BIRTHDAY_ALERT_MINUTE)
#
#         if Settings.objects.filter(account=account.pk).exists():
#             setting = Settings.objects.filter(account=account.pk).first()
#             scheduled_time = setting.birthday_alert_time
#
#         dob = contact.address.date_of_birth
#
#         crontab_, created = CrontabSchedule.objects.get_or_create(
#             minute=scheduled_time.minute,
#             hour=scheduled_time.hour,
#             day_of_week=dob.isoweekday(),
#             day_of_month=dob.day,
#             month_of_year=dob.month
#         )
#
#         task = PeriodicTask.objects.filter(name='Birthday Schedule for %s[%s %s %s]'
#                                                 % (contact.name, contact.phone, contact.email, account.user.email)
#                                            ).first()
#         if not task:
#             PeriodicTask.objects.create(
#                 crontab=crontab_,
#                 name='Birthday Schedule for %s[%s %s %s]' % (
#                     contact.name, contact.phone, contact.email, account.user.email),
#                 task='app.helper.send_birthday_reminder',
#                 kwargs=json.dumps({
#                     'contact_id': contact.id,
#                     'domain': domain
#                 })
#             )
#         else:
#             task.crontab = crontab_
#             task.save()
#
#         contact.is_reminder_set = True
#         contact.save()
#
#         return True
#     except Exception, e:
#         return False
#
#
# @celery_app.task
# def send_birthday_reminder(contact_id, domain):
#     """
#     send birthday reminder for a contact
#     :param contact_id:
#     :return:
#     """
#     contact = Contact.objects.get(pk=contact_id)
#     account = contact.user
#
#     birthday_category = Category.objects.filter(slug=settings.BIRTHDAY_CATEGORY).first()
#     random_card = birthday_category.cards.filter(is_active=True).order_by('?').first()
#
#     if account.saved_cards.filter(is_active=True).count() > 0:
#         random_card = account.saved_cards.filter(is_active=True, card__categories__in=[birthday_category]).order_by(
#             '?').first()
#
#     if not random_card:
#         reset_message = "<p>Hello {},</p>" \
#                         "<p> There are currently no birthday cards available."
#
#         MailService.send_mail('No Birthday Cards', 'system@kaadie.com', 'Kaadie', settings.ALERT_ACCOUNT,
#                               reset_message, 'Kaadie')
#
#     return send_card(random_card.pk, account.pk, [contact.email], [contact.phone], '', account.name, account.email,
#                      domain, tag=settings.BIRTHDAY_CATEGORY)
#
#
# def set_attributes(_object, **kwargs):
#     """
#
#     :param _object:
#     :param kwargs:
#     :return:
#     """
#     for key, value in kwargs.items():
#         if hasattr(_object, key):
#             setattr(_object, key, value)
#
#     return _object
#
#
# def validate_file_uploaded(file_path):
#     """
#     checks if uploaded file is a permitted file
#     :param file_path:
#     :return:
#     """
#     try:
#         _mime = guess_type(file_path)
#         return _mime[0] in settings.ACCEPTED_MIME_TYPES
#     except:
#         return False
#
#
# def request_artist_permission(email, portfolio, name):
#     """
#     request artist permission
#     :param email:
#     :param portfolio:
#     :param name:
#     :return:
#     """
#     request_message = "<p>Hello Admin,</p>" \
#                       "<p>%s (%s) wants to have artist rights on Kaadie.</p><p>Click here to view his portfolio %s</p>" \
#                       "<p>As an artist, he can create cards that can " \
#                       "viewed, shared and saved by all visitors to the platform.</p>" \
#                       "<p>To grant access, create an artist account for this user on the admin panel.</p>" % (name, email, portfolio)
#
#     return MailService.send_mail('Artist Rights Permission', 'system@kaadie.com', 'Kaadie', settings.ALERT_ACCOUNT,
#                                  request_message, 'Kaadie')
#
#
# def record_log(request, verb, _object, object_type):
#     """
#     record logs
#     :param request:
#     :param verb:
#     :param _object:
#     :param object_type:
#     :return:
#     """
#     ua = get_user_agent(request)
#
#     device_type = 'pc'
#
#     if ua.is_mobile:
#         device_type = 'mobile'
#
#     if ua.is_tablet:
#         device_type = 'tablet'
#
#     username = 'anonymous' if not request.user.is_authenticated() else request.user.email
#     name = 'Anonymous' if not request.user.is_authenticated() else request.user.account.name
#
#     record_activity_log.delay(username, name, verb, _object, object_type,
#                               datetime.now(), ua.browser.family, ua.device.family, device_type, ua.os.family)
#
#     return True
#
#
# @celery_app.task
# def record_activity_log(identifier, actor, verb, _object, object_type, timestamp, browser, device, device_type, os):
#     """
#     records activity log
#     :param identifier:
#     :param actor:
#     :param verb:
#     :param _object:
#     :param object_type:
#     :param timestamp
#     :param browser:
#     :param device:
#     :param device_type
#     :param os:
#     :return:
#     """
#     try:
#         db = mongo_client[settings.MONGOD_DATABASE]
#
#         activities = db.activities
#         record = {
#             'id': identifier,
#             'actor': actor,
#             'verb': verb,
#             'object': _object,
#             'object_type': object_type,
#             'browser': browser,
#             'device': device,
#             'device_type': device_type,
#             'os': os,
#             'timestamp': timestamp
#         }
#         activities.insert_one(record)
#         return True
#     except:
#         log.error('[ACTOR: %s][VERB: %s][ACTION: %s][STATUS: FAILED]' % (actor, verb, _object))
#         return False
#
#
# def rewrite_css_file(organization_id):
#     """
#     rewrite css file for that organization
#     :param organization_id:
#     :return:
#     """
#     try:
#         import os
#         organization = Organization.objects.get(pk=organization_id)
#         path = os.path.join(settings.BASE_DIR, 'staticfiles/css/brands/%s.css' % organization.code)
#
#         _file = open(path, 'wb')
#         css_data = render_to_string('css/default.txt', locals())
#         _file.write(css_data)
#         _file.close()
#
#         return True
#
#     except Exception, e:
#         log.error('[FAILED TO REWRITE CSS FILE BECAUSE %s] [OBJECT: %s]' % (e, organization_id))
#         return False
#
#
# def retrieve_logs(identifier):
#     """
#     retrieve logs for a particular user
#     :param identifer:
#     :return:
#     """
#     client = MongoClient(settings.MONGOD_HOST, settings.MONGOD_PORT)
#     db = client[settings.MONGOD_DATABASE]
#
#     activities = db.activities
#
#     count = activities.find({'id': identifier}).count()
#     logs = map(lambda x: x, activities.find({'id': identifier}))
#
#     return {
#         'count': count,
#         'results': logs
#     }
#
#
# def get_custom_domain(request):
#     """
#     get sub domain
#     :param request:
#     :return:
#     """
#     domain = 'http://%s' % settings.SCHEME
#
#     try:
#         path = request.path
#         split_path = path.split('/')
#
#         if split_path[0] not in settings.RESTRICTED_ROUTES and len(split_path[0]) > 0:
#
#             if Organization.objects.filter(code=split_path[0].lower()).exists():
#                 domain = 'http://%s/%s' % (settings.SCHEME, split_path[0].lower())
#
#         return domain
#     except:
#         return domain
#
#
# def cache_single_record(model_name, pk, **kwargs):
#     """
#     cache individual record in redis
#     :param model_name:
#     :param pk:
#     :param kwargs:
#     :return:
#     """
#     from . import models
#     title = model_name.title().replace(' ', '')
#     model_klass = getattr(models, title)
#
#     instance = model_klass.objects.get(pk=pk)
#
#     orgs = Organization.objects.all()
#
#     if model_name in settings.CACHED_MODELS:
#         if model_name == 'section':
#             for org in orgs:
#                 cache_records('%s_sections' % org.code, pk, **kwargs)
#
#         elif model_name == 'category':
#             cache_records('category', '%s:%s' % (instance.section_id, instance.slug),
#                           **kwargs)
#             cards = [app_serializers.CardSerializer(c).data for c in instance.cards.all()]
#             for _card in cards:
#                 cache_records('%s:cards' % instance.slug, _card['slug'], **_card)
#
#         elif model_name == 'saved card':
#             serialized = app_serializers.CardSerializer(instance.card).data
#             cache_records('saved_cards:%s' % instance.user.pk, serialized['pk'], **serialized)
#
#         else:
#             if model_name == 'card' and instance.is_featured is True and instance.is_active is True:
#                 cache_records('featured_cards', instance.pk, **kwargs)
#
#             cache_records(model_name, instance.pk, **kwargs)
#
#     return True
#
#
# @celery_app.task
# def index_instance(model_name, pk):
#     """
#     index model instance in elasticsearch
#     :return:
#     """
#     try:
#         from . import models
#         title = model_name.title().replace(' ', '')
#         model_klass = getattr(models, title)
#
#         klass = getattr(app_serializers, '%sSerializer' % title)
#         instance = model_klass.objects.get(pk=pk)
#
#         serialized = klass(instance)
#         data = serialized.data
#         data['pk'] = instance.pk
#
#         if model_name == 'card':
#             data['slugs'] = data['categories']
#             data['b64_url'] = "data:image/png;base64,%s" % get_as_base64(instance.image_url)
#             data['sent_counts'] = instance.sent_counts
#             data['saved_counts'] = instance.saved_counts
#             data['shared_count'] = instance.shared_count
#
#         elif model_name == 'category':
#             _section = Section.objects.get(pk=data['section_id'])
#             klass = getattr(app_serializers, 'SectionSerializer')
#             section = klass(_section)
#             _data = section.data
#             data['permissible_organizations'] = _data['permissible_organizations']
#
#         elif model_name == 'banner':
#             image_url = data['image_url']
#             data['b64_url'] = "data:image/png;base64,%s" % get_as_base64(image_url)
#
#         elif model_name == 'organization':
#             if data.get('logo'):
#                 image_url = data['logo']
#                 data['logo'] = "data:image/png;base64,%s" % get_as_base64(image_url)
#
#             rewrite_css_file(instance.pk)
#
#         es.index(index=settings.ES_INDEX, doc_type=model_name, id=instance.pk, body=data)
#
#         cache_single_record(model_name, **data)
#
#         return True
#     except:
#         return False
#
#
# @celery_app.task
# def delete_instance(model_name, serialized_instance):
#     """
#     index model instance in elasticsearch
#     :return:
#     """
#     try:
#         instance = Payload(serialized_instance)
#         instance_id = instance.pk
#         try:
#             es.delete(index=settings.ES_INDEX, doc_type=model_name, id=instance_id)
#         except Exception, e:
#             print '==============='
#             print e
#
#         if model_name == 'card':
#             redis_obj.hdel('card', instance_id)
#             redis_obj.hdel('featured_cards', instance_id)
#
#             for category in instance.categories:
#                 redis_obj.hdel('%s:cards' % category, instance.slug)
#
#         elif model_name == 'category':
#             redis_obj.delete('%s:cards' % instance.slug)
#             section = instance.section
#
#             klass = getattr(app_serializers, 'SectionSerializer')
#             serialized = klass(section)
#             data = serialized.data
#
#             for org in Organization.objects.all():
#                 if redis_obj.hexists('%s_sections' % org.code):
#                     cache_records('%s_sections' % org.code, section.pk, **data)
#
#         elif model_name == 'section':
#             for org in Organization.objects.all():
#                 if redis_obj.hexists('%s_sections' % org.code, instance_id):
#                     redis_obj.hdel('%s_sections' % org.code, instance_id)
#
#         elif model_name == 'organization':
#             redis_obj.hdel('organization', instance.code)
#
#         else:
#             redis_obj.hdel(model_name, instance_id)
#
#         return True
#     except:
#         return False
#
#
# @celery_app.task
# def trigger_notification(message, link, target_user_ids=[], event=None):
#     """
#     send notifications using pusher
#     :param message:
#     :param link:
#     :param event:
#     :return:
#     """
#     if not event:
#         event = settings.PUSHER_DEFAULT_EVENT
#
#     try:
#         for user_id in target_user_ids:
#             _user = Account.objects.get(pk=user_id)
#             notification = Notifications.objects.create(body=message, link=link, event=event, user=_user)
#             pusher_client.trigger('kaadie', notification.event, {'message': notification.body,
#                                                                  'link': notification.link})
#         return True
#     except:
#         return False
#
#
# class Payload(object):
#     def __init__(self, j):
#         for key, value in j.items():
#             if isinstance(value, (list, tuple)):
#                 setattr(self, key, [Payload(item) if isinstance(item, dict) else item for item in value])
#             else:
#                 setattr(self, key, Payload(value) if isinstance(value, dict) else value)
#
#     def __getitem__(self, item):
#         return getattr(self, item)
#
# # def fetch_sections(code):
# #     """
# #     fetch sections matching this organization code
# #     :param code: organization code
# #     :return:
# #     """
# #     section_query = es.search(index=settings.ES_INDEX, doc_type='section',
# #                               body={'query': {'terms': {
# #                                   'permissible_organizations': [code]
# #                               }}})
# #     section_hits = section_query['hits']
# #
# #     if section_hits['total'] == 0:
# #
# #         organization = Organization.objects.filter(code=code).first()
# #         if not organization:
# #             return False, []
# #
# #         return True, Section.objects.filter(permissible_organizations__in=[organization]).order_by('position').all()
# #
# #     _sections = section_hits['hits']
# #     return True, [Payload(c['_source']) for c in _sections]
#
#
# def cache_sections():
#     """
#     cache sections in redis
#     :param
#     :return:
#     """
#     organizations = Organization.objects.all()
#     for org in organizations:
#         cache_organization_sections(org.code)
#
#     return True
#
#
# def cache_organization_sections(code):
#     """
#     fetch sections matching this organization code
#     :param code: organization code
#     :return:
#     """
#     section_query = es.search(index=settings.ES_INDEX, doc_type='section',
#                               body={'query': {'terms': {
#                                   'permissible_organizations': [code]
#                               }}})
#     section_hits = section_query['hits']
#
#     if section_hits['total'] == 0:
#
#         organization = Organization.objects.filter(code=code).first()
#         if not organization:
#             return False, []
#
#         _sections = [app_serializers.SectionSerializer(c).data for c in Section.objects.filter(
#             permissible_organizations__in=[organization]).order_by('position').all()]
#     else:
#         _sections = [c['_source'] for c in section_hits['hits']]
#
#     for item in _sections:
#         cache_records('%s_sections' % code, item['pk'], **item)
#
#     return True
#
#
# # def fetch_banners():
# #     """
# #     fetch all banners
# #     :return:
# #     """
# #     banner_query = es.search(index=settings.ES_INDEX, doc_type='banner', body={'query': {'match_all': {}}})
# #     banner_hits = banner_query['hits']
# #
# #     if banner_hits['total'] == 0:
# #         return Banner.objects.all()
# #
# #     _banners = banner_hits['hits']
# #     return [Payload(c['_source']) for c in _banners]
#
# def cache_banners():
#     """
#     fetch all banners
#     :return:
#     """
#     banner_query = es.search(index=settings.ES_INDEX, doc_type='banner', body={'query': {'match_all': {}}})
#     banner_hits = banner_query['hits']
#
#     if banner_hits['total'] == 0:
#         _banners = [app_serializers.BannerSerializer(i).data for i in Banner.objects.all()]
#     else:
#         _banners = [c['_source'] for c in banner_hits['hits']]
#
#     for ban in _banners:
#         cache_records('banner', ban['pk'], **ban)
#
#     return True
#
#
# def cache_featured_cards():
#     """
#     Fetch all featured cards
#     :return:
#     """
#     fetch_query = es.search(index=settings.ES_INDEX, doc_type='card',
#                             body={'query': {'function_score': {'query': {'bool': {'must': [
#                                 {'match': {'is_active': True}},
#                                 {'match': {'is_featured': True}},
#                                 {'match': {'is_public': True}}
#                             ]}}}}})
#
#     fetch_hits = fetch_query['hits']
#
#     if fetch_hits['total'] == 0:
#         _cards = [app_serializers.CardSerializer(c).data for c in Card.objects.filter(is_featured=True,
#                                                                                       is_active=True,
#                                                                                       is_public=True).order_by(
#             '?').all()]
#     else:
#         _cards = [c['_source'] for c in fetch_hits['hits']]
#
#     for _card in _cards:
#         cache_records('featured_cards', _card['pk'], **_card)
#
#     return True
#
#
# def cache_categories():
#     """
#     fetch sections matching this organization code
#     :param code: organization code
#     :return:
#     """
#     # category_query = es.search(index=settings.ES_INDEX, doc_type='category',
#     #                           body={'query': {'match_all': {}}})
#     # cat_hits = category_query['hits']
#     #
#     # if cat_hits['total'] == 0:
#     #
#     # else:
#     #     categories = [c['_source'] for c in cat_hits['hits']]
#
#     categories = Category.objects.all()
#
#     for item in categories:
#         cache_records('category', '%s:%s' % (item.section_id, item.slug),
#                       **app_serializers.CategorySerializer(item).data)
#         cards = [app_serializers.CardSerializer(c).data for c in item.cards.all()]
#         for _card in cards:
#             cache_records('%s:cards' % item.slug, _card['slug'], **_card)
#
#     return True
#
#
# # def fetch_saved_cards(user_id):
# #     """
# #     fetch saved cards by user id
# #     :return:
# #     """
# #     saved_query = es.search(index=settings.ES_INDEX, doc_type='saved card', body={'query': {'match': {'user_id':
# #                                                                                                           user_id}}})
# #     saved_hits = saved_query['hits']
# #
# #     if saved_hits['total'] == 0:
# #         account = Account.objects.get(pk=user_id)
# #         return [app_serializers.CardSerializer(c.card).data for c in account.saved_cards.all()]
# #
# #     _saved = saved_hits['hits']
# #
# #     return [Payload(c['_source']['card']) for c in _saved]
#
#
# def cache_saved_cards(user_id):
#     """
#     fetch saved cards by user id
#     :return:
#     """
#     saved_query = es.search(index=settings.ES_INDEX, doc_type='saved card', body={'query': {'match': {'user_id':
#                                                                                                           user_id}}})
#     saved_hits = saved_query['hits']
#
#     if saved_hits['total'] == 0:
#         account = Account.objects.get(pk=user_id)
#         _saved = [app_serializers.CardSerializer(c.card).data for c in account.saved_cards.all()]
#     else:
#         _saved = [c['_source']['card'] for c in saved_hits['hits']]
#
#     for s in _saved:
#         cache_records('saved_cards:%s' % user_id, s['pk'], **s)
#
#     return True
#
#
# def cache_records(hash_name, obj_id, **kwargs):
#     """
#     cache dictionary
#     :param obj_id:
#     :param hash_name:
#     :param kwargs:
#     :return:
#     """
#     if redis_obj.hexists(hash_name, obj_id):
#         redis_obj.hdel(hash_name, obj_id)
#
#     res = redis_obj.hset(hash_name, obj_id, json.dumps(kwargs))
#     print(res)
#     return res
#
#
# def cache_organization():
#     """
#     save organization into redis cache
#     :return:
#     """
#     organs_query = es.search(index=settings.ES_INDEX, doc_type='organization', body={'query': {'match_all': {}}})
#
#     organs_hits = organs_query['hits']
#
#     if organs_hits['total'] == 0:
#         _organs = [app_serializers.OrganizationSerializer(c).data for c in Organization.objects.all()]
#     else:
#         _organs = [c['_source'] for c in organs_hits['hits']]
#
#     for s in _organs:
#         cache_records('organization', s['code'], **s)
#
#     return True
#
#
# def fetch_categories(code):
#     """
#     fetch categories by organization code
#     :param code:
#     :return:
#     """
#     organization = Organization.objects.filter(code=code).first()
#     if not organization:
#         return False, Category.objects.filter(is_active=True).order_by('?')[:6].all()
#
#     try:
#         cat_query = es.search(index=settings.ES_INDEX, doc_type='category',
#                               body={'query': {'function_score': {'query': {'terms': {
#                                   'permissible_organizations': [code]
#                               }}, 'functions': [{
#                                   'random_score': {'seed': random.randint(1, 1000000)}
#                               }]}}, 'filter': {
#                                   'bool': {'must': [{'term': {'is_active': True}}]}
#                               }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)
#
#         cat_hits = cat_query['hits']
#
#         if cat_hits['total'] == 0:
#             return False, Category.objects.filter(is_active=True,
#                                                   section__permissible_organizations__in=[organization
#                                                                                           ]).order_by('?')[:6].all()
#         _categories = [c['_source'] for c in cat_hits['hits']]
#         slugs = [c['slug'] for c in _categories]
#
#         query = es.search(index=settings.ES_INDEX, doc_type='card', body={'query': {'terms': {'slugs': slugs}}},
#                           size=50)
#
#         query_hits = query['hits']
#
#         if query_hits['total'] == 0:
#             return False, Category.objects.filter(is_active=True,
#                                                   section__permissible_organizations__in=[organization
#                                                                                           ]).order_by('?')[:6].all()
#
#         hits = query_hits['hits']
#
#         data = [i.get('_source') for i in hits]
#
#         unique_keys = list(set(list(chain.from_iterable([c['slugs'] for c in data]))))
#         categories = []
#
#         for key in unique_keys:
#             # category_query = es.search(index=settings.ES_INDEX, doc_type='category', body={'query': {'match':
#             #                                                                                              {'slug': key}}})
#             # cat_query_hits = category_query['hits']
#             # cat_total = cat_query_hits['total']
#             #
#             # if cat_total == 0:
#             #     continue
#             #
#             # cat_hits = cat_query_hits['hits']
#             # cat = cat_hits[0].get('_source')
#             try:
#                 cat = (item for item in _categories if item['slug'] == key).next()
#                 cat['cards'] = [j for j in data if key in j['slugs']]
#                 categories.append(cat)
#             except:
#                 continue
#
#         print('------------------------------')
#         __categories = filter(lambda x: len(x['cards']) > 0, categories)
#         return True, __categories
#     except Exception, e:
#         print('==============================')
#         return False, Category.objects.filter(is_active=True,
#                                               section__permissible_organizations__in=[organization
#                                                                                       ]).order_by('?')[:6].all()
#
#
# def fetch_from_cache(code, user_id=None):
#     """
#     fetch cached elements
#     :param user_id:
#     :param code:
#     :return:
#     """
#     _banners = redis_obj.hgetall('banner')
#     banners = [json.loads(c) for c in _banners.values()]
#     # banners = [Payload(json.loads(c)) for c in _banners.values()]
#
#     _featured = redis_obj.hgetall('featured_cards')
#     featured_cards = [json.loads(c) for c in _featured.values()]
#     # featured_cards = [Payload(json.loads(j)) for j in _featured.values()]
#
#     _sections = redis_obj.hgetall('%s_sections' % code)
#     __sections = [json.loads(c) for c in _sections.values()]
#     sections = sorted(__sections, key=lambda x: (x['position'] is None, x['position']))
#     # sections = [Payload(json.loads(j)) for j in _sections.values()]
#
#     saved = []
#     if user_id:
#         _saved = redis_obj.hgetall('saved_cards:%s' % user_id)
#         __saved = [json.loads(c) for c in _saved.values()]
#         saved = [c['card'] for c in __saved]
#         # saved = [Payload(json.loads(i)) for i in _saved.values()]
#
#     return banners, featured_cards, sections, saved
#
#
# def fetch_cache_saved_cards(user_id):
#     """
#     fetch saved cards from redis matching user_id
#     :param user_id:
#     :return:
#     """
#     try:
#         _saved = redis_obj.hgetall('saved_cards:%s' % user_id)
#         return [Payload(json.loads(i)) for i in _saved.values()]
#     except Exception, e:
#         _user = Account.objects.filter(pk=user_id).first()
#         saved = []
#         if _user:
#             saved = [c.card for c in _user.saved_cards.all()]
#         return saved
#
#
# def fetch_cache_sections(code):
#     """
#     fetch cached sections
#     :param code:
#     :return:
#     """
#     try:
#         _sections = redis_obj.hgetall('%s_sections' % code)
#         sections = [Payload(json.loads(j)) for j in _sections.values()]
#         return sorted(sections, key=lambda x: (x.position is None, x.position))
#     except Exception, e:
#         organization = get_organization(code)
#         return Section.objects.filter(permissible_organizations__in=[organization.pk]).order_by(
#             'position').all()
#
#
# def publish_message(module_name, function_name, *args, **kwargs):
#     """
#     publish message via kafka
#     :param module_name:
#     :param function_name:
#     :param args:
#     :param kwargs:
#     :return:
#     """
#     # Block for 'synchronous' sends
#     import os
#
#     file_path = os.path.realpath(__file__)
#     try:
#         producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
#                                  value_serializer=lambda m: json.dumps(m).encode('ascii'))
#
#         # Asynchronous by default
#         future = producer.send(str('messages'), {'mod_name': module_name, 'fxn_name': function_name, 'args': args,
#                                                  'data': kwargs})
#
#         record_metadata = future.get(timeout=10)
#         if future.succeeded():
#             return True
#     except KafkaError, e:
#         # Decide what to do if produce request failed...
#         print(e)
#         return False
#
#
# def load_random_cards(slug):
#     try:
#         random_query = es.search(index=settings.ES_INDEX, doc_type='card',
#                                  body={'query': {
#                                      'function_score': {'query': {"match": {'is_active': True}}, 'functions': [{
#                                          'random_score': {'seed': random.randint(1, 1000000)}
#                                      }]}}, 'filter': {
#                                      'bool': {'must': [{'term': {'is_public': True}}],
#                                               'must_not': [{'term': {'slug': slug}}]}
#                                  }, 'sort': [{"_score": {"order": "desc"}}]}, size=6)
#
#         random_hits = random_query['hits']
#
#         if random_hits['total'] == 0:
#             random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=slug).order_by(
#                 '?')[:6].all()
#
#         else:
#             _random = random_hits['hits']
#             random_cards = filter(lambda x: x.slug != slug, [Payload(i['_source']) for i in _random])
#     except:
#         random_cards = Card.objects.filter(is_active=True, is_public=True).exclude(slug=slug).order_by(
#             '?')[:6].all()
#
#     return random_cards
#
#
# def validate_url(url):
#     """
#     check if url is valid
#     :param url:
#     :return:
#     """
#     parsed_url = urlparse.urlparse(str(url))
#     return bool(parsed_url.scheme)