from datetime import datetime, timedelta
import time, hashlib

from django.utils.text import slugify
from django.views.decorators.csrf import csrf_exempt
from django_celery_beat.models import PeriodicTask

from rest_framework import viewsets as rest_viewsets, mixins as rest_mixins, response, status, authentication
import models, serializers, forms
from filters import CardFilter
from services import mixins
from app.services import helper


class CardViewSet(rest_viewsets.ModelViewSet):
    """
        retrieve:
            Return a card instance.

        list:
            Return all cards, ordered by most recently created.

        create:
            Create a new card.

        delete:
            Remove an existing card.

        partial_update:
            Update one or more fields on an existing user.

        update:
            Update a card.
    """
    # authentication_classes = (CsrfExemptSessionAuthentication,)
    authentication_classes = (mixins.CsrfExemptSessionAuthentication, authentication.TokenAuthentication,
                              authentication.SessionAuthentication)
    queryset = models.Card.objects.all()
    serializer_class = serializers.CardSerializer
    filter_class = CardFilter
    filter_fields = ('user_id', 'categories')

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(CardViewSet, self).dispatch(*args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            # user = models.User.objects.get(pk=serializer.validated_data['user'])
            user = request.user
            account = user.account
            serializer.validated_data['user'] = account

            card = models.Card.objects.filter(slug=slugify(serializer.validated_data['title'])).first()

            if card:
                return response.Response({'message': 'Card already exists'}, status=status.HTTP_400_BAD_REQUEST)

            card = models.Card.objects.create(**serializer.validated_data)

            return response.Response({
                "pk": card.pk,
                "date_created": card.date_created,
                "date_modified": card.date_modified,
                "title": card.title,
                "description": card.description,
                "image_url": card.image_url,
                "slug": card.slug,
                "user_id": card.user.pk
            }, status=status.HTTP_201_CREATED)

        return response.Response({
            'status': 'Bad request',
            'message': serializer.errors
        }, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        try:
            card = self.get_object()

            if request.GET.get('resource') == 'increment_shared_count' and request.GET.get('platform') is not None:
                platform = request.GET.get('platform')
                notification_id = request.GET.get('hash_code')
                models.SharedCard.objects.create(platform=platform, card=card, user=request.user.account,
                                                 notification_id=notification_id)
                return response.Response({'response': True})

            if not card.user.pk == request.user.account.pk or not request.user.is_staff:
                return response.Response({'message': 'You are not authorized to edit this card'},
                                         status=status.HTTP_401_UNAUTHORIZED)

            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            data = request.data.copy()
            data['user'] = request.user.account.pk
            serializer = self.get_serializer(instance, data=data, partial=partial)
            serializer.is_valid(raise_exception=True)
            card.image_url = data['image_url']
            card.title = data['title']
            card.description = data.get('description', card.description)
            card.save()

            _data = self.serializer_class(card)
            return response.Response(_data.data)
        except Exception, e:
            raise e

    def destroy(self, request, *args, **kwargs):

        instance = self.get_object()
        if not instance.user.pk == request.user.account.pk or not request.user.is_staff:
            return response.Response({'message': 'You are not authorized to edit this card'},
                                     status=status.HTTP_401_UNAUTHORIZED)

        self.perform_destroy(instance)

        return response.Response(status=status.HTTP_204_NO_CONTENT)


class AccountViewSet(rest_viewsets.ModelViewSet):
    """
        retrieve:
            Return a user account instance.

        list:
            Return all accounts, ordered by most recently joined.

        create:
            Create a new account.

        delete:
            Remove an existing account.

        partial_update:
            Update one or more fields on an existing account.

        update:
            Updates an account.
    """
    queryset = models.Account.objects.all()
    serializer_class = serializers.AccountSerializer
    authentication_classes = (mixins.CsrfExemptSessionAuthentication, authentication.TokenAuthentication,
                              authentication.SessionAuthentication)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(AccountViewSet, self).dispatch(*args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            models.Account.objects.create_user(**serializer.validated_data)

            return response.Response(serializer.validated_data, status=status.HTTP_201_CREATED)

        return response.Response({
            'status': 'Bad request',
            'message': 'Account could not be created with received data.'
        }, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, *args, **kwargs):
        """
        custom update method for accounts
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        try:
            address_data = request.data['address']
            address = models.Address.objects.get(pk=address_data['id'])
            address.first_name = address_data['first_name']
            address.last_name = address_data['last_name']
            address.city = address_data['city']
            address.country = address_data['country']
            address.full_name = '%s %s' % (address.first_name, address.last_name)
            address.phone = address_data["phone"]
            address.address = address_data['address']
            if address_data['birthday']:
                birth = datetime.strptime(address_data['birthday'], '%Y-%m-%dT%H:%M:%S.%fZ') \
                        + timedelta(hours=1)
                address.date_of_birth = birth.date()

            address.save()
            contact = models.Contact.objects.get(pk=request.data['pk'])
            contact.email = request.data['email']
            return response.Response({'response': True})
        except:
            raise Exception


class AddressViewSet(rest_viewsets.ModelViewSet):
    """
        retrieve:
            Return an address instance.

        list:
            Return all addresses, ordered by most recently joined.

        create:
            Create a new address.

        delete:
            Remove an existing address.

        partial_update:
            Update one or more fields on an existing address.

        update:
            Update an address.
    """
    lookup_field = 'username'
    queryset = models.Address.objects.all()
    serializer_class = serializers.AddressSerializer

    def create(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            models.Address.objects.create_user(**serializer.validated_data)

            return response.Response(serializer.validated_data, status=status.HTTP_201_CREATED)

        return response.Response({
            'status': 'Bad request',
            'message': 'Address could not be created with received data.'
        }, status=status.HTTP_400_BAD_REQUEST)


class ContactViewSet(rest_viewsets.ModelViewSet):
    """
        retrieve:
            Return a contact instance.

        list:
            Return all contacts, ordered by most recently joined.

        create:
            Create a new contact.

        delete:
            Remove an existing contact.

        partial_update:
            Update one or more fields on an existing contact.

        update:
            Update a contact.
    """
    queryset = models.Contact.objects.all()
    serializer_class = serializers.ContactSerializer
    authentication_classes = (mixins.CsrfExemptSessionAuthentication, authentication.TokenAuthentication,
                              authentication.SessionAuthentication)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ContactViewSet, self).dispatch(*args, **kwargs)

    def list(self, request):

        user_id = request.user.pk
        queryset = models.Contact.objects.filter(user=user_id)
        queryset = self.filter_queryset(queryset)

        page = self.paginate_queryset(queryset)

        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    def create(self, request, *args, **kwargs):
        """
        create new contact
        :param request:
        :return:
        """
        user = models.User.objects.get(pk=request.user.pk)

        request_data = request.data or request.POST

        form = forms.ContactForm(request_data)

        if not form.is_valid():
            return response.Response(form.errors,
                                     status=status.HTTP_400_BAD_REQUEST)

        data = dict(first_name=form.data.get('first_name'),
                    last_name=form.data.get('last_name', ''),
                    email=form.data.get('email'),
                    phone=form.data.get('phone'),
                    city=form.data.get('city'),
                    country=form.data.get('country'),
                    address=form.data.get('address'),
                    date_of_birth=form.data.get('date_of_birth')
                    )

        email_contacts = [c.email for c in user.account.contacts.all()]
        phone_contacts = [c.phone for c in user.account.contacts.all()]

        if data['email'] in email_contacts:
            return response.Response({'message': 'You have a contact with this email address - %s already.'
                                                 % data['email']}, status=status.HTTP_400_BAD_REQUEST)

        if data['phone'] in phone_contacts:
            return response.Response({'message': 'You have a contact with this phone number - %s already.'
                                                 % data['phone']}, status=status.HTTP_400_BAD_REQUEST)

        dob = None

        if data['date_of_birth'] is not None and isinstance(data['date_of_birth'], (str, unicode)):
            try:
                dob = datetime.strptime(data['date_of_birth'], '%Y-%m-%d').date()
            except:
                return response.Response('Invalid Date Format, should be in the format - 1970-01-01',
                                         status=status.HTTP_400_BAD_REQUEST)

        address = models.Address.objects.create(first_name=data['first_name'], last_name=data['last_name'],
                                         city=data['city'],
                                         country=data['country'], phone=data['phone'], address=data['address'],
                                         full_name='%s %s' % (data['first_name'], data['last_name']),
                                         date_of_birth=dob
                                         )
        address.save()

        contact = models.Contact.objects.create(address=address, email=data['email'], user=user.account)

        if contact.address.date_of_birth is not None:
            helper.set_birthday_reminder.delay(contact.id, domain='kaadie.com')

        account = self.serializer_class(contact)

        return response.Response(account.data,
                                 status=status.HTTP_200_OK)

    def update(self, request, pk, *args, **kwargs):
        """
        custom update method for contacts
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        request_data = request.data or request.POST

        form = forms.ContactForm(request_data)

        if not form.is_valid():
            return response.Response(form.errors,
                                     status=status.HTTP_400_BAD_REQUEST)

        data = dict(first_name=form.data.get('first_name'),
                    last_name=form.data.get('last_name', None),
                    email=form.data.get('email'),
                    phone=form.data.get('phone'),
                    city=form.data.get('city'),
                    country=form.data.get('country'),
                    address=form.data.get('address'),
                    )

        contact = models.Contact.objects.get(pk=pk)
        address = helper.set_attributes(contact.address, **data)
        address.full_name = '%s %s' % (address.first_name, address.last_name)

        # try:
        # address_data = request.data['address']
        # address = Address.objects.get(pk=address_data['id'])
        # address.first_name = address_data['first_name']
        # address.last_name = address_data['last_name']
        # address.city = address_data['city']
        # address.country = address_data['country']
        # address.full_name = '%s %s' % (address.first_name, address.last_name)
        # address.phone = address_data["phone"]

        birthday = form.data.get('date_of_birth')

        if birthday is not None and isinstance(birthday, (str, unicode)):
            # birth = datetime.strptime(form.data.get('date_of_birth'), '%Y-%m-%dT%H:%M:%S.%fZ') \
            #         + timedelta(hours=1)
            try:
                birthday = birthday.replace('T00:00:00.000Z', '')
                birth = datetime.strptime(str(birthday), '%Y-%m-%d')
                address.date_of_birth = birth.date()
            except:
                return response.Response('Invalid Date Format, should be in the format - 1970-01-01',
                                         status=status.HTTP_400_BAD_REQUEST)

        address.save()
        contact.email = request.data['email']
        contact.save()

        if contact.address.date_of_birth:
            sub = kwargs.get('sub')
            domain = helper.get_domain(sub)
            helper.set_birthday_reminder.delay(contact.id, domain=domain)

        account = self.serializer_class(contact)

        return response.Response(account.data,
                                 status=status.HTTP_200_OK)
        # except:
        #     raise Exception

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)

        task = PeriodicTask.objects.filter(name='Birthday Schedule for %s[%s %s %s]'
                                                % (instance.name, instance.phone, instance.email,
                                                   request.user.email)).first()

        if task:
            task.delete()

        return response.Response(status=status.HTTP_204_NO_CONTENT)


class SectionViewSet(rest_viewsets.ModelViewSet):
    """
        retrieve:
            Return a section instance.

        list:
            Return all sections, ordered by most recently joined.

        create:
            Create a new section.

        delete:
            Remove an existing section.

        partial_update:
            Update one or more fields on an existing section.

        update:
            Update a section.
    """
    queryset = models.Section.objects.all()
    serializer_class = serializers.SectionSerializer

    def create(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            models.Section.objects.create_user(**serializer.validated_data)
            return response.Response(serializer.validated_data, status=status.HTTP_201_CREATED)

        return response.Response({
            'status': 'Bad request',
            'message': 'Section could not be created with received data.'
        }, status=status.HTTP_400_BAD_REQUEST)


class CategoryViewSet(rest_viewsets.ModelViewSet):
    """
        retrieve:
            Return a category instance.

        list:
            Return all categories, ordered by most recently joined.

        create:
            Create a new category.

        delete:
            Remove an existing category.

        update:
            Update a category.
    """
    queryset = models.Category.objects.all()
    serializer_class = serializers.CategorySerializer
    filter_fields = ('section_id',)

    def create(self, request):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            models.Category.objects.create_user(**serializer.validated_data)

            return response.Response(serializer.validated_data, status=status.HTTP_201_CREATED)

        return response.Response({
            'status': 'Bad request',
            'message': 'Category could not be created with received data.'
        }, status=status.HTTP_400_BAD_REQUEST)


class SavedCardViewSet(rest_viewsets.ModelViewSet):
    """
        retrieve:
            Return a savedcard instance.

        list:
            Return all saved cards for a specific user.

        create:
            Create a new saved card.

        delete:
            Remove an existing saved card.
    """
    authentication_classes = (mixins.CsrfExemptSessionAuthentication, authentication.TokenAuthentication,
                              authentication.SessionAuthentication)
    queryset = models.SavedCard.objects.all()
    serializer_class = serializers.SavedCardSerializer

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(SavedCardViewSet, self).dispatch(*args, **kwargs)

    def list(self, request, *args, **kwargs):
        queryset = models.SavedCard.objects.filter(user=request.user.account.pk)
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)

        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    def create(self, request, *args, **kwargs):
        """
        create saved card object
        :param request:
        :return:
        """

        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            # user = models.User.objects.get(pk=serializer.validated_data['user_id'])
            user = request.user
            account = user.account
            card = models.Card.objects.filter(pk=serializer.validated_data['card_id']).first()

            if not card:
                return response.Response({
                    'message': 'Card matching this ID %s does not exist' % serializer.validated_data['card_id']
                }, status=status.HTTP_400_BAD_REQUEST)

            serializer.validated_data['user'] = account
            serializer.validated_data['card'] = card

            # saved_card = models.SavedCard.objects.filter(card=card.pk, user=account.pk).first()
            # if saved_card:
            #     saved_card.count += 1
            #     saved_card.save()
            # else:
            if not models.SavedCard.objects.filter(card=card.pk, user=account.pk).exists():
                models.SavedCard.objects.create(**serializer.validated_data)

            return response.Response(serializer.data, status=status.HTTP_201_CREATED)

        return response.Response({
            'message': serializer.errors
        }, status=status.HTTP_400_BAD_REQUEST)


class SentCardViewSet(rest_viewsets.ModelViewSet):
    """
        retrieve:
            Return a sent card instance.

        list:
            Return all sent cards belonging to a specific user.

        delete:
            Remove an existing sent card.
    """
    authentication_classes = (mixins.CsrfExemptSessionAuthentication, authentication.TokenAuthentication,
                              authentication.SessionAuthentication)
    queryset = models.SentCard.objects.all()
    serializer_class = serializers.SentCardSerializer

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(SentCardViewSet, self).dispatch(*args, **kwargs)

    def list(self, request, *args, **kwargs):
        queryset = models.SentCard.objects.filter(user=request.user.account.pk)
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)

        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)


class ReceivedCardViewSet(rest_viewsets.ModelViewSet):
    """
        retrieve:
            Return a received card instance.

        list:
            Return all received card belonging to a specific user.

        delete:
            Remove an existing user.
    """
    authentication_classes = (mixins.CsrfExemptSessionAuthentication, authentication.TokenAuthentication,
                              authentication.SessionAuthentication)
    queryset = models.ReceivedCard.objects.all()
    serializer_class = serializers.ReceivedCardSerializer

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ReceivedCardViewSet, self).dispatch(*args, **kwargs)

    def list(self, request, *args, **kwargs):
        queryset = models.ReceivedCard.objects.filter(user=request.user.account.pk)

        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)

        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)


class CommentViewSet(rest_viewsets.ModelViewSet):
    """
        retrieve:
            Return a comment instance.

        list:
            Return all comments for a specific sent card.

        create:
            Create a new comment.

        delete:
            Remove an existing comment.
    """
    authentication_classes = (mixins.CsrfExemptSessionAuthentication, authentication.TokenAuthentication,
                              authentication.SessionAuthentication)
    queryset = models.Comments.objects.all()
    serializer_class = serializers.CommentSerializer
    lookup_field = 'sent_card_id'

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(CommentViewSet, self).dispatch(*args, **kwargs)

    def list(self, request, *args, **kwargs):
        """
        list all comments without filtering by sent_card_id
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        # if not request.user.is_staff:
        #     return response.Response({
        #         'message': 'Not Permitted'
        #     }, status=status.HTTP_403_FORBIDDEN)

        queryset = models.Comments.objects.filter(user=request.user.account.pk)
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)

        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    def create(self, request, *args, **kwargs):
        """
        create comment object
        :param request:
        :return:
        """

        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            sent_card = models.SentCard.objects.get(pk=serializer.validated_data['sent_card_id'])
            received_card = models.ReceivedCard.objects.get(pk=serializer.validated_data['received_card_id'])

            data = serializer.validated_data.copy()
            data['sent_card'] = sent_card
            data['received_card'] = received_card

            models.Comments.objects.create(**data)

            return response.Response(serializer.data, status=status.HTTP_201_CREATED)

        return response.Response({
            'status': 'Bad request',
            'message': serializer.errors
        }, status=status.HTTP_400_BAD_REQUEST)


class BannerViewSet(rest_viewsets.GenericViewSet, rest_mixins.ListModelMixin):
    queryset = models.Banner.objects.all()
    serializer_class = serializers.BannerSerializer
