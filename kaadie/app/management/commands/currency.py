from django.core.management import BaseCommand, CommandError


class Command(BaseCommand):
    """
    create currencies available
    """
    help = "Create all currencies"

    def handle(self, *args, **options):
        pass