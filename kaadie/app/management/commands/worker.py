import logging
import threading, json
from importlib import import_module

from kafka import KafkaConsumer

from django.core.management import BaseCommand, CommandError


def wrapper(func, *args, **kwargs):
    """
    function wrapper
    :param func:
    :param args:
    :param kwargs:
    :return:
    """
    return func(*args, **kwargs)


class Consumer(threading.Thread):
    daemon = True

    def run(self):
        consumer = KafkaConsumer(auto_offset_reset='latest', enable_auto_commit=False)
        consumer.subscribe(['messages'])

        for message in consumer:
            t = threading.Thread(target=Consumer.process_message(**json.loads(message.value)))
            t.start()

    @classmethod
    def process_message(cls, mod_name, fxn_name, *args, **kwargs):
        """
        process received message
        :param mod_name:
        :param fxn_name:
        :param args:
        :param kwargs:
        :return:
        """
        print '========= processing message ============'
        mod = import_module(mod_name)
        fxn = getattr(mod, fxn_name)
        args = kwargs.get('args')
        data = kwargs.get('data')
        resp = wrapper(fxn, *args, **data)
        print(resp)
        print '========= terminating message =========='


class Command(BaseCommand):
    """
    create currencies available
    """
    help = "Create all currencies"

    def handle(self, *args, **options):
        logging.basicConfig(
            format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
            level=logging.INFO
        )
        main()


def main():
    thread = Consumer()
    thread.run()