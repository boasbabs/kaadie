# -*- coding: utf-8 -*-

# THIS IS FOR DEVELOPMENT ENVIRONMENT
# DO NOT USE IT IN PRODUCTION

# Create your own dev_local.py
# import * this module there and use it like this:
# python manage.py runserver --settings=forum.settings.dev_local

from __future__ import unicode_literals

from .common import *

DEBUG = True

DOMAIN = 'http://ec2-75-101-244-147.compute-1.amazonaws.com'

SCHEME = 'ec2-75-101-244-147.compute-1.amazonaws.com'
