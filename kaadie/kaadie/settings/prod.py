# -*- coding: utf-8 -*-

# MINIMAL CONFIGURATION FOR PRODUCTION ENV

# Create your own prod_local.py
# import * this module there and use it like this:
# python manage.py runserver --settings=forum.settings.prod_local

from __future__ import unicode_literals

from .common import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

SECRET_KEY = os.environ.get("SECRET_KEY", 'q0xfqh=!t@kf_yb+7*z9fs&014+^tkv-s2o95*$alcm98%$zgj')

DOMAIN = 'http://kaadie.com'

SCHEME = 'kaadie.com'
