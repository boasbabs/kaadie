from __future__ import absolute_import, unicode_literals

from django.conf import settings
# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from .celery import app as celery_app
from elasticsearch import Elasticsearch
from pymongo import MongoClient
import pusher
import redis

# mongodb configuration
mongo_client = MongoClient(settings.MONGOD_HOST, settings.MONGOD_PORT, connect=False)

# elasticsearch index configuration
try:
    es = Elasticsearch()
    es.indices.create(index=settings.ES_INDEX, ignore=400)
except Exception, e:
    print('=============error================')
    print(e)

# pusher configuration
pusher_client = pusher.Pusher(app_id=settings.PUSHER_APP_ID, key=settings.PUSHER_KEY,
                              secret=settings.PUSHER_SECRET, cluster=settings.PUSHER_CLUSTER, ssl=True)

# redis configuration
redis_obj = redis.Redis(host=settings.REDIS_HOST, port=settings.REDIS_PORT)

__all__ = ['celery_app', 'es', 'mongo_client', 'pusher_client', 'redis_obj']
