"""kaadie URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from rest_framework_swagger import renderers
# from rest_framework.authtoken import views as auth_views
from rest_framework_expiring_authtoken import views as expiring_views
from rest_framework import response, views as rest_views, schemas, authentication, permissions
import debug_toolbar
from dashing.utils import router as dashing_routers
import spirit.urls

handler404 = 'app.views.custom_page_not_found'
handler500 = 'app.views.custom_internal_server_error'


class SwaggerSchemaView(rest_views.APIView):
    renderer_classes = [
        renderers.OpenAPIRenderer,
        renderers.SwaggerUIRenderer
    ]

    permission_classes = (permissions.AllowAny,)

    parser_classes = ()

    authentication_classes = (authentication.TokenAuthentication, authentication.SessionAuthentication,)

    def get(self, request):
        generator = schemas.SchemaGenerator(title='Kaadie API Documentation', urlconf='app.urls')
        schema = generator.get_schema(request=request)

        return response.Response(schema)

admin.site.site_header = 'KAADIE ADMIN'

urlpatterns = [
    url(r'^forum/', include(spirit.urls)),
    url(r'^', include('app.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^s3direct/', include('s3direct.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-token-auth/', expiring_views.obtain_expiring_auth_token),
    url(r'^v1/docs$', SwaggerSchemaView.as_view(), name='docs'),
    url(r'^dashboard/', include(dashing_routers.urls)),
    url(r'^docs/', include('rest_framework_docs.urls')),
    url(r'^__debug__/', include(debug_toolbar.urls)),
]
