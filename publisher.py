import json

from kafka import KafkaProducer
from kafka.errors import KafkaError

producer = KafkaProducer(bootstrap_servers=['localhost:9092'], value_serializer=lambda m: json.dumps(m).encode('ascii'))

# Asynchronous by default
future = producer.send('messages', {'a': 'b'})

# Block for 'synchronous' sends
try:
    record_metadata = future.get(timeout=10)
except KafkaError, e:
    # Decide what to do if produce request failed...
    print(e)

# Successful result returns assigned partition and offset
print (record_metadata.topic)
print (record_metadata.partition)
print (record_metadata.offset)