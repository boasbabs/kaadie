FROM python:2.7
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
    createdb -U postgres -h localhost kaadie
ADD . /code/
RUN python kaadie/manage.py makemigrations
    python kaadie/manage.py migrate
    python kaadie/manage.py collectstatic
