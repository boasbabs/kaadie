#!/usr/bin/env python

import logging
import threading, json, os
from importlib import import_module
from imp import load_source

from kafka import KafkaConsumer

import sys, os, django
sys.path.append("/opt/kaadie/kaadie")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "kaadie.settings")
django.setup()

from django.conf import settings


class Consumer(threading.Thread):
    daemon = True

    def run(self):
        consumer = KafkaConsumer(bootstrap_servers=['localhost:9092'], auto_offset_reset='latest', enable_auto_commit=False)
        consumer.subscribe([str(settings.KAFKA_DEFAULT_TOPIC)])

        for message in consumer:
            Consumer.process_message(**json.loads(message.value))
            # t = threading.Thread(target=Consumer.process_message(**json.loads(message.value)))
            # t.start()

    @classmethod
    def process_message(cls, file_path, mod_name, fxn_name, *args, **kwargs):
        """
        process received message
        :param mod_name:
        :param fxn_name:
        :param args:
        :param kwargs:
        :return:
        """
        print '========= processing message ============'
        mod = load_source(mod_name, file_path)
        fxn = getattr(mod, fxn_name)
        print(fxn)
        print '========= terminating message =========='


def main():
    thread = Consumer()
    thread.run()


if __name__ == "__main__":
    logging.basicConfig(
        filename='/var/log/kafka-worker.log',
        format='%(asctime)s.%(msecs)s:%(name)s:%(thread)d:%(levelname)s:%(process)d:%(message)s',
        level=logging.DEBUG
        )

    main()